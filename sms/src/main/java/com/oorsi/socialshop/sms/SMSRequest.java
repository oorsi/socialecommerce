package com.oorsi.socialshop.sms;

public class SMSRequest {

	public SMSRequest(String src, String dst, String text) {
		this.src = src;
		this.dst = dst;
		this.text = text;
	}

	private String src;
	private String dst;
	private String text;

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
