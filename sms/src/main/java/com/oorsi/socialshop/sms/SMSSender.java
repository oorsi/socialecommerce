package com.oorsi.socialshop.sms;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SMSSender {

	@Value("#{environment.AUTHENTICATION_ID}")
	private String authenticationID;

	@Value("#{environment.AUTHENTICATION_TOKEN}")
	private String authenticationToken;

	@Value("#{environment.SOURCE_NUMBER}")
	private String sourceNumber;

	private String URL = "https://api.plivo.com/v1/Account/MANTA0MZZJZDKZMZDMNZ/Message/";

	public ResponseEntity<Object> sendSMS(String number, String message) {
		String plainCreds = authenticationID + ":" + authenticationToken;
		byte[] plainCredsBytes = plainCreds.getBytes(Charset.forName("US-ASCII"));
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + base64Creds);

		HttpEntity<SMSRequest> request = new HttpEntity<SMSRequest>(new SMSRequest(sourceNumber, number, message),
				headers);
		return new RestTemplate().exchange(URL, HttpMethod.POST, request, Object.class);
	}
}
