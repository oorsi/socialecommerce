package com.oorsi.socialshop.push;

public class PushRequest {

	private String app_id;
	private String[] include_player_ids;
	private Data data;
	private Content contents;

	public PushRequest(String app_id, String[] include_player_ids, String page, String message) {
		this.app_id = app_id;
		this.include_player_ids = include_player_ids;
		this.data = new Data(page);
		this.contents = new Content(message);
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public String[] getInclude_player_ids() {
		return include_player_ids;
	}

	public void setInclude_player_ids(String[] include_player_ids) {
		this.include_player_ids = include_player_ids;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public Content getContents() {
		return contents;
	}

	public void setContents(Content contents) {
		this.contents = contents;
	}

	class Content {
		String en;

		public Content(String en) {
			this.en = en;
		}

		public String getEn() {
			return en;
		}
	}

	class Data {
		String page;

		public Data(String page) {
			this.page = page;
		}

		public String getPage() {
			return page;
		}
	}

}
