package com.oorsi.socialshop.push;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PushSender {

	@Value("#{environment.ONE_SIGNAL_AUTHORIZATION}")
	private String oneSignalAuthorization;

	@Value("#{environment.ONE_SIGNAL_APP_ID}")
	private String oneSignalAppID;

	private String URL = "https://onesignal.com/api/v1/notifications";

	public void sendPushNotification(List<String> playerIDs, String page, String message) {
		String[] playerIDStrings = new String[playerIDs.size()];

		for (int i = 0; i < playerIDs.size(); i++) {
			playerIDStrings[i] = playerIDs.get(i);
		}
		sendPushNotification(playerIDStrings, page, message);
	}

	public void sendPushNotification(String[] playerIDs, String page, String message) {

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + oneSignalAuthorization);

		HttpEntity<PushRequest> request = new HttpEntity<PushRequest>(
				new PushRequest(oneSignalAppID, playerIDs, page, message), headers);
		Object response = new RestTemplate().exchange(URL, HttpMethod.POST, request, Object.class);
		System.out.println(response);
	}

}
