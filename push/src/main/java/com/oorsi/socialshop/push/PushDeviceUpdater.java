package com.oorsi.socialshop.push;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PushDeviceUpdater {

	@Value("#{environment.ONE_SIGNAL_AUTHORIZATION}")
	private String oneSignalAuthorization;

	@Value("#{environment.ONE_SIGNAL_APP_ID}")
	private String oneSignalAppID;

	private String URL = "https://onesignal.com/api/v1/players/";

	public void updatePushDevice(String playerID, Long userID, String firstName, String lastName) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + oneSignalAuthorization);

		HttpEntity<PushDeviceUpdateRequest> request = new HttpEntity<PushDeviceUpdateRequest>(
				new PushDeviceUpdateRequest(oneSignalAppID, userID, firstName, lastName), headers);
		Object response = new RestTemplate().exchange(URL + playerID, HttpMethod.POST, request, Object.class);
		System.out.println(response);
	}

}
