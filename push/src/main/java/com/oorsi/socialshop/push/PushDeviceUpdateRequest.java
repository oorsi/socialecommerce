package com.oorsi.socialshop.push;

public class PushDeviceUpdateRequest {
	private String app_id;
	private Tags tags;

	public PushDeviceUpdateRequest(String app_id, Long userID, String firstName, String lastName) {
		this.app_id = app_id;
		this.tags = new Tags(userID.toString(), firstName, lastName);
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public Tags getTags() {
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}

	class Tags {

		public Tags(String userID, String firstName, String lastName) {
			this.userID = userID;
			this.firstName = firstName;
			this.lastName = lastName;
		}

		private String userID;
		private String firstName;
		private String lastName;

		public String getUserID() {
			return userID;
		}

		public void setUserID(String userID) {
			this.userID = userID;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

	}
}
