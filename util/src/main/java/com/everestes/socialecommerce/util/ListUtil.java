package com.everestes.socialecommerce.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ListUtil {

	@SuppressWarnings("unchecked")
	public static <From, To> List<To> extractListField(List<From> list, String fieldName) {
		List<To> retList = new ArrayList<>();
		for (From t : list) {
			try {
				Field field = t.getClass().getDeclaredField(fieldName);
				// setAccessible to true to get private field value
				field.setAccessible(true);
				// get value
				retList.add((To) (field.get(t)));
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		return retList;
	}

}
