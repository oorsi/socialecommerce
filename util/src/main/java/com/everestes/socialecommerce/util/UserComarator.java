package com.everestes.socialecommerce.util;

import java.util.Comparator;

import com.everestes.socialecommerce.domain.user.User;

public class UserComarator implements Comparator<User> {

	@Override
	public int compare(User o1, User o2) {
		return o1.getFirstName().compareTo(o2.getFirstName());
	}

}
