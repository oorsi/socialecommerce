package com.everestes.socialecommerce.util;

public class PhoneNumberUtil {

	public static String formatPhoneNumber(String phoneNumber) {
		String formattedString = phoneNumber.replaceAll("[^\\d]", "");

		if (formattedString.startsWith("1")) {
			return "+" + formattedString;
		} else {
			return "+1" + formattedString;
		}
	}

}
