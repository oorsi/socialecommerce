package com.everestes.socialecommerce.util;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

public class DozerHelper {

	public static <T, U> List<U> map(final Mapper mapper, final List<T> source, final Class<U> destType) {

		final List<U> dest = new ArrayList<U>();

		for (T element : source) {
			if (element == null) {
				continue;
			}
			dest.add(mapper.map(element, destType));
		}

		return dest;
	}
}