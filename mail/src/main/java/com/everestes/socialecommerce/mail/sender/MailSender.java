package com.everestes.socialecommerce.mail.sender;

import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.everestes.socialecommerce.domain.order.CustomerOrder;

@Component
public class MailSender {

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("#{environment.MAIL_SEND_FROM}")
	private String supportEmail;

	@Autowired
	@Qualifier("mailTemplateEngine")
	private TemplateEngine templateEngine;

	@Value("#{environment.MAIL_SERVER_HOST}")
	private String baseURL;

	public void sendRegistrationConfirmation(final Locale locale, String firstName, String lastName, String email)
			throws MessagingException {
		// Prepare the evaluation context
		final Context ctx = new Context(locale);
		ctx.setVariable("firstName", firstName);
		ctx.setVariable("lastName", lastName);
		ctx.setVariable("email", email);

		sendEmail(ctx, email, "Welcome to OOrsi!", "user/registrationConfirmation.html");

	}

	public void sendPasswordResetEmail(final Locale locale, String email, String code) throws MessagingException {
		// Prepare the evaluation context
		final Context ctx = new Context(locale);
		ctx.setVariable("code", code);

		sendEmail(ctx, email, "Reset Password!", "user/resetPassword.html");
	}

	public void sendPasswordResetConfirmation(final Locale locale, String email) throws MessagingException {
		// Prepare the evaluation context
		final Context ctx = new Context(locale);
		sendEmail(ctx, email, "Password reset successful!", "user/resetPasswordConfirmation.html");
	}

	public void sendOrderConfirmation(final Locale locale, String email, CustomerOrder customerOrder)
			throws MessagingException {

		// Prepare the evaluation context
		final Context ctx = new Context(locale);
		ctx.setVariable("order", customerOrder);
		sendEmail(ctx, email, "Order Confirmation!", "user/orderConfirmation.html");

	}

	private void sendEmail(final Context context, String email, String subject, String templateName)
			throws MessagingException {

		context.setVariable("baseURL", baseURL);

		// Prepare message using a Spring helper
		final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
		message.setSubject(subject);
		message.setFrom(supportEmail);
		message.setTo(email);

		// Create the HTML body using Thymeleaf
		final String htmlContent = this.templateEngine.process(templateName, context);
		message.setText(htmlContent, true /* isHtml */);

		new Thread(new Runnable() {
			@Override
			public void run() {
				javaMailSender.send(mimeMessage);
			}
		}).start();
		;

	}

}
