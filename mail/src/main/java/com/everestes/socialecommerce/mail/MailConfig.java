package com.everestes.socialecommerce.mail;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

@Configuration
@PropertySource(value = { "classpath:javamail.properties" })
@ComponentScan(basePackages = "com.everestes.socialecommerce.mail")
public class MailConfig {

	@Value("#{environment.MAIL_SERVER_HOST}")
	private String mailServerHost;

	@Value("#{environment.MAIL_SERVER_PROTOCOL}")
	private String mailServerProtocol;

	@Value("#{environment.MAIL_SERVER_PORT}")
	private String mailServerPort;

	@Value("#{environment.MAIL_SERVER_USERNAME}")
	private String mailServerUsername;

	@Value("#{environment.MAIL_SERVER_PASSWORD}")
	private String mailServerPassword;

	// @Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		configurer.setIgnoreUnresolvablePlaceholders(true);
		return configurer;
	}

	@Bean
	public JavaMailSender javaMailSender() throws IOException {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailServerHost);
		mailSender.setPort(Integer.parseInt(mailServerPort));
		mailSender.setProtocol(mailServerProtocol);
		mailSender.setUsername(mailServerUsername);
		mailSender.setPassword(mailServerPassword);
		mailSender.setJavaMailProperties(javaMailProperties());
		return mailSender;
	}

	private Properties javaMailProperties() throws IOException {
		Properties properties = new Properties();
		properties.load(new ClassPathResource("javamail.properties").getInputStream());
		return properties;
	}

	/**
	 * THYMELEAF: View Resolver - implementation of Spring's ViewResolver interface.
	 */
	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine());
		return viewResolver;
	}

	/**
	 * THYMELEAF: Template Engine (Spring4-specific version).
	 */
	@Bean(name = "mailTemplateEngine")
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(emailTemplateResolver());
		return templateEngine;
	}

	/**
	 * THYMELEAF: Template Resolver for email templates.
	 */
	private TemplateResolver emailTemplateResolver() {
		TemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("/mail/");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setOrder(1);
		return templateResolver;
	}

}
