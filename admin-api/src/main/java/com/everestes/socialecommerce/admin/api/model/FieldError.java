package com.everestes.socialecommerce.admin.api.model;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

public class FieldError {

	static Logger logger = LoggerFactory.getLogger(FieldError.class);

	private String fieldName;
	private String code;
	private String message;

	public static FieldError create(MessageSource messageSource, Locale locale, String fieldName, String code,
			String defaultMessage) {
		try {
			return new FieldError(fieldName, code, messageSource.getMessage(defaultMessage, null, locale));
		} catch (NoSuchMessageException e) {
			return new FieldError(fieldName, code, defaultMessage);
		}
	}

	public FieldError(String fieldName, String code, String message) {
		this.fieldName = fieldName;
		this.code = code;
		this.message = message;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
