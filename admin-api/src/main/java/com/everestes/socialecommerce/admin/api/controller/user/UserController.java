package com.everestes.socialecommerce.admin.api.controller.user;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.admin.api.model.CreateUserModel;
import com.everestes.socialecommerce.admin.api.model.FieldError;
import com.everestes.socialecommerce.admin.api.model.user.UserView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.DuplicateEmailException;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/user")
public class UserController {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private Mapper mapper;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private FriendshipService friendshipService;

	@GetMapping("/all")
	public List<UserView> getAllUsers() {
		List<User> allUsers = userService.getAllUsers();
		Collections.sort(allUsers, new Comparator<User>() {
			public int compare(User o1, User o2) {
				return o2.getRegistrationDate().compareTo(o1.getRegistrationDate());
			}
		});
		return DozerHelper.map(mapper, allUsers, UserView.class);
	}

	@GetMapping("/count")
	public int getNumberOfAllUsers() {
		return userService.getNumberOfAllUsers();
	}

	@GetMapping("/{id}")
	public UserView getUserDetails(@PathVariable("id") Long userId) {
		return mapper.map(userService.getUserById(userId), UserView.class);
	}

	@PostMapping("/save")
	public ResponseEntity<?> create(@RequestBody CreateUserModel createUserModel, BindingResult result,
			HttpServletRequest request, Locale locale) {
		if (result.hasErrors()) {
			List<com.everestes.socialecommerce.admin.api.model.FieldError> fieldErrorMessages = new ArrayList<com.everestes.socialecommerce.admin.api.model.FieldError>();
			for (org.springframework.validation.FieldError error : result.getFieldErrors()) {
				fieldErrorMessages.add(com.everestes.socialecommerce.admin.api.model.FieldError.create(messageSource,
						locale, error.getField(), error.getCode(), error.getDefaultMessage()));
			}

			return ResponseEntity.badRequest().body(fieldErrorMessages);
		}
		User user;
		try {
			user = userService.registerUser(createUserModel.getEmail(),
					new Integer(SecureRandom.getInstanceStrong().nextInt(1000000)).toString(),
					createUserModel.getFirstName(), createUserModel.getLastName(), locale,
					createUserModel.getBirthDate(), createUserModel.getFbat(), true);
			return ResponseEntity.ok(mapper.map(user, UserView.class));

		} catch (NoSuchAlgorithmException e) {
			logger.error("Cannot generate new password", e);
			return ResponseEntity.badRequest().body(new FieldError[] { FieldError.create(messageSource, locale,
					"password", "password.generateFailed", "Cannot generate new password") });
		} catch (DuplicateEmailException e) {
			logger.error("Email address already Registered", e);
			return ResponseEntity.badRequest().body(new FieldError[] { FieldError.create(messageSource, locale, "email",
					"register.alreadyRegistered", "Email address already Registered") });
		}

	}

	@GetMapping("/info")
	private UserView userInfo(@RequestParam Long userId) {
		User user = userService.getUserById(userId);
		UserView userInfo = mapper.map(user, UserView.class);
		userInfo.setNumberOfFollowed(friendshipService.numberOfFollowing(user.getUserID()));
		userInfo.setNumberOfFollowers(friendshipService.numberOfFollowers(user.getUserID()));
		return userInfo;
	}
}
