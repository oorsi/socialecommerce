package com.everestes.socialecommerce.admin.api.model;

import com.everestes.socialecommerce.domain.type.Retailer;

public class AddProductModel {

	private String url;
	private Retailer retailer;
	private String sku;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

}
