package com.everestes.socialecommerce.admin.api.controller.wishlist;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.admin.api.model.WishlistProductView;
import com.everestes.socialecommerce.admin.api.model.wishlist.AddWishlistModel;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.wishlist.WishListService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/wishlist")
public class WishlistController {

	@Autowired
	private WishListService wishlistService;

	@Autowired
	private UserService userService;

	@Autowired
	private Mapper mapper;

	@GetMapping("/list")
	public List<WishlistProductView> json(@RequestParam Long userId) {
		return DozerHelper.map(mapper, wishlistService.list(userId), WishlistProductView.class);
	}

	@PostMapping("/add")
	public WishlistProductView add(@RequestBody AddWishlistModel addWishlistModel) {
		return mapper.map(wishlistService.addToWishList(userService.getUserById(addWishlistModel.getUserId()),
				addWishlistModel.getRetailer(), addWishlistModel.getSku()), WishlistProductView.class);
	}
}
