package com.everestes.socialecommerce.admin.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.everestes.socialecommerce.admin.api")
public class AppConfig {

}
