package com.everestes.socialecommerce.admin.api.controller.user;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.admin.api.JwtTokenUtil;
import com.everestes.socialecommerce.admin.api.model.FieldError;
import com.everestes.socialecommerce.admin.api.model.user.JwtAuthenticationRequest;
import com.everestes.socialecommerce.admin.api.model.user.JwtAuthenticationResponse;
import com.everestes.socialecommerce.service.user.UserService;

@RestController
public class LoginController {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	@Qualifier("authMgr")
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
			HttpServletRequest request, Locale locale) {

		// Perform the security
		Authentication authentication;
		try {
			authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));

			SecurityContextHolder.getContext().setAuthentication(authentication);

			// Reload password post-security so we can generate token
			final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getUsername());
			final String token = jwtTokenUtil.generateToken(userDetails, DeviceUtils.getCurrentDevice(request));

			// Return the token
			return ResponseEntity.ok(new JwtAuthenticationResponse(token));
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "password", "wrong.password", "Wrong password") });
		}
	}

}
