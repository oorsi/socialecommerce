package com.everestes.socialecommerce.admin.api.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.oorsi.socialshop.sms.SMSSender;

@Controller
@RequestMapping("/test/sms")
public class SMSController {

	@Autowired
	private SMSSender smsSender;

	@RequestMapping("/send")
	public ResponseEntity<Object> sendSMS(String number, String message) {
		return smsSender.sendSMS(number, message);
	}

}
