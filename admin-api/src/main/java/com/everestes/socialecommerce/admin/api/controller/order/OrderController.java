package com.everestes.socialecommerce.admin.api.controller.order;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.everestes.socialecommerce.admin.api.model.order.OrderView;
import com.everestes.socialecommerce.service.order.OrderService;
import com.everestes.socialecommerce.util.DozerHelper;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private Mapper mapper;

	@RequestMapping("/open/all")
	private List<OrderView> openOrders() {
		return DozerHelper.map(mapper, orderService.getCustomerOrders(), OrderView.class);
	}

	@RequestMapping("/open/user/{userId}")
	private List<OrderView> openOrders(@PathVariable("userId") Long userId) {
		return DozerHelper.map(mapper, orderService.getCustomerOrders(userId), OrderView.class);
	}
}
