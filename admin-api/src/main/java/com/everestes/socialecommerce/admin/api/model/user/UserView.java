package com.everestes.socialecommerce.admin.api.model.user;

import java.util.Date;

public class UserView {

	private long userID;
	private String firstName;
	private String lastName;
	private String email;
	private Date registrationDate;

	private long numberOfFollowers;
	private long numberOfFollowed;

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public long getNumberOfFollowers() {
		return numberOfFollowers;
	}

	public void setNumberOfFollowers(long numberOfFollowers) {
		this.numberOfFollowers = numberOfFollowers;
	}

	public long getNumberOfFollowed() {
		return numberOfFollowed;
	}

	public void setNumberOfFollowed(long numberOfFollowed) {
		this.numberOfFollowed = numberOfFollowed;
	}

}
