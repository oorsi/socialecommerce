package com.everestes.socialecommerce.admin.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

	@RequestMapping("/")
	private String getHealth() {
		return "{\"status\": \"up\"}";
	}

	@RequestMapping("/test")
	private String getTest() {
		return "{\"status\": \"testing\"}";
	}

}
