package com.everestes.socialecommerce.admin.api.controller.catalog;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.admin.api.model.AddProductModel;
import com.everestes.socialecommerce.admin.api.model.catalog.ProductView;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.service.product.ProductService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private Mapper mapper;

	@GetMapping("/all")
	public List<ProductView> getProducts() {
		return DozerHelper.map(mapper, productService.getAllProducts(), ProductView.class);
	}

	@PostMapping("/add")
	public ProductView addProduct(@RequestBody AddProductModel addProductModel) {
		if (StringUtils.isEmpty(addProductModel.getUrl())) {
			return mapper.map(productService.addProduct(Retailer.AMAZON, addProductModel.getUrl()), ProductView.class);
		} else {
			return mapper.map(productService.addProductBySKU(addProductModel.getRetailer(), addProductModel.getSku()),
					ProductView.class);
		}
	}

	@GetMapping("/retailers")
	public Retailer[] addProduct() {
		return Retailer.values();
	}

}
