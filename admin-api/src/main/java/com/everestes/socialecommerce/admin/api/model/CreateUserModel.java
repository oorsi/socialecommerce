package com.everestes.socialecommerce.admin.api.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

public class CreateUserModel {

	@Email(message = "email.invalid")
	@NotNull(message = "field.required")
	private String email;
	@NotNull(message = "field.required")
	private String firstName;
	@NotNull(message = "field.required")
	private String lastName;

	private String gender;

	// Facebook access token
	private String fbat;

	private Date birthDate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFbat() {
		return fbat;
	}

	public void setFbat(String fbat) {
		this.fbat = fbat;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
