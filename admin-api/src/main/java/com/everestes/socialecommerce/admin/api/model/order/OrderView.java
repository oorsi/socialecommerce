package com.everestes.socialecommerce.admin.api.model.order;

import java.util.Date;
import java.util.List;

import com.everestes.socialecommerce.admin.api.model.user.UserView;

public class OrderView {

	private Long id;

	private List<OrderProductView> orderProducts;

	private UserView fromUser;

	private UserView forUser;

	private String shippingAddress;

	private Date date;

	private Float totalPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<OrderProductView> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProductView> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public UserView getForUser() {
		return forUser;
	}

	public void setForUser(UserView forUser) {
		this.forUser = forUser;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public UserView getFromUser() {
		return fromUser;
	}

	public void setFromUser(UserView fromUser) {
		this.fromUser = fromUser;
	}

}
