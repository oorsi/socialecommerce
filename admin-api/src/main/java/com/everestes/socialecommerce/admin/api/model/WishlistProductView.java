package com.everestes.socialecommerce.admin.api.model;

import com.everestes.socialecommerce.admin.api.model.catalog.ProductView;

public class WishlistProductView {

	private Long id;
	private ProductView product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductView getProduct() {
		return product;
	}

	public void setProduct(ProductView product) {
		this.product = product;
	}
}
