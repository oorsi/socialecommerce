package com.everestes.socialecommerce.admin.api.model.wishlist;

import com.everestes.socialecommerce.domain.type.Retailer;

public class AddWishlistModel {

	private Long userId;
	private Retailer retailer;
	private String sku;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

}
