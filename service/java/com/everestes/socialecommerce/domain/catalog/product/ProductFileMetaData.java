package com.everestes.socialecommerce.domain.catalog.product;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import com.everestes.socialecommerce.file.FileMetaData;

@Entity
@AssociationOverrides({
		@AssociationOverride(name = "productFileMetaDataPK.product", joinColumns = @JoinColumn(name = "productID") ),
		@AssociationOverride(name = "productFileMetaDataPK.fileMetaData", joinColumns = @JoinColumn(name = "fileMetaDataID") ) })
public class ProductFileMetaData {

	@EmbeddedId
	private ProductFileMetaDataPK productFileMetaDataPK;
	// private Boolean isBanner;
	// private Boolean isPortraitPoster;

	public ProductFileMetaDataPK getProductFileMetaDataPK() {
		if (productFileMetaDataPK == null) {
			productFileMetaDataPK = new ProductFileMetaDataPK();
		}
		return productFileMetaDataPK;
	}

	public Product getProduct() {
		return getProductFileMetaDataPK().getProduct();
	}

	public void setProduct(Product product) {
		getProductFileMetaDataPK().setProduct(product);
	}

	public FileMetaData getFileMetaData() {
		return getProductFileMetaDataPK().getFileMetaData();
	}

	public void setFileMetaData(FileMetaData fileMetaData) {
		getProductFileMetaDataPK().setFileMetaData(fileMetaData);
	}

	public void setProductFileMetaDataPK(ProductFileMetaDataPK productFileMetaDataPK) {
		this.productFileMetaDataPK = productFileMetaDataPK;
	}

}
