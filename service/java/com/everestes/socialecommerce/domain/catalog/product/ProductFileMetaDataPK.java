package com.everestes.socialecommerce.domain.catalog.product;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import com.everestes.socialecommerce.file.FileMetaData;

@Embeddable
public class ProductFileMetaDataPK implements Serializable {

	private static final long serialVersionUID = -8798489870106980353L;
	@ManyToOne
	private Product product;
	@ManyToOne
	private FileMetaData fileMetaData;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public FileMetaData getFileMetaData() {
		return fileMetaData;
	}

	public void setFileMetaData(FileMetaData fileMetaData) {
		this.fileMetaData = fileMetaData;
	}

}
