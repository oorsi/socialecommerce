package com.everestes.socialecommerce.domain.catalog.product;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.everestes.socialecommerce.file.FileMetaData;

public class Product {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private long productID;
	private String name;
	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "portraitPosterFileID")
	private FileMetaData portraitPoster;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "landscapePosterFileID")
	private FileMetaData landscapePoster;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bannerFileID")
	private FileMetaData banner;

	@OneToMany(mappedBy = "movieFileMetaDataPK.movie", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ProductFileMetaData> productFiles;

	public long getProductID() {
		return productID;
	}

	public void setProductID(long productID) {
		this.productID = productID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FileMetaData getPortraitPoster() {
		return portraitPoster;
	}

	public void setPortraitPoster(FileMetaData portraitPoster) {
		this.portraitPoster = portraitPoster;
	}

	public FileMetaData getLandscapePoster() {
		return landscapePoster;
	}

	public void setLandscapePoster(FileMetaData landscapePoster) {
		this.landscapePoster = landscapePoster;
	}

	public FileMetaData getBanner() {
		return banner;
	}

	public void setBanner(FileMetaData banner) {
		this.banner = banner;
	}

	public List<ProductFileMetaData> getProductFiles() {
		return productFiles;
	}

	public void setProductFiles(List<ProductFileMetaData> productFiles) {
		this.productFiles = productFiles;
	}

}
