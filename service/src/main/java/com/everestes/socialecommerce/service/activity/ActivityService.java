package com.everestes.socialecommerce.service.activity;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.activity.ActivityDAO;
import com.everestes.socialecommerce.data.dao.activity.LikeDAO;
import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;

@Service
public class ActivityService {

	@Autowired
	private ActivityDAO activityDAO;

	@Autowired
	private LikeDAO likeDAO;

	@Transactional
	public List<Activity> getFreindsActivity(User follower, Date fromDate, Integer limit) {
		List<Activity> activities = activityDAO.getFriendsActivity(follower, fromDate, limit);
		initActivityListExtras(activities, follower.getUserID());
		return activities;
	}

	@Transactional
	public List<Activity> getActivityForUser(User follower, Long userId) {
		List<Activity> activities = activityDAO.getActivityForUser(userId);
		initActivityListExtras(activities, follower.getUserID());
		return activities;
	}

	@Transactional
	public Activity getActivity(Long id, User user) {
		Activity activity = activityDAO.getId(id);
		initActivityExtras(activity, user.getUserID(), true);
		return activity;
	}

	private void initActivityListExtras(List<Activity> activities, Long loggedInUser) {
		for (Activity activity : activities) {
			initActivityExtras(activity, loggedInUser, false);
		}
	}

	private void initActivityExtras(Activity activity, Long loggedInUser, boolean loadComments) {
		if (null != activity.getLikes()) {
			Hibernate.initialize(activity.getLikes().size());
		}
		if (null != activity.getComments()) {
			if (loadComments) {
				Hibernate.initialize(activity.getComments());
			} else {
				Hibernate.initialize(activity.getComments().size());
			}
		}
		if (null != loggedInUser) {
			activity.setLiked(likeDAO.likedBy(loggedInUser, activity.getId()));
		}
	}
}
