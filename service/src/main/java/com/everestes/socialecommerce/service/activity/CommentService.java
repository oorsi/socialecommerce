package com.everestes.socialecommerce.service.activity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.activity.ActivityDAO;
import com.everestes.socialecommerce.data.dao.activity.CommentDAO;
import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.domain.activity.Comment;

@Service
public class CommentService {

	@Autowired
	private CommentDAO commentDAO;

	@Autowired
	private ActivityDAO activityDAO;

	@Autowired
	private UserDAO userDAO;

	@Transactional
	public Comment saveComment(String commentString, Long commentedBy, Long wishListProductId) {
		Comment comment = new Comment();
		comment.setComment(commentString);
		comment.setUser(userDAO.getId(commentedBy));
		comment.setActivity(activityDAO.getId(wishListProductId));

		return commentDAO.merge(comment);
	}

	@Transactional
	public List<Comment> getCommentsForActivity(Long activityId) {
		return commentDAO.getCommentsForActivity(activityId);
	}

	@Transactional
	public void removeComment(Long commentId) {

		Comment comment = commentDAO.getComment(commentId);
		commentDAO.delete(comment);

	}

}
