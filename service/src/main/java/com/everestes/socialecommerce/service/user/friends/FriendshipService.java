package com.everestes.socialecommerce.service.user.friends;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.impl.friendship.FriendshipDAO;
import com.everestes.socialecommerce.data.dao.user.FacebookUserDAO;
import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.relationship.Friendship;
import com.everestes.socialecommerce.social.facebook.friends.FacebookService;
import com.everestes.socialecommerce.util.ListUtil;
import com.oorsi.socialshop.push.PushSender;

@Service
public class FriendshipService {

	@Autowired
	private FriendshipDAO friendshipDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private FacebookService facebookService;

	@Autowired
	private FacebookUserDAO facebookUserDAO;

	@Autowired
	private PushSender pushSender;

	@Transactional
	public List<User> getFriends(long userID) {

		List<User> users = friendshipDAO.getFriends(userID);

		for (User user : users) {
			user.setFollowed(true);
		}

		return users;
	}

	@Transactional
	public List<User> getFollowers(long userID) {

		List<User> users = friendshipDAO.getFollowers(userID);

		for (User user : users) {
			user.setFollowed(isFollowing(userID, user.getUserID()));
		}

		return users;
	}

	@Transactional
	public Friendship followFriend(long userID, long friendID) {
		Friendship friendship = friendshipDAO.getFriendship(userID, friendID);
		if (null == friendship) {
			friendship = new Friendship();
			friendship.setFollower(userDAO.getId(userID));
			friendship.setFollowed(userDAO.getId(friendID));
			friendship = friendshipDAO.merge(friendship);

			// notify friend
			if (friendship.getFollowed().getPushPlayerIDs() != null
					&& friendship.getFollowed().getPushPlayerIDs().size() > 0) {
				pushSender.sendPushNotification(
						ListUtil.extractListField(friendship.getFollowed().getPushPlayerIDs(), "playerID"),
						"friendship", friendship.getFollower().getFirstName() + " "
								+ friendship.getFollower().getLastName() + " started following you!");
			}
		}
		return friendship;
	}

	@Transactional
	public void unfollow(long userID, long friendID) {
		Friendship friendship = friendshipDAO.getFriendship(userID, friendID);
		friendshipDAO.delete(friendship);
	}

	@Transactional
	public long numberOfFollowers(long userID) {
		return friendshipDAO.getNumberOfFollowers(userID);
	}

	@Transactional
	public long numberOfFollowing(long userID) {
		return friendshipDAO.getNumberOfFollowing(userID);
	}

	@Transactional
	public List<User> getFacebookFriends(long userId, String accessToken) {
		List<String> friendIDs = facebookService.getFriends(accessToken);

		if (friendIDs.size() > 0) {

			List<User> users = facebookUserDAO.getFacebookUserByFacebookId(friendIDs);

			for (User user : users) {
				user.setFollowed(isFollowing(userId, user.getUserID()));
			}
			return users;
		} else {
			return new ArrayList<>();
		}
	}

	@Transactional
	public boolean isFollowing(long followerId, long followedId) {
		return friendshipDAO.isFollowing(followerId, followedId);
	}

	public void sendPush() {
		// pushSender.sendPushNotification();
	}

}
