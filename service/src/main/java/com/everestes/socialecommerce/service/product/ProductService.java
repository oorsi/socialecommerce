package com.everestes.socialecommerce.service.product;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.catalog.ProductDAO;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.catalog.ProductSKU;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.ws.client.amazon.rainforest.AmazonRainforestClient;
import com.everestes.socialecommerce.ws.client.bestbuy.BestbuyProductClient;

@Service
public class ProductService {

	@Autowired
	private BestbuyProductClient bestbuyProductClient;

	@Autowired
	private AmazonRainforestClient amazonRainforestClient;

	@Autowired
	private ProductDAO productDAO;

	public List<Product> getProducts(int startingIndex, int pageSize) {
		return bestbuyProductClient.getProducts(null, pageSize, startingIndex);
	}

	public List<Product> getProductBySku(Retailer retailer, String... skus) {
		return bestbuyProductClient.getProductBySku(skus);
	}

	public List<Product> getTrendingProducts() {
		return bestbuyProductClient.getTrendingProducts();
	}

	@Transactional
	public Product getProductBySku(Retailer retailer, String sku, boolean includeImages) {
		Product product = productDAO.getProductByRetailerAndSku(retailer, sku);
		if (null != product) {
			if (includeImages) {
				Hibernate.initialize(product.getProductImageURLs());
			}
			return product;
		}

		if (Retailer.BEST_BUY.equals(retailer)) {
			return bestbuyProductClient.getProductBySku(sku);
		} else {// if (Retailer.BEST_BUY.equals(retailer)) {
			return amazonRainforestClient.getProductByAsin(sku);
		}
	}

	@Transactional
	public Product getProductById(Long id, boolean includeImages) {
		Product product = productDAO.getId(id);
		if (includeImages) {
			Hibernate.initialize(product);
		}
		return product;
	}

	public List<Product> searchProducts(String searchString, int pageNumber, int pageSize)
			throws UnsupportedEncodingException {
		return amazonRainforestClient.search(searchString);
	}

	public List<Product> searchProductsByUPC(String upc) {
		return bestbuyProductClient.getProductsByUPC(new String[] { upc });
	}

	public List<Product> getProductBySku(Retailer bestBuy, List<String> skus) {
		return bestbuyProductClient.getProductBySku(skus.toArray(new String[skus.size()]));

	}

	@Transactional
	public Product saveProduct(Product p, Retailer retailer, String sku) {

		Product product = productDAO.getProductByRetailerAndSku(retailer, sku);
		if (null == product) {
			ArrayList<ProductSKU> productSKUs = new ArrayList<>();
			productSKUs.add(new ProductSKU(sku, retailer, product));
			p.setProductSKUs(productSKUs);
			p.setAddedDate(new Date());
			product = productDAO.merge(p);
		}
		return product;
	}

	@Transactional
	public Product addProduct(Retailer retailer, String url) {
		Product product = amazonRainforestClient.getProductByURL(url);
		return saveProduct(product, product.getRetailer(), product.getSku());
	}

	@Transactional
	public Product addProductBySKU(Retailer retailer, String sku) {
		Product product = getProductBySku(retailer, sku, false);
		return saveProduct(product, product.getRetailer(), product.getSku());
	}

	@Transactional
	public Product getProduct(Long id) {
		return productDAO.getId(id);
	}

	@Transactional
	public List<Product> getAllProducts() {
		return productDAO.findAll();
	}

}
