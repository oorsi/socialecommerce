package com.everestes.socialecommerce.service.user.phone;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.user.phone.PhoneNumberConfirmationDAO;
import com.everestes.socialecommerce.data.dao.user.phone.PhoneNumberDAO;
import com.everestes.socialecommerce.domain.user.PhoneNumber;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.phoneNumber.PhoneNumberConfirmation;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;
import com.everestes.socialecommerce.util.PhoneNumberUtil;
import com.oorsi.socialshop.sms.SMSSender;

@Service
public class PhoneNumberService {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private PhoneNumberDAO phoneNumberDAO;

	@Autowired
	private PhoneNumberConfirmationDAO phoneNumberConfirmationDAO;

	@Autowired
	private SMSSender smsSender;

	@Autowired
	private FriendshipService friendshipService;

	// @Autowired
	// private UserDAO userDAO;

	@Transactional
	public void savePhoneNumber(User user, String phoneNumber) {
		// if there's unconfirmed number, just change the phone number
		logger.info("before getUnconfirmedPhoneNumber "
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		PhoneNumber phoneNumberEntity = phoneNumberDAO.getUnconfirmedPhoneNumber(user.getUserID());
		logger.info("after getUnconfirmedPhoneNumber "
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		if (null == phoneNumberEntity) {
			phoneNumberEntity = new PhoneNumber();
		}

		phoneNumberEntity.setPhoneNumber(PhoneNumberUtil.formatPhoneNumber(phoneNumber));
		phoneNumberEntity.setUser(user);
		phoneNumberEntity = phoneNumberDAO.merge(phoneNumberEntity);
		PhoneNumberConfirmation phoneNumberConfirmation = phoneNumberConfirmationDAO
				.getPhoneNumberConfirmationForUser(user.getUserID());
		if (null == phoneNumberConfirmation) {
			phoneNumberConfirmation = new PhoneNumberConfirmation();
		}

		try {
			logger.info("1 info " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
			phoneNumberConfirmation.setConfirmation(
					new Integer(SecureRandom.getInstance("NativePRNGNonBlocking").nextInt(1000000)).toString());
			logger.info("2 info " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		phoneNumberConfirmation.setPhoneNumber(phoneNumberEntity);
		phoneNumberConfirmationDAO.merge(phoneNumberConfirmation);

		// Send SMS
		smsSender.sendSMS(phoneNumberEntity.getPhoneNumber(),
				"Your Confirmation Code is: " + phoneNumberConfirmation.getConfirmation());
	}

	@Transactional
	public boolean confirmPhoneNumber(Long userID, String confirmation) {
		PhoneNumberConfirmation phoneNumberConfirmation = phoneNumberConfirmationDAO
				.getPhoneNumberConfirmationForUser(userID);

		if (null != phoneNumberConfirmation) {
			if (phoneNumberConfirmation.getConfirmation().equals(confirmation)
					&& phoneNumberConfirmation.getNumberOfTries() < 5) {
				phoneNumberConfirmation.getPhoneNumber().setConfirmed(true);

				phoneNumberConfirmation.getPhoneNumber().setPhoneNumberConfirmation(null);

				phoneNumberDAO.deleteOtherPhoneNumbers(phoneNumberConfirmation.getPhoneNumber().getPhoneNumber(),
						userID);

				phoneNumberDAO.merge(phoneNumberConfirmation.getPhoneNumber());
				phoneNumberConfirmationDAO.delete(phoneNumberConfirmation);
				return true;
			} else if (phoneNumberConfirmation.getNumberOfTries() >= 5) {
				return false;
			} else {
				phoneNumberConfirmation.setNumberOfTries(phoneNumberConfirmation.getNumberOfTries() + 1);
				phoneNumberConfirmationDAO.merge(phoneNumberConfirmation);
				return false;
			}
		} else {
			return false;
		}

	}

	@Transactional
	public List<PhoneNumber> searchPhoneNumber(Long userID, List<String> phoneNumbers) {
		for (int i = 0; i < phoneNumbers.size(); i++) {
			phoneNumbers.set(i, PhoneNumberUtil.formatPhoneNumber(phoneNumbers.get(i)));
		}
		List<PhoneNumber> ps = phoneNumberDAO.getUsersByPhoneNumbers(phoneNumbers);
		for (PhoneNumber p : ps) {
			p.getUser().setFollowed(friendshipService.isFollowing(userID, p.getUser().getUserID()));
		}
		return ps;
	}

	@Transactional
	public PhoneNumber getUnconfirmedPhoneNumber(Long userID) {
		return phoneNumberDAO.getUnconfirmedPhoneNumber(userID);
	}

	@Transactional
	public boolean hasPhoneNumber(Long userID) {
		if (null != phoneNumberDAO.getconfirmedPhoneNumber(userID)) {
			return true;
		} else {
			return false;
		}
	}

}
