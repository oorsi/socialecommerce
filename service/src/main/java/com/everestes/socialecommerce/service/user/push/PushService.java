package com.everestes.socialecommerce.service.user.push;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.data.dao.user.push.PushPlayerIDDAO;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.push.PushPlayerID;
import com.oorsi.socialshop.push.PushDeviceUpdater;

@Service
public class PushService {
	@Autowired
	private PushPlayerIDDAO pushPlayerIDDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private PushDeviceUpdater pushDeviceUpdater;

	@Transactional
	public void savePlayerID(Long userID, String playerID) {
		PushPlayerID pushPlayerID = pushPlayerIDDAO.getId(playerID);
		if (null == pushPlayerID) {
			pushPlayerID = new PushPlayerID();
		}

		pushPlayerID.setPlayerID(playerID);

		User user = userDAO.getId(userID);
		pushPlayerID.setUser(user);

		pushPlayerIDDAO.save(pushPlayerID);

		pushDeviceUpdater.updatePushDevice(playerID, userID, user.getFirstName(), user.getLastName());

	}

	@Transactional
	public void deletePlayerID(Long userID, String playerID) {
		pushPlayerIDDAO.deletePlayerID(userID, playerID);
	}
}
