package com.everestes.socialecommerce.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.impl.file.FileMetaDataDAO;
import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.domain.file.FileMetaData;
import com.everestes.socialecommerce.domain.file.FileType;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.file.upload.FileUploadService;
import com.everestes.socialecommerce.file.upload.exception.FileUploadException;
import com.everestes.socialecommerce.file.upload.response.FileUploadResponse;

@Service
public class FileManagementService {

	@Autowired
	private FileMetaDataDAO fileMetaDataDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private FileUploadService fileUploadService;

	@Transactional
	public FileMetaData uploadProfilePicture(Long userId, String fileName, InputStream inputStream)
			throws FileUploadException {

		// upload to aws
		List<FileUploadResponse> responses = fileUploadService.upload(FileType.FULL.toString(), fileName, inputStream);

		List<FileMetaData> fileMetaDatas = new ArrayList<>();
		for (FileUploadResponse response : responses) {
			// save file meta data
			fileMetaDatas.add(save(null, response.getLocation(), response.getFileName()));
		}

		User user = userDAO.getId(userId);
		user.setProfilePicture(fileMetaDatas.get(0));
		userDAO.save(user);

		return fileMetaDatas.get(0);
	}

	@Transactional
	public FileMetaData save(Long fileID, String directory, String fileName) {
		FileMetaData fileMetaData;
		if (fileID == null) {
			fileMetaData = new FileMetaData();
		} else {
			fileMetaData = fileMetaDataDAO.getId(fileID);
		}
		fileMetaData.setFileName(fileName);
		fileMetaData.setDirectory(directory);
		return fileMetaDataDAO.merge(fileMetaData);
	}

}
