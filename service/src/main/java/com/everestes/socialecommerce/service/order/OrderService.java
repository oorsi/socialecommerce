package com.everestes.socialecommerce.service.order;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.cart.CartProductDAO;
import com.everestes.socialecommerce.data.dao.order.OrderDAO;
import com.everestes.socialecommerce.domain.address.UserAddress;
import com.everestes.socialecommerce.domain.cart.CartProduct;
import com.everestes.socialecommerce.domain.order.CustomerOrder;
import com.everestes.socialecommerce.domain.order.OrderProduct;
import com.everestes.socialecommerce.domain.payment.PaymentMethod;
import com.everestes.socialecommerce.service.address.AddressService;
import com.everestes.socialecommerce.service.payment.ChargePaymentMethodException;
import com.everestes.socialecommerce.service.payment.PaymentMethodException;
import com.everestes.socialecommerce.service.payment.PaymentService;
import com.everestes.socialecommerce.service.user.UserService;

@Service
public class OrderService {

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private CartProductDAO cartProductDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private OrderDAO orderDAO;

	@Transactional
	public CustomerOrder submitOrder(Long userID, Long forUser, Long shipTo, Long shippingAddress,
			String paymentMethodId) throws NoUserAddressException, NoPaymentMethodException,
			ChargePaymentMethodException, PaymentMethodException {

		// create new order
		CustomerOrder order = new CustomerOrder();

		if (shipTo == 1) {
			// if shipping to giver, validate shipping address(If the shipping
			// address id is Logged in users address)
			UserAddress address = addressService.getUserAddress(userID, shippingAddress);
			if (null != address) {
				order.setShippingAddress(address);
			} else {
				throw new NoUserAddressException();
			}
		}

		// Validate payment method if the payment method is associated with
		// logged in user

		PaymentMethod paymentMethod = paymentService.getPaymentMethod(userID, paymentMethodId);

		if (null == paymentMethod) {
			throw new NoPaymentMethodException();
		}

		order.setDate(new Date());
		order.setForUser(userService.getUserById(forUser));
		order.setFromUser(userService.getUserById(userID));

		order.setShipTo(shipTo);

		List<CartProduct> cartProducts = cartProductDAO.getCart(userID, forUser, false, false);

		for (CartProduct cartProduct : cartProducts) {
			OrderProduct orderProduct = new OrderProduct();
			orderProduct.setProduct(cartProduct.getProduct());
			orderProduct.setSoldPrice(cartProduct.getProduct().getSalePrice());
			orderProduct.setQuantity(cartProduct.getQuantity());
			orderProduct.setCustomerOrder(order);
			order.addOrderProduct(orderProduct);

			// set cart product as sold
			cartProduct.setPurchased(true);
		}

		order = orderDAO.merge(order);

		// charge customer
		paymentService.chargeCustomer(order, paymentMethod);

		// if everything goes right save cart products
		cartProductDAO.mergeAll(cartProducts);
		return order;
	}

	@Transactional
	public List<CustomerOrder> getCustomerOrders() {
		return orderDAO.findAll();
	}

	@Transactional
	public List<CustomerOrder> getCustomerOrders(Long userId) {
		return orderDAO.getOrders(userId);
	}

	@Transactional
	public CustomerOrder getCustomerOrder(Long userId, Long orderId) {
		CustomerOrder order = orderDAO.getOrder(userId, orderId);
		Hibernate.initialize(order.getOrderProducts());
		return order;
	}

}
