package com.everestes.socialecommerce.service.payment;

public class ChargePaymentMethodException extends PaymentMethodException {

	private static final long serialVersionUID = 1L;

	public ChargePaymentMethodException() {
		super();
	}

	public ChargePaymentMethodException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ChargePaymentMethodException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChargePaymentMethodException(String message) {
		super(message);
	}

	public ChargePaymentMethodException(Throwable cause) {
		super(cause);
	}

}
