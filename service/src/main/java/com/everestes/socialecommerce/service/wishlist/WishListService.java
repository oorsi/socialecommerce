package com.everestes.socialecommerce.service.wishlist;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.activity.LikeDAO;
import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.data.dao.wishlist.WishListProductDAO;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.wishlist.WishListProduct;
import com.everestes.socialecommerce.service.product.ProductService;

@Service
public class WishListService {

	@Autowired
	private WishListProductDAO wishListProductDAO;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private LikeDAO likeDAO;

	@Transactional
	public WishListProduct addToWishList(User user, Retailer retailer, String sku) {

		Product product = productService.getProductBySku(retailer, sku, false);
		if (null == product.getProductId()) {
			product = productService.saveProduct(product, retailer, sku);
		}

		return addToWishList(user, product);
	}

	@Transactional
	public WishListProduct addToWishList(User user, String url) {
		Product product = productService.addProduct(Retailer.AMAZON, url);
		return addToWishList(user, product);
	}

	@Transactional
	public WishListProduct addToWishList(User user, Long productId) {

		Product product = productService.getProduct(productId);
		return addToWishList(user, product);
	}

	private WishListProduct addToWishList(User user, Product product) {
		WishListProduct wishListProduct = new WishListProduct();

		wishListProduct.setU(userDAO.getId(user.getUserID()));
		wishListProduct.onCreate();
		wishListProduct.setProduct(product);

		WishListProduct wishListProductEntity = wishListProductDAO.merge(wishListProduct);

		initWishListProductExtras(wishListProductEntity, null);
		return wishListProductEntity;
	}

	@Transactional
	public void removeProduct(long userID, long productId) {
		WishListProduct wishListProduct = wishListProductDAO.getWishListProduct(userID, productId);
		if (null == wishListProduct) {
			return;
		}
		wishListProductDAO.delete(wishListProduct);

	}

	@Transactional
	public List<WishListProduct> list(Long userId) {
		List<WishListProduct> wishListProducts = wishListProductDAO.getWishListProducts(userId);
		initWishlistProductListExtras(wishListProducts, userId);
		return wishListProducts;
	}

	@Transactional
	public List<WishListProduct> getFreindsActivity(User follower, Date date, int limit) {
		List<WishListProduct> wishListProducts = wishListProductDAO.getWishListActivity(follower, date, limit);
		initWishlistProductListExtras(wishListProducts, follower.getUserID());
		return wishListProducts;
	}

	@Transactional
	public List<WishListProduct> getFreindActivity(User follower, Long friendID, Date date, int limit) {
		List<WishListProduct> wishListProducts = wishListProductDAO.getWishListProducts(friendID);
		initWishlistProductListExtras(wishListProducts, follower.getUserID());
		return wishListProducts;
	}

	@Transactional
	public boolean isInWishList(Long userId, Retailer retailer, long sku) {
		return wishListProductDAO.isInWishList(userId, retailer, sku);
	}

	@Transactional
	public boolean isInWishList(Long userId, Long productId) {
		return wishListProductDAO.isInWishList(userId, productId);
	}

	private void initWishlistProductListExtras(List<WishListProduct> wishListProducts, Long loggedInUser) {
		for (WishListProduct wishListProduct : wishListProducts) {
			initWishListProductExtras(wishListProduct, loggedInUser);
		}
	}

	private void initWishListProductExtras(WishListProduct wishListProduct, Long loggedInUser) {
		if (null != wishListProduct.getLikes()) {
			Hibernate.initialize(wishListProduct.getLikes().size());
		}
		if (null != wishListProduct.getComments()) {
			Hibernate.initialize(wishListProduct.getComments().size());
		}
		if (null != loggedInUser) {
			wishListProduct.setLiked(likeDAO.likedBy(loggedInUser, wishListProduct.getId()));
		}
	}

}
