package com.everestes.socialecommerce.service.activity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.activity.ActivityDAO;
import com.everestes.socialecommerce.data.dao.activity.LikeDAO;
import com.everestes.socialecommerce.domain.activity.ActivityLike;
import com.everestes.socialecommerce.service.user.UserService;

@Service
public class LikeService {

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityDAO activityDAO;

	@Autowired
	private LikeDAO likeDAO;

	@Transactional
	public ActivityLike like(Long userId, Long activityId) {
		ActivityLike like = new ActivityLike();
		like.setLikedBy(userService.getUserById(userId));
		like.setActivity(activityDAO.getId(activityId));
		return likeDAO.merge(like);
	}

	@Transactional
	public int unlike(Long userId, Long activityId) {
		return likeDAO.unlike(userId, activityId);
	}

}
