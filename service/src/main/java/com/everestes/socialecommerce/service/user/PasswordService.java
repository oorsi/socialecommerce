package com.everestes.socialecommerce.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.user.PasswordResetCodeDAO;
import com.everestes.socialecommerce.domain.user.PasswordResetCode;

@Service
public class PasswordService {

	@Autowired
	private PasswordResetCodeDAO passwordResetCodeDAO;

	@Transactional
	public void deletePasswordResetCode(Long userID) {
		passwordResetCodeDAO.delete(userID);
	}

	@Transactional
	public PasswordResetCode savePasswordResetCode(PasswordResetCode code) {
		return passwordResetCodeDAO.merge(code);
	}

	@Transactional
	public PasswordResetCode getPasswordResetCode(String code) {
		return passwordResetCodeDAO.getPasswordResetCode(code);
	}

}
