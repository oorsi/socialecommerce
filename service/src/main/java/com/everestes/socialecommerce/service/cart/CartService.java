package com.everestes.socialecommerce.service.cart;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.cart.CartProductDAO;
import com.everestes.socialecommerce.domain.cart.CartProduct;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.service.product.ProductService;
import com.everestes.socialecommerce.service.user.UserService;

@Service
public class CartService {

	@Autowired
	private CartProductDAO cartProductDAO;

	@Autowired
	private ProductService productService;

	@Autowired
	private UserService userService;

	@Transactional
	public CartProduct addToCart(Long from, Long to, Long productId) {
		Product product = productService.getProduct(productId);
		return addToCart(from, to, product);
	}

	@Transactional
	public CartProduct addToCart(Long from, Long to, Retailer retailer, String sku) {
		Product product = productService.getProductBySku(retailer, sku, false);
		product = productService.saveProduct(product, retailer, sku);
		return addToCart(from, to, product);
	}

	@Transactional
	public CartProduct addToCart(Long from, Long to, Product product) {
		CartProduct cartProduct = cartProductDAO.getCartProduct(from, to, product.getProductId(), false, false);

		if (null == cartProduct) {
			cartProduct = new CartProduct();
			cartProduct.setFromUser(userService.getUserById(from));
			cartProduct.setForUser(userService.getUserById(to));
			cartProduct.setProduct(productService.getProduct(product.getProductId()));
			cartProduct.setQuantity(1);
			cartProduct.setDate(new Date());
		} else {
			cartProduct.setQuantity(cartProduct.getQuantity() + 1);
		}
		cartProduct.setDate(new Date());
		return cartProductDAO.merge(cartProduct);
	}

	@Transactional
	public List<CartProduct> getCart(Long from) {
		return cartProductDAO.getCart(from, false, false);
	}

	@Transactional
	public long getCartSize(Long from) {
		return cartProductDAO.getCartSize(from);
	}

	@Transactional
	public List<CartProduct> getCart(Long from, Long to) {
		return cartProductDAO.getCart(from, to, false, false);
	}

	@Transactional
	public CartProduct updateQuantity(Long fromId, Long forId, Long productId, int quantity)
			throws CartProductNotFoundException {
		CartProduct cartProduct = cartProductDAO.getCartProduct(fromId, forId, productId, false, false);
		if (null == cartProduct) {
			throw new CartProductNotFoundException();
		}
		cartProduct.setQuantity(quantity);
		return cartProductDAO.merge(cartProduct);
	}

	@Transactional
	public void deleteProductFromCart(Long fromUserId, Long toUserId, Long productId) {
		CartProduct cartProduct = cartProductDAO.getCartProduct(fromUserId, toUserId, productId, false, false);
		if (null == cartProduct) {
			return;
		}
		cartProduct.setRemoved(true);
		cartProductDAO.save(cartProduct);
	}

	@Transactional
	public void saveAll(List<CartProduct> cartProducts) {
		cartProductDAO.saveAll(cartProducts);
	}
}
