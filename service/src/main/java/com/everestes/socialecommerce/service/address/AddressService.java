package com.everestes.socialecommerce.service.address;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.address.UserAddressDAO;
import com.everestes.socialecommerce.domain.address.UserAddress;
import com.everestes.socialecommerce.service.user.UserService;

@Service
public class AddressService {

	@Autowired
	public UserAddressDAO userAddressDAO;

	@Autowired
	public UserService userService;

	@Transactional
	public UserAddress getDefaultAddress(Long userId) {
		return userAddressDAO.getDefatultAddress(userId);
	}

	@Transactional
	public UserAddress save(Long id, String name, String address1, String address2, String city, String state,
			String zip, String phoneNumber, String formattedAddress, boolean isDefault, Long userID) {
		UserAddress address = null;
		if (id != null) {
			address = getUserAddress(userID, id);
		}
		if (address == null) {
			address = new UserAddress();
		}

		address.setName(name);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		address.setPhoneNumber(phoneNumber);
		address.setUser(userService.getUserById(userID));
		address.setFormattedAddress(formattedAddress);
		if (isDefault) {
			userAddressDAO.removeDefault(userID);
			address.setDefault(isDefault);
		}
		return userAddressDAO.merge(address);
	}

	@Transactional
	public List<UserAddress> getUserAddresses(Long userID) {
		return userAddressDAO.getAddresses(userID);
	}

	@Transactional
	public UserAddress getUserAddress(Long userID, Long userAddressID) {
		return userAddressDAO.getAddress(userID, userAddressID);
	}

	@Transactional
	public void deleteAddress(Long userID, Long userAddressID) throws AddressNotFoundException {
		UserAddress address = getUserAddress(userID, userAddressID);
		if (null == address) {
			throw new AddressNotFoundException();
		}
		userAddressDAO.delete(address);
	}

	@Transactional
	public UserAddress makeDefault(Long userID, Long userAddressID) throws AddressNotFoundException {
		UserAddress address = getUserAddress(userID, userAddressID);
		if (null == address) {
			throw new AddressNotFoundException();
		}
		userAddressDAO.removeDefault(userID);
		address.setDefault(true);
		return userAddressDAO.merge(address);
	}

}
