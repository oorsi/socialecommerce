package com.everestes.socialecommerce.service.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.payment.ChargeDAO;
import com.everestes.socialecommerce.data.dao.payment.PaymentCustomerDAO;
import com.everestes.socialecommerce.domain.order.CustomerOrder;
import com.everestes.socialecommerce.domain.payment.Charge;
import com.everestes.socialecommerce.domain.payment.PaymentCustomer;
import com.everestes.socialecommerce.domain.payment.PaymentMethod;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.UserService;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.model.ExternalAccount;

@Service
public class PaymentService {

	@Value("#{environment.STRIPE_API_SECRET}")
	private String stripeKey;

	@Autowired
	public UserService userService;

	@Autowired
	public PaymentCustomerDAO paymentCustomerDAO;

	@Autowired
	public Mapper mapper;

	@Autowired
	public ChargeDAO chargeDAO;

	Logger logger = LoggerFactory.getLogger(PaymentService.class);

	@PostConstruct
	public void init() {
		Stripe.apiKey = stripeKey;
	}

	@Transactional
	public PaymentMethod savePaymentInfo(Long userId, String token, boolean isDefault) throws PaymentMethodException {

		// check if the customer is already created in stripe
		PaymentCustomer paymentCustomer = paymentCustomerDAO.getPaymentCustomer(userId);
		Customer customer = null;
		try {
			if (null != paymentCustomer) {
				customer = Customer.retrieve(paymentCustomer.getCustomerId());
			}
			if (null == customer) {
				User user = userService.getUserById(userId);
				// Create a Stripe Customer
				Map<String, Object> customerParams = new HashMap<String, Object>();
				customerParams.put("source", token);
				customerParams.put("description", user.getUserID());
				customer = Customer.create(customerParams);

				// Persist customer info to DB
				paymentCustomer = new PaymentCustomer();
				paymentCustomer.setCustomerId(customer.getId());
				paymentCustomer.setUser(user);
				paymentCustomerDAO.merge(paymentCustomer);

				return mapper.map(customer.getSources().retrieve(customer.getDefaultSource()), PaymentMethod.class);
			} else {

				Card card = customer.createCard(token);
				return mapper.map(card, PaymentMethod.class);

			}

		} catch (AuthenticationException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (InvalidRequestException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (APIConnectionException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (CardException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (APIException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		}

	}

	@Transactional
	public List<PaymentMethod> getPaymentMethods(Long userId) throws PaymentMethodException {
		// from DB
		PaymentCustomer paymentCustomer = paymentCustomerDAO.getPaymentCustomer(userId);

		List<PaymentMethod> paymentMethods = new ArrayList<>();
		if (null != paymentCustomer) {

			Map<String, Object> cardParams = new HashMap<String, Object>();
			cardParams.put("object", "card");

			try {
				List<ExternalAccount> accounts = Customer.retrieve(paymentCustomer.getCustomerId()).getSources()
						.list(cardParams).getData();
				for (ExternalAccount account : accounts) {
					if (account instanceof Card) {
						paymentMethods.add(mapper.map(account, PaymentMethod.class));
					}
				}

			} catch (AuthenticationException e) {

				logger.error("", e);
				throw new PaymentMethodException(e);
			} catch (InvalidRequestException e) {

				logger.error("", e);
				throw new PaymentMethodException(e);
			} catch (APIConnectionException e) {

				logger.error("", e);
				throw new PaymentMethodException(e);
			} catch (CardException e) {

				logger.error("", e);
				throw new PaymentMethodException(e);
			} catch (APIException e) {

				logger.error("", e);
				throw new PaymentMethodException(e);
			}

		}

		return paymentMethods;

	}

	@Transactional
	public PaymentMethod getDefaultPaymentMethod(Long userId) throws PaymentMethodException {
		PaymentCustomer paymentCustomer = paymentCustomerDAO.getPaymentCustomer(userId);

		try {
			Customer customer = Customer.retrieve(paymentCustomer.getCustomerId());
			Card card = (Card) customer.getSources().retrieve(customer.getDefaultSource());
			return mapper.map(card, PaymentMethod.class);
		} catch (AuthenticationException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (InvalidRequestException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (APIConnectionException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (CardException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		} catch (APIException e) {

			logger.error("", e);
			throw new PaymentMethodException(e);
		}
	}

	public PaymentMethod getPaymentMethod(Long userId, String id) throws PaymentMethodException {
		List<PaymentMethod> paymentMethods = getPaymentMethods(userId);
		for (PaymentMethod paymentMethod : paymentMethods) {
			if (paymentMethod.getId().equals(id)) {
				return paymentMethod;
			}
		}
		return null;
	}

	@Transactional
	public Charge chargeCustomer(CustomerOrder order, PaymentMethod paymentMethod) throws ChargePaymentMethodException {
		Map<String, Object> params = new HashMap<>();
		params.put("card", paymentMethod.getId());
		params.put("customer", paymentMethod.getCustomer());
		params.put("currency", "USD");
		params.put("amount", (int) (order.getTotalPrice() * 100));
		params.put("capture", true);
		try {
			com.stripe.model.Charge charge = com.stripe.model.Charge.create(params);
			Charge chargeObject = new Charge();
			chargeObject.setCustomerOrder(order);
			chargeObject.setId(charge.getId());
			return chargeDAO.merge(chargeObject);
		} catch (AuthenticationException e) {
			logger.error("", e);
			throw new ChargePaymentMethodException(e);
		} catch (InvalidRequestException e) {
			logger.error("", e);
			throw new ChargePaymentMethodException(e);
		} catch (APIConnectionException e) {
			logger.error("", e);
			throw new ChargePaymentMethodException(e);
		} catch (CardException e) {
			logger.error("", e);
			throw new ChargePaymentMethodException(e);
		} catch (APIException e) {
			logger.error("", e);
			throw new ChargePaymentMethodException(e);
		}
	}

	@Transactional
	public void deletePaymentMethod(Long userID, String cardID) throws ChargePaymentMethodException {
		PaymentCustomer paymentCustomer = paymentCustomerDAO.getPaymentCustomer(userID);
		if (null != paymentCustomer) {
			Customer customer;
			try {
				customer = Customer.retrieve(paymentCustomer.getCustomerId());
				customer.getSources().retrieve(cardID).delete();

			} catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException
					| APIException e) {
				e.printStackTrace();
				throw new ChargePaymentMethodException(e);
			}
		}

	}

	public void changeDefaultMethod(Long userID, String cardID) throws ChargePaymentMethodException {
		PaymentCustomer paymentCustomer = paymentCustomerDAO.getPaymentCustomer(userID);
		if (null != paymentCustomer) {
			Customer customer;
			try {
				customer = Customer.retrieve(paymentCustomer.getCustomerId());
				customer.setDefaultSource(cardID);
			} catch (AuthenticationException | InvalidRequestException | APIConnectionException | CardException
					| APIException e) {
				e.printStackTrace();
				throw new ChargePaymentMethodException(e);
			}
		}

	}

}
