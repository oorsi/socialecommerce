package com.everestes.socialecommerce.service;

import java.util.Arrays;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.everestes.socialecommerce.data.DataConfig;
import com.everestes.socialecommerce.file.upload.FileUploadConfig;
import com.everestes.socialecommerce.mail.MailConfig;
import com.everestes.socialecommerce.social.facebook.FacebookConfig;
import com.everestes.socialecommerce.ws.client.RestClientConfig;
import com.oorsi.socialshop.push.PushConfig;
import com.oorsi.socialshop.shipping.ShippingConfig;
import com.oorsi.socialshop.sms.SMSConfig;

@Configuration
@ComponentScan(basePackages = { "com.everestes.socialecommerce.service" })
@Import(value = { DataConfig.class, RestClientConfig.class, MailConfig.class, FacebookConfig.class,
		FileUploadConfig.class, ShippingConfig.class, SMSConfig.class, PushConfig.class })
public class ServiceConfig {

	@Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		configurer.setIgnoreUnresolvablePlaceholders(true);
		return configurer;
	}

	@Bean
	public Mapper mapper() {
		List<String> mappingFiles = Arrays.asList("classpath:service-mapping.xml");
		DozerBeanMapper dozerBean = new DozerBeanMapper();
		dozerBean.setMappingFiles(mappingFiles);
		return dozerBean;
	}
}
