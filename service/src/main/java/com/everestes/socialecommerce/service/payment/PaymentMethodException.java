package com.everestes.socialecommerce.service.payment;

public class PaymentMethodException extends Exception {

	private static final long serialVersionUID = 1L;

	public PaymentMethodException() {
		super();
	}

	public PaymentMethodException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PaymentMethodException(String message, Throwable cause) {
		super(message, cause);
	}

	public PaymentMethodException(String message) {
		super(message);
	}

	public PaymentMethodException(Throwable cause) {
		super(cause);
	}

}
