package com.everestes.socialecommerce.service.cart;

public class CartProductNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4588177685756702085L;

	public CartProductNotFoundException() {
		super();
	}

	public CartProductNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CartProductNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CartProductNotFoundException(String message) {
		super(message);
	}

	public CartProductNotFoundException(Throwable cause) {
		super(cause);
	}

}
