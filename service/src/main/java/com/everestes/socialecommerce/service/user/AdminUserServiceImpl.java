package com.everestes.socialecommerce.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everestes.socialecommerce.data.dao.user.UserDAO;
import com.everestes.socialecommerce.domain.user.Role;
import com.everestes.socialecommerce.domain.user.RoleType;
import com.everestes.socialecommerce.domain.user.User;

@Service("adminUserService")
public class AdminUserServiceImpl implements UserDetailsService {

	@Autowired
	@Qualifier("userDAO")
	private UserDAO userDAO;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userDAO.getUserByUsername(username);

		if (user != null) {
			for (Role role : user.getRoles()) {
				if (role.getRoleType().equals(RoleType.ADMIN) || role.getRoleType().equals(RoleType.SUPER_ADMIN)) {
					return user;
				}
			}
		}

		return null;
	}

}
