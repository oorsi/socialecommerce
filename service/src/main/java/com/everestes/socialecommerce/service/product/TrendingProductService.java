package com.everestes.socialecommerce.service.product;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everestes.socialecommerce.data.dao.catalog.TrendingProductDAO;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.catalog.TrendingProduct;

@Service
public class TrendingProductService {

	@Autowired
	private TrendingProductDAO trendingProductDAO;

	@Transactional
	public List<Product> getTrendingProducts() {
		return trendingProductDAO.findAll().stream().map(TrendingProduct::getProduct).collect(Collectors.toList());
	}

}
