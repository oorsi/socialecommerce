package com.everestes.socialecommerce.data.dao.catalog;

import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.catalog.TrendingProduct;

@Repository
public class TrendingProductDAOImpl extends BaseDAOImpl<TrendingProduct> implements TrendingProductDAO {

	protected TrendingProductDAOImpl() {
		super(TrendingProduct.class);
	}

}
