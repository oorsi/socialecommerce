package com.everestes.socialecommerce.data.dao.user;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.PasswordResetCode;

public interface PasswordResetCodeDAO extends BaseDAO<PasswordResetCode> {

	void delete(Long userId);

	PasswordResetCode getPasswordResetCode(String code);
	
	PasswordResetCode getPasswordResetCodeForUser(Long userID);

}
