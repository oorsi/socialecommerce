package com.everestes.socialecommerce.data.dao.user.phone;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.phoneNumber.PhoneNumberConfirmation;

public interface PhoneNumberConfirmationDAO extends BaseDAO<PhoneNumberConfirmation> {

	PhoneNumberConfirmation getPhoneNumberConfirmationForUser(Long userID);

}
