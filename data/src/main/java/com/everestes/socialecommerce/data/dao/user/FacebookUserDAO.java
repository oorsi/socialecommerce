package com.everestes.socialecommerce.data.dao.user;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.FacebookUser;
import com.everestes.socialecommerce.domain.user.User;

public interface FacebookUserDAO extends BaseDAO<FacebookUser> {

	FacebookUser getFacebookUserByFacebookId(String facebookId);

	List<User> getFacebookUserByFacebookId(List<String> facebookIds);

}
