package com.everestes.socialecommerce.data.dao.order;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.order.OrderProduct;

public interface OrderProductDAO extends BaseDAO<OrderProduct> {

}
