package com.everestes.socialecommerce.data.dao.user.push;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.push.PushPlayerID;

public interface PushPlayerIDDAO extends BaseDAO<PushPlayerID> {

	public void deletePlayerID(Long userID, String playerID);

}
