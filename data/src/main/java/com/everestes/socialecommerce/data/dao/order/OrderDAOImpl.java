package com.everestes.socialecommerce.data.dao.order;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.order.CustomerOrder;

@Repository
public class OrderDAOImpl extends BaseDAOImpl<CustomerOrder> implements OrderDAO {

	protected OrderDAOImpl() {
		super(CustomerOrder.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerOrder> getOrders(Long userId) {
		Query query = getSession().createQuery(
				"Select co from CustomerOrder co where co.fromUser.userID = :userID order by co.date desc");
		query.setParameter("userID", userId);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerOrder> getOpenOrders(Long userId) {
		Query query = getSession().createQuery(
				"Select co from CustomerOrder co where co.fromUser.userID = :userID order by co.date desc");
		query.setParameter("userID", userId);
		return query.list();
	}

	@Override
	public CustomerOrder getOrder(Long orderId) {
		return null;
	}

	@Override
	public CustomerOrder getOrder(Long userId, Long orderId) {
		Query query = getSession()
				.createQuery("Select co from CustomerOrder co where co.id=:orderId and co.fromUser.userID = :userID");
		query.setParameter("userID", userId);
		query.setParameter("orderId", orderId);
		return (CustomerOrder) query.uniqueResult();
	}

}
