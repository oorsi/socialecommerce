package com.everestes.socialecommerce.data.dao.user.phone;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.PhoneNumber;

@Repository
public class PhoneNumberDAOImpl extends BaseDAOImpl<PhoneNumber> implements PhoneNumberDAO {

	protected PhoneNumberDAOImpl() {
		super(PhoneNumber.class);
	}

	@Override
	public void deleteAllNumbersForUser(Long userID) {
		Query query = getSession().createQuery("delete PhoneNumber p where p.user.userID=:userID")
				.setParameter("userID", userID);
		query.executeUpdate();
	}

	@Override
	public void deleteUnconfirmedNumbersForUser(Long userID) {
		Query query = getSession()
				.createQuery("delete PhoneNumber p where p.user.userID=:userID and p.confirmed=:confirmed")
				.setParameter("userID", userID).setParameter("confirmed", false);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PhoneNumber> getUsersByPhoneNumbers(List<String> phoneNumbers) {
		Query query = getSession()
				.createQuery(
						"select p from PhoneNumber p where p.phoneNumber in :phoneNumbers and p.confirmed=:confirmed")
				.setParameter("confirmed", true).setParameterList("phoneNumbers", phoneNumbers);
		return query.list();

	}

	@Override
	public PhoneNumber getUnconfirmedPhoneNumber(Long userID) {
		Query query = getSession()
				.createQuery("from PhoneNumber p where p.user.userID=:userID and p.confirmed=:confirmed")
				.setParameter("userID", userID).setParameter("confirmed", false);
		return (PhoneNumber) query.uniqueResult();

	}

	@Override
	public PhoneNumber getconfirmedPhoneNumber(Long userID) {
		Query query = getSession()
				.createQuery("from PhoneNumber p where p.user.userID=:userID and p.confirmed=:confirmed")
				.setParameter("userID", userID).setParameter("confirmed", true);
		return (PhoneNumber) query.uniqueResult();

	}

	@Override
	public void deleteOtherPhoneNumbers(String phoneNumber, Long userID) {
		Query query = getSession().createQuery(
				"delete PhoneNumber p where p.phoneNumber=:phoneNumber and  p.user.userID != :userID and confirmed=:confirmed")
				.setParameter("phoneNumber", phoneNumber).setParameter("userID", userID)
				.setParameter("confirmed", true);
		query.executeUpdate();

	}
}
