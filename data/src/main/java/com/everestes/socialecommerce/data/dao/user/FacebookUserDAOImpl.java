package com.everestes.socialecommerce.data.dao.user;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.FacebookUser;
import com.everestes.socialecommerce.domain.user.User;

@Repository
public class FacebookUserDAOImpl extends BaseDAOImpl<FacebookUser> implements FacebookUserDAO {

	protected FacebookUserDAOImpl() {
		super(FacebookUser.class);
	}

	@Override
	public FacebookUser getFacebookUserByFacebookId(String facebookId) {
		Query query = getSession().createQuery("from FacebookUser where facebookUserID=:facebookUserID");
		query.setParameter("facebookUserID", facebookId);
		return (FacebookUser) query.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<User> getFacebookUserByFacebookId(List<String> facebookIds) {
		Query query = getSession().createQuery("select user from FacebookUser where facebookUserID in (:facebookUserIDs)");
		query.setParameterList("facebookUserIDs", facebookIds);
		return query.list();
	}

}
