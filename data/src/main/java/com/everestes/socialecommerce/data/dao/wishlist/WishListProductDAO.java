package com.everestes.socialecommerce.data.dao.wishlist;

import java.util.Date;
import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.wishlist.WishListProduct;

public interface WishListProductDAO extends BaseDAO<WishListProduct> {

	public List<WishListProduct> getWishListProducts(Long userId);

	List<WishListProduct> getWishListActivity(User follower);

	List<WishListProduct> getWishListActivity(User follower, Date time, int count);

	boolean isInWishList(Long userId, Retailer retailer, Long sku);

	WishListProduct getWishListProduct(long userID, long productId);

	boolean isInWishList(Long userId, Long productId);
}
