package com.everestes.socialecommerce.data.dao.wishlist;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.wishlist.WishListProduct;

@Repository
public class WishListProductDAOImpl extends BaseDAOImpl<WishListProduct> implements WishListProductDAO {

	protected WishListProductDAOImpl() {
		super(WishListProduct.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WishListProduct> getWishListProducts(Long userid) {
		Query query = getSession().createQuery("SELECT w from WishListProduct w where w.u.userID=:userID order by w.updated DESC");
		query.setParameter("userID", userid);
		return query.list();
	}

	@Override
	public WishListProduct getWishListProduct(long userID, long productId) {

		Query query = getSession().createQuery(
				"SELECT w from WishListProduct w where w.u.userID=:userID and w.product.productId = :productId");
		query.setParameter("userID", userID);
		query.setParameter("productId", productId);

		return (WishListProduct) query.uniqueResult();

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<WishListProduct> getWishListActivity(User follower) {
		Query query = getSession()
				.createSQLQuery(
						"select  wlp.* from WishListProduct wlp left outer join Friendship f on wlp.userId=f.followedId where f.followerId = :userID1 or wlp.userId = :userID2 order by wlp.updated desc")
				.addEntity(WishListProduct.class);
		query.setParameter("userID1", follower.getUserID());
		query.setParameter("userID2", follower.getUserID());
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<WishListProduct> getWishListActivity(User follower, Date time, int limit) {
		Query query = getSession()
				.createSQLQuery(
						"select  wlp.* from WishListProduct wlp left outer join Friendship f on wlp.userId=f.followedId where f.followerId = :userID or wlp.userId = :userID and wlp.updated >= :updatedEndDate order by wlp.updated desc limit :limit")
				.addEntity(WishListProduct.class);
		query.setParameter("userID", follower.getUserID());
		query.setParameter("limit", limit);
		query.setParameter("updatedEndDate", new Timestamp(time.getTime()));
		return query.list();
	}

	@Override
	public boolean isInWishList(Long userId, Retailer retailer, Long sku) {
		Query query = getSession().createQuery(
				"Select count(*) from WishListProduct where u.userID=:userId and product.retailer = :retailer and product.sku=:sku");
		query.setParameter("userId", userId);
		query.setParameter("retailer", retailer);
		query.setParameter("sku", sku);

		if ((long) (query.uniqueResult()) == 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean isInWishList(Long userId, Long productId) {
		Query query = getSession().createQuery(
				"Select count(*) from WishListProduct where u.userID=:userId and product.productId = :productId");
		query.setParameter("userId", userId);
		query.setParameter("productId", productId);

		if ((long) (query.uniqueResult()) == 0) {
			return false;
		} else {
			return true;
		}
	}

}
