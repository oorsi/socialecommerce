package com.everestes.socialecommerce.data.dao.payment;

import org.hibernate.Query;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.payment.PaymentCustomer;

public interface PaymentCustomerDAO extends BaseDAO<PaymentCustomer> {

	PaymentCustomer getPaymentCustomer(long userId);

}
