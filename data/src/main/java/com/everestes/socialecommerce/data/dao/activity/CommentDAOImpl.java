package com.everestes.socialecommerce.data.dao.activity;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.activity.Comment;

@Repository
public class CommentDAOImpl extends BaseDAOImpl<Comment> implements CommentDAO {

	protected CommentDAOImpl() {
		super(Comment.class);
	}

	@Override
	public Comment getComment(Long commentId) {
		Query query = getSession().createQuery("SELECT c from Comment c where c.id=:commentId ");
		query.setParameter("commentId", commentId);

		return (Comment) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentsForActivity(Long activityId) {
		Query query = getSession().createQuery("SELECT c from Comment c where c.activity.id=:activityId ");
		query.setParameter("activityId", activityId);

		return query.list();
	}

}
