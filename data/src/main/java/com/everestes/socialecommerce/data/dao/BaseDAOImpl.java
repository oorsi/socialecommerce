package com.everestes.socialecommerce.data.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseDAOImpl<T> extends AbstractDAO implements BaseDAO<T> {
	// process X records at a time during batch operations
	public static final int BATCH_LIMIT = 100;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Class<T> dtoClass;

	protected BaseDAOImpl(Class<T> dtoClass) {
		this.dtoClass = dtoClass;
	}

	public void delete(T object) {
		getSession().delete(object);
	}

	@SuppressWarnings("unchecked")
	public final T merge(T object) {
		return (T) getSession().merge(object);
	}

	public final void save(T object) {
		getSession().saveOrUpdate(object);
	}

	public final void update(T object) {
		getSession().update(object);
	}

	public final void flush() {
		getSession().flush();
	}

	@Override
	public void saveAll(List<T> objects) {
		logger.debug("Persisting " + objects.size() + " "
				+ dtoClass.getSimpleName() + " objects.");

		// retrieve the session
		Session session = getSession();

		// for all of the objects
		for (int i = 0; i < objects.size(); i++) {
			// save this object
			session.save(objects.get(i));

			// check to see if we have hit our batch limit
			if (i % BATCH_LIMIT == 0) {
				session.flush();
				session.clear();

				if (logger.isDebugEnabled()) {
					float percent = ((float) i / objects.size()) * 100;
					logger.debug(String
							.format("Batch Persist: %.2f%%", percent));
				}
			}
		}

		session.flush();
		session.clear();
	}

	@Override
	public void updateAll(List<T> objects) {
		logger.debug("Updating " + objects.size() + " "
				+ dtoClass.getSimpleName() + " objects.");

		// retrieve the session
		Session session = getSession();

		// for all of the objects
		for (int i = 0; i < objects.size(); i++) {
			// save this object
			session.update(objects.get(i));

			// check to see if we have hit our batch limit
			if (i % BATCH_LIMIT == 0) {
				session.flush();
				session.clear();

				if (logger.isDebugEnabled()) {
					float percent = ((float) i / objects.size()) * 100;
					logger.debug(String
							.format("Batch Persist: %.2f%%", percent));
				}
			}
		}

		session.flush();
		session.clear();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> mergeAll(List<T> objects) {
		// holds the list of persisted objects
		List<T> persistedObjects = new ArrayList<T>(objects.size());

		// retrieve the session
		Session session = getSession();

		// for all of the objects
		for (int i = 0; i < objects.size(); i++) {
			// save this object
			T persistedObject = (T) session.merge(objects.get(i));
			persistedObjects.add(persistedObject);

			// check to see if we have hit our batch limit
			if (i % BATCH_LIMIT == 0) {
				session.flush();
				session.clear();

				if (logger.isDebugEnabled()) {
					float percent = ((float) i / objects.size()) * 100;
					logger.debug(String
							.format("Batch Persist: %.2f%%", percent));
				}
			}
		}

		session.flush();
		session.clear();

		// return the list of objects that have been merged to the db
		return persistedObjects;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Criteria criteria = getSession().createCriteria(dtoClass);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<T> result = criteria.list();
		return result;
	}

	@SuppressWarnings("unchecked")
	public final T getId(Integer id) {
		T t = (T) getSession().get(dtoClass, id);
		return t;
	}

	@SuppressWarnings("unchecked")
	public final T getId(Long id) {
		T t = (T) getSession().get(dtoClass, id);
		return t;
	}
	
	@SuppressWarnings("unchecked")
	public final T getId(String id) {
		T t = (T) getSession().get(dtoClass, id);
		return t;
	}

	@SuppressWarnings("unchecked")
	public final T getId(Long id, String fetchProfile) {
		Session s = getSession();
		s.enableFetchProfile(fetchProfile);
		T t = (T) s.get(dtoClass, id);

		return t;
	}

}
