package com.everestes.socialecommerce.data.dao.payment;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.payment.Charge;

public interface ChargeDAO extends BaseDAO<Charge> {

}
