package com.everestes.socialecommerce.data.dao.user;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.User;

public interface UserDAO extends BaseDAO<User> {
	public User getUserByUsername(String username);

	List<User> searchByName(String firstName, String lastName, Long searchingUserId);

	List<User> searchByLastName(String lastName, Long searchingUserId);

	List<User> searchByFirstName(String firstName, Long searchingUserId);

	boolean userExists(String email);

	int getNumberOfUsers();

}
