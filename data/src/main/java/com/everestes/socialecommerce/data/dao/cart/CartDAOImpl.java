package com.everestes.socialecommerce.data.dao.cart;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.cart.Cart;

@Repository
public class CartDAOImpl extends BaseDAOImpl<Cart> implements CartDAO {

	protected CartDAOImpl() {
		super(Cart.class);
	}
	
	@Override
	public Cart getCart(Long from) {
		Query query = getSession().createQuery(
				"select cart from Cart cart where cart.fromUser.userID = :from");
		query.setParameter("from", from);
		return null;
	}

	@Override
	public Cart getCart(Long from, Long to) {
		Query query = getSession().createQuery(
				"select cart from Cart cart where cart.fromUser.userID = :from and cart.toUser.userID = :to");
		query.setParameter("from", from);
		query.setParameter("to", to);
		return null;
	}

}
