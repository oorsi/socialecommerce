package com.everestes.socialecommerce.data.dao.activity;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.activity.ActivityLike;

public interface LikeDAO extends BaseDAO<ActivityLike> {

	int unlike(long userId, long activityId);

	boolean likedBy(Long userID, Long wishlistProductId);

}
