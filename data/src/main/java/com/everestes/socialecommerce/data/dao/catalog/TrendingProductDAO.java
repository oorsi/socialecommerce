package com.everestes.socialecommerce.data.dao.catalog;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.catalog.TrendingProduct;

public interface TrendingProductDAO extends BaseDAO<TrendingProduct> {

}
