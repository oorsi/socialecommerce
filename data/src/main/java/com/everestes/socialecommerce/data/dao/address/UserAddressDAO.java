package com.everestes.socialecommerce.data.dao.address;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.address.UserAddress;

public interface UserAddressDAO extends BaseDAO<UserAddress> {

	List<UserAddress> getAddresses(Long userID);

	UserAddress getDefatultAddress(Long userID);

	UserAddress getAddress(Long userID, Long userAddressID);

	void removeDefault(Long userID);
}
