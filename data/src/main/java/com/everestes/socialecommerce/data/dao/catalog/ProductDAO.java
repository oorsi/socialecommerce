package com.everestes.socialecommerce.data.dao.catalog;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.type.Retailer;

public interface ProductDAO extends BaseDAO<Product> {

	public Product getProductByRetailerAndSku(Retailer retailer, String sku);
	
	

}
