package com.everestes.socialecommerce.data.dao.catalog;


import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.type.Retailer;

@Repository
public class ProductDAOImpl extends BaseDAOImpl<Product> implements ProductDAO {

	protected ProductDAOImpl() {
		super(Product.class);
	}

	@Override
	public Product getProductByRetailerAndSku(Retailer retailer, String sku) {
		Query query = getSession()
				.createQuery("SELECT product from Product product where retailer = :retailer and sku = :sku");
		
		query.setParameter("retailer", retailer);
		query.setParameter("sku", sku);
		return (Product) query.uniqueResult();
	}

}
