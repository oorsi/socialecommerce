package com.everestes.socialecommerce.data.dao.activity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;

@Repository
public class ActivityDAOImpl extends BaseDAOImpl<Activity> implements ActivityDAO {

	public ActivityDAOImpl() {
		super(Activity.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Activity> getWishListActivity(User follower, Date time, int limit) {
		Query query = getSession()
				.createSQLQuery(
						"select  a.* from Activity a left outer join Friendship f on a.userId=f.followedId where f.followerId = :userID or a.userId = :userID and a.updated >= :updatedEndDate order by a.updated desc limit :limit")
				.addEntity(Activity.class);
		query.setParameter("userID", follower.getUserID());
		query.setParameter("limit", limit);
		query.setParameter("updatedEndDate", new Timestamp(time.getTime()));
		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Activity> getFriendsActivity(User follower, Date fromDate, Integer limit) {

		Criteria criteria = getSession().createCriteria(Activity.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		criteria.createAlias("user", "user");
		criteria.createAlias("user.followedBy", "friendship");
		criteria.add(Restrictions.eq("friendship.follower", follower));

		if (fromDate != null) {
			criteria.add(Restrictions.gt("updated", fromDate));
		}

		criteria.addOrder(Order.desc("updated"));
		return criteria.list();

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Activity> getActivityForUser(Long userId) {
		Query query = getSession().createQuery("select a from Activity a where a.user.userID=:userId");
		query.setParameter("userId", userId);
		return query.list();

	}

}
