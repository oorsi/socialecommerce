package com.everestes.socialecommerce.data.dao.cart;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.cart.CartProduct;

@Repository
public class CartProductDAOImpl extends BaseDAOImpl<CartProduct> implements CartProductDAO {

	protected CartProductDAOImpl() {
		super(CartProduct.class);
	}

	@Override
	public CartProduct getCartProduct(Long from, Long to, Long productId, boolean purchased, boolean removed) {
		Query query = getSession().createQuery(
				"SELECT cartProduct FROM CartProduct cartProduct WHERE cartProduct.fromUser.userID = :from AND cartProduct.forUser.userID = :to AND cartProduct.product.productId = :productId and cartProduct.purchased=:purchased and cartProduct.removed=:removed");
		query.setParameter("from", from);
		query.setParameter("to", to);
		query.setParameter("productId", productId);
		query.setParameter("purchased", purchased);
		query.setParameter("removed", removed);
		return (CartProduct) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CartProduct> getCart(Long from, boolean purchased, boolean removed) {
		Query query = getSession().createQuery(
				"SELECT cartProduct FROM CartProduct cartProduct WHERE cartProduct.fromUser.userID = :from and cartProduct.purchased=:purchased and cartProduct.removed=:removed ORDER BY cartProduct.date DESC");
		query.setParameter("from", from);
		query.setParameter("purchased", purchased);
		query.setParameter("removed", removed);
		return query.list();
	}

	@Override
	public long getCartSize(Long from) {
		Query query = getSession().createQuery(
				"SELECT sum(cartProduct.quantity) FROM CartProduct cartProduct WHERE cartProduct.fromUser.userID = :from and cartProduct.purchased=:purchased and cartProduct.removed=:removed ORDER BY cartProduct.date DESC");
		query.setParameter("from", from);
		query.setParameter("purchased", false);
		query.setParameter("removed", false);
		long result;
		try {
			result = (Long) query.uniqueResult();
		} catch (NullPointerException e) {
			result = 0;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CartProduct> getCart(Long from, Long to, boolean purchased, boolean removed) {
		Query query = getSession().createQuery(
				"SELECT cartProduct FROM CartProduct cartProduct WHERE cartProduct.fromUser.userID = :from and cartProduct.forUser.userID = :to and cartProduct.purchased=:purchased and cartProduct.removed=:removed ORDER BY cartProduct.date DESC");
		query.setParameter("from", from);
		query.setParameter("to", to);
		query.setParameter("purchased", purchased);
		query.setParameter("removed", removed);
		return query.list();
	}

}
