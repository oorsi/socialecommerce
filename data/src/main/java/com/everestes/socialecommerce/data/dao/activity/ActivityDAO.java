package com.everestes.socialecommerce.data.dao.activity;

import java.util.Date;
import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;

public interface ActivityDAO extends BaseDAO<Activity> {

	List<Activity> getWishListActivity(User follower, Date time, int limit);

	List<Activity> getActivityForUser(Long userId);

	List<Activity> getFriendsActivity(User follower, Date fromDate, Integer limit);

}
