package com.everestes.socialecommerce.data.dao.payment;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.payment.PaymentCustomer;

@Repository
public class PaymentCustomerDAOImpl extends BaseDAOImpl<PaymentCustomer> implements PaymentCustomerDAO {

	protected PaymentCustomerDAOImpl() {
		super(PaymentCustomer.class);
	}

	@Override
	public PaymentCustomer getPaymentCustomer(long userId) {
		Query query = getSession().createQuery("SELECT p from PaymentCustomer p where p.user.userID=:userID");
		query.setParameter("userID", userId);
		return (PaymentCustomer) query.uniqueResult();
	}

}
