package com.everestes.socialecommerce.data.dao;

import java.util.List;

public interface BaseDAO<T> {
	void delete(T object);

	T merge(T object);

	void save(T object);

	/**
	 * See {@link org.hibernate.Session#update(Object)}.
	 */
	void update(T object);

	void flush();

	void saveAll(List<T> object);

	void updateAll(List<T> objects);

	List<T> mergeAll(List<T> object);

	List<T> findAll();

	T getId(Integer id);

	T getId(Long id);

	T getId(String id);
}
