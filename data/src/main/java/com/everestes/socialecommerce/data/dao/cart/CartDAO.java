package com.everestes.socialecommerce.data.dao.cart;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.cart.Cart;

public interface CartDAO extends BaseDAO<Cart> {

	public Cart getCart(Long from, Long to);

	Cart getCart(Long from);
}
