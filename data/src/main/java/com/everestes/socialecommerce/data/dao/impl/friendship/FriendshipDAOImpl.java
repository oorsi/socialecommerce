package com.everestes.socialecommerce.data.dao.impl.friendship;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.relationship.Friendship;

@Repository
public class FriendshipDAOImpl extends BaseDAOImpl<Friendship> implements FriendshipDAO {
	public FriendshipDAOImpl() {
		super(Friendship.class);
	}

	@Override
	public long getNumberOfFollowers(long userID) {
		Query query = getSession().createQuery("select count(*) from Friendship where followed.userID = :userID");
		query.setParameter("userID", userID);
		return (long) query.uniqueResult();
	}

	@Override
	public long getNumberOfFollowing(long userID) {
		Query query = getSession().createQuery("select count(*) from Friendship where follower.userID = :userID");
		query.setParameter("userID", userID);
		return (long) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getFriends(long userID) {
		Query query = getSession().createQuery("select f.followed from Friendship f where f.follower.userID = :userID order by f.created desc");
		query.setParameter("userID", userID);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getFollowers(long userID) {
		Query query = getSession().createQuery("select f.follower from Friendship f where f.followed.userID = :userID  order by f.created desc");
		query.setParameter("userID", userID);
		return query.list();
	}

	@Override
	public void unfollow(Long followerID, Long followedID) {
		Query query = getSession().createQuery(
				"delete from Friendship f where f.follower.userID = :followerID and f.followed.userID = :followedID");
		query.setParameter("followerID", followerID);
		query.setParameter("followedID", followedID);
		query.executeUpdate();
	}

	@Override
	public Friendship getFriendship(long followerUserID, long followedUserID) {
		Query query = getSession().createQuery(
				"select f from Friendship f where f.follower.userID = :followerUserID and f.followed.userID = :followedUserID");
		query.setParameter("followerUserID", followerUserID);
		query.setParameter("followedUserID", followedUserID);
		return (Friendship) query.uniqueResult();
	}

	@Override
	public boolean isFollowing(long followerId, long followedId) {
		Query query = getSession().createQuery(
				"select count(f) from Friendship f where f.follower.userID = :followerUserID and f.followed.userID = :followedUserID");
		query.setParameter("followerUserID", followerId);
		query.setParameter("followedUserID", followedId);
		long count = (long) query.uniqueResult();
		if (0 == count) {
			return false;
		} else {
			return true;
		}
	}

}
