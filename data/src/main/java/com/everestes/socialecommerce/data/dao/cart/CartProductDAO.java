package com.everestes.socialecommerce.data.dao.cart;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.cart.CartProduct;

public interface CartProductDAO extends BaseDAO<CartProduct> {

	CartProduct getCartProduct(Long from, Long to, Long productId, boolean purchased, boolean removed);

	List<CartProduct> getCart(Long from, boolean purchased, boolean removed);

	List<CartProduct> getCart(Long from, Long to, boolean purchased, boolean removed);

	long getCartSize(Long from);

}
