package com.everestes.socialecommerce.data.dao;

public class GenericDAOImpl extends BaseDAOImpl<Object> implements GenericDAO {
	public GenericDAOImpl() {
		super(Object.class);
	}
}
