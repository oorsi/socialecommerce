package com.everestes.socialecommerce.data.dao.activity;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.activity.Comment;

public interface CommentDAO extends BaseDAO<Comment> {

	Comment getComment(Long commentId);

	List<Comment> getCommentsForActivity(Long activityId);

}
