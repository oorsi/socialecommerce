package com.everestes.socialecommerce.data.dao.impl.friendship;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.relationship.Friendship;

public interface FriendshipDAO extends BaseDAO<Friendship> {
	public List<User> getFriends(long userID);

	public Friendship getFriendship(long followerUserID, long followedUserID);

	long getNumberOfFollowing(long userID);

	long getNumberOfFollowers(long userID);

	void unfollow(Long followerID, Long followedID);

	boolean isFollowing(long followerId, long followedId);

	List<User> getFollowers(long userID);
}
