package com.everestes.socialecommerce.data.dao.user;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.User;

@Repository("userDAO")
public class UserDAOImpl extends BaseDAOImpl<User> implements UserDAO {

	protected UserDAOImpl() {
		super(User.class);
	}

	@Override
	public User getUserByUsername(String username) {
		Query query = getSession().createQuery("select user from User user where user.email = :username");
		query.setParameter("username", username.toLowerCase().trim());
		User user = (User) query.uniqueResult();
		return user;
	}

	@Override
	public boolean userExists(String email) {
		Query query = getSession().createQuery("select count(userID) from User user where user.email = :email");
		query.setParameter("email", email.toLowerCase().trim());
		long count = (long) query.uniqueResult();
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchByFirstName(String firstName, Long searchingUserId) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.like("firstName", firstName + "%").ignoreCase());
		criteria.add(Restrictions.ne("userID", searchingUserId));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchByLastName(String lastName, Long searchingUserId) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.like("lastName", lastName + "%").ignoreCase());
		criteria.add(Restrictions.ne("userID", searchingUserId));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchByName(String firstName, String lastName, Long searchingUserId) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.like("firstName", firstName + "%").ignoreCase());
		criteria.add(Restrictions.like("lastName", lastName + "%").ignoreCase());
		criteria.add(Restrictions.ne("userID", searchingUserId));
		return criteria.list();
	}

	@Override
	public int getNumberOfUsers() {
		Query query = getSession().createQuery("Select count(*) from User");
		return (int) query.uniqueResult();
	}

}
