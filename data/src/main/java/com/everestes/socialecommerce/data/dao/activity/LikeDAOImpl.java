package com.everestes.socialecommerce.data.dao.activity;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.activity.ActivityLike;

@Repository
public class LikeDAOImpl extends BaseDAOImpl<ActivityLike> implements LikeDAO {

	protected LikeDAOImpl() {
		super(ActivityLike.class);
	}

	@Override
	public int unlike(long userId, long activityId) {

		Query query = getSession().createQuery(
				"delete from ActivityLike where likedBy.userID=:likedBy and activity.id=:activityId");

		query.setParameter("likedBy", userId);
		query.setParameter("activityId", activityId);

		return query.executeUpdate();

	}

	@Override
	public boolean likedBy(Long userID, Long activityId) {
		Query query = getSession().createQuery(
				"Select count(al.id) from ActivityLike al where al.activity.id=:activityId and al.likedBy.userID=:userID");
		query.setParameter("userID", userID);
		query.setParameter("activityId", activityId);

		long count = (long) query.uniqueResult();
		if (0 == count) {
			return false;
		} else {
			return true;
		}
	}

}
