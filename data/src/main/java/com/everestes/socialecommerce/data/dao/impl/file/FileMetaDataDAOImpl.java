package com.everestes.socialecommerce.data.dao.impl.file;

import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.file.FileMetaData;

@Repository
public class FileMetaDataDAOImpl extends BaseDAOImpl<FileMetaData> implements FileMetaDataDAO {

	public FileMetaDataDAOImpl() {
		super(FileMetaData.class);
	}

}
