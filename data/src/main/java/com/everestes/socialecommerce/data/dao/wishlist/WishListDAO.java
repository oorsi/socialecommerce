package com.everestes.socialecommerce.data.dao.wishlist;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.wishlist.WishList;

public interface WishListDAO extends BaseDAO<WishList> {
}
