package com.everestes.socialecommerce.data.dao.user.phone;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.phoneNumber.PhoneNumberConfirmation;

@Repository
public class PhoneNumberConfirmationDAOImpl extends BaseDAOImpl<PhoneNumberConfirmation>
		implements PhoneNumberConfirmationDAO {

	protected PhoneNumberConfirmationDAOImpl() {
		super(PhoneNumberConfirmation.class);
	}

	@Override
	public PhoneNumberConfirmation getPhoneNumberConfirmationForUser(Long userID) {
		Query query = getSession().createQuery("from PhoneNumberConfirmation where phoneNumber.user.userID=:userId")
				.setParameter("userId", userID);

		return (PhoneNumberConfirmation) query.uniqueResult();
	}

}
