package com.everestes.socialecommerce.data.dao.address;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.address.UserAddress;

@Repository
public class UserAddressDAOImpl extends BaseDAOImpl<UserAddress> implements UserAddressDAO {

	protected UserAddressDAOImpl() {
		super(UserAddress.class);
	}

	@Override
	public UserAddress getDefatultAddress(Long userID) {
		Query query = getSession()
				.createQuery("select u from UserAddress u where u.user.id = :userID and isDefault=:isDefault");
		query.setParameter("userID", userID);
		query.setParameter("isDefault", true);
		return (UserAddress) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserAddress> getAddresses(Long userID) {
		Query query = getSession().createQuery("select u from UserAddress u where u.user.id = :userID");
		query.setParameter("userID", userID);
		return query.list();
	}

	@Override
	public UserAddress getAddress(Long userID, Long userAddressID) {
		Query query = getSession()
				.createQuery("select u from UserAddress u where u.user.id = :userID and u.id = :userAddressID");
		query.setParameter("userID", userID);
		query.setParameter("userAddressID", userAddressID);
		return (UserAddress) query.uniqueResult();
	}

	@Override
	public void removeDefault(Long userID) {
		Query query = getSession()
				.createQuery("update UserAddress u set u.isDefault = :isDefault where u.user.id = :userID");
		query.setParameter("userID", userID);
		query.setParameter("isDefault", false);
		query.executeUpdate();
	}

}
