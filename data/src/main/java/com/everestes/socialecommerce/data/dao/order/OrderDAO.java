package com.everestes.socialecommerce.data.dao.order;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.order.CustomerOrder;

public interface OrderDAO extends BaseDAO<CustomerOrder> {

	List<CustomerOrder> getOrders(Long userId);

	@PreAuthorize("hasRole('ADMIN')")
	CustomerOrder getOrder(Long orderId);

	CustomerOrder getOrder(Long userId, Long orderId);

	List<CustomerOrder> getOpenOrders(Long userId);
}
