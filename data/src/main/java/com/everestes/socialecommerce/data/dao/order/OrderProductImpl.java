package com.everestes.socialecommerce.data.dao.order;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.order.OrderProduct;

public class OrderProductImpl extends BaseDAOImpl<OrderProduct> implements OrderProductDAO {

	protected OrderProductImpl() {
		super(OrderProduct.class);
	}

}
