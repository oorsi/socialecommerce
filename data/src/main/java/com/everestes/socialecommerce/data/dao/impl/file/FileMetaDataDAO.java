package com.everestes.socialecommerce.data.dao.impl.file;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.file.FileMetaData;

public interface FileMetaDataDAO extends BaseDAO<FileMetaData> {

}
