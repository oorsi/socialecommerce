package com.everestes.socialecommerce.data.dao.user;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.PasswordResetCode;

@Repository
public class PasswordResetCodeDAOImpl extends BaseDAOImpl<PasswordResetCode> implements PasswordResetCodeDAO {

	protected PasswordResetCodeDAOImpl() {
		super(PasswordResetCode.class);
	}

	@Override
	public void delete(Long userId) {
		Query query = getSession().createQuery("delete from PasswordResetCode where user.userID=:userID");
		query.setParameter("userID", userId);
		query.executeUpdate();
	}

	@Override
	public PasswordResetCode getPasswordResetCode(String code) {
		Query query = getSession().createQuery("select prc from PasswordResetCode prc where prc.code=:code");
		query.setParameter("code", code);
		return (PasswordResetCode) query.uniqueResult();
	}

	@Override
	public PasswordResetCode getPasswordResetCodeForUser(Long userID) {
		Query query = getSession().createQuery("select prc from PasswordResetCode prc where prc.user.userID=:userID");
		query.setParameter("userID", userID);
		return (PasswordResetCode) query.uniqueResult();
	}

}
