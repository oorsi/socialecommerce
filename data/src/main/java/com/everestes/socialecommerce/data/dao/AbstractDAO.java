package com.everestes.socialecommerce.data.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractDAO {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Returns the current session to access the repository
	 * 
	 * @return
	 */
	protected synchronized final Session getSession() {
		// retrieve the current hibernate session
		return sessionFactory.getCurrentSession();
	}

}
