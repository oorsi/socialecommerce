package com.everestes.socialecommerce.data.dao.payment;

import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.payment.Charge;

@Repository
public class ChargeDAOImpl extends BaseDAOImpl<Charge> implements ChargeDAO {

	protected ChargeDAOImpl() {
		super(Charge.class);
	}

}
