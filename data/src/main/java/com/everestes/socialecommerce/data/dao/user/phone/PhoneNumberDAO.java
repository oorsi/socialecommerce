package com.everestes.socialecommerce.data.dao.user.phone;

import java.util.List;

import com.everestes.socialecommerce.data.dao.BaseDAO;
import com.everestes.socialecommerce.domain.user.PhoneNumber;

public interface PhoneNumberDAO extends BaseDAO<PhoneNumber> {

	PhoneNumber getUnconfirmedPhoneNumber(Long userID);

	void deleteUnconfirmedNumbersForUser(Long userID);

	void deleteAllNumbersForUser(Long userID);

	List<PhoneNumber> getUsersByPhoneNumbers(List<String> phoneNumbers);

	PhoneNumber getconfirmedPhoneNumber(Long userID);

	void deleteOtherPhoneNumbers(String phoneNumber, Long userID);

}
