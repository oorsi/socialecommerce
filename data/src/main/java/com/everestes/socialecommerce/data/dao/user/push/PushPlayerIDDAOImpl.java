package com.everestes.socialecommerce.data.dao.user.push;

import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.user.push.PushPlayerID;

@Repository
public class PushPlayerIDDAOImpl extends BaseDAOImpl<PushPlayerID> implements PushPlayerIDDAO {

	protected PushPlayerIDDAOImpl() {
		super(PushPlayerID.class);
	}

	@Override
	public void deletePlayerID(Long userID, String playerID) {
		getSession().createQuery("DELETE FROM PushPlayerID where playerID=:playerID and user.userID=:userID")
				.setParameter("playerID", userID).setParameter("playerID", playerID).executeUpdate();

	}

}
