package com.everestes.socialecommerce.data.dao.wishlist;

import org.springframework.stereotype.Repository;

import com.everestes.socialecommerce.data.dao.BaseDAOImpl;
import com.everestes.socialecommerce.domain.wishlist.WishList;

@Repository
public class WishListDAOImpl extends BaseDAOImpl<WishList> implements WishListDAO {

	protected WishListDAOImpl() {
		super(WishList.class);
	}

}
