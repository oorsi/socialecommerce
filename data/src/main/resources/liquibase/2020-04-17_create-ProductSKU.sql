CREATE TABLE `ProductSKU` (
  `id` bigint(20) NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
	`retailer` varchar(255) DEFAULT NULL,
    `url` varchar(255) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);