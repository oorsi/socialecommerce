
-- MySQL dump 10.13  Distrib 5.7.29, for macos10.14 (x86_64)
--
-- Host: localhost    Database: social_ecomm
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Activity`
--

DROP TABLE IF EXISTS `Activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Activity` (
  `activityType` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `forUserId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1guilaaj9j21nx823tvj0rde7` (`forUserId`),
  KEY `FKm9rjschmg38muvvdspqeto7cc` (`userId`),
  CONSTRAINT `FK1guilaaj9j21nx823tvj0rde7` FOREIGN KEY (`forUserId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_h54htld06sgewo1vnlt16ok7s` FOREIGN KEY (`forUserId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_pnqhpn65eaugs8gu80ajy1qf9` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKm9rjschmg38muvvdspqeto7cc` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ActivityLike`
--

DROP TABLE IF EXISTS `ActivityLike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ActivityLike` (
  `id` bigint(20) NOT NULL,
  `activityId` bigint(20) NOT NULL,
  `likedById` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mnigm6bq92eqrjp0pptmb3tsn` (`activityId`,`likedById`),
  UNIQUE KEY `UKmnigm6bq92eqrjp0pptmb3tsn` (`activityId`,`likedById`),
  KEY `FKtpio8dtd87oyldd725dloblre` (`likedById`),
  CONSTRAINT `FK7aunmotsdyq7pia0gui5kwy63` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FK_2pux7nnlvxwstg0agtaiwrvco` FOREIGN KEY (`likedById`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_j71b208h0ml285njv5df7d7sm` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FK_jne2pj0e2bd8t9sj0ufkp81ek` FOREIGN KEY (`activityId`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKcq3b7inwm2x7bbqy54paetbop` FOREIGN KEY (`activityId`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKtpio8dtd87oyldd725dloblre` FOREIGN KEY (`likedById`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CartProduct`
--

DROP TABLE IF EXISTS `CartProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CartProduct` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `purchased` bit(1) NOT NULL,
  `quantity` int(11) NOT NULL,
  `removed` bit(1) NOT NULL,
  `forUserId` bigint(20) NOT NULL,
  `fromUserId` bigint(20) NOT NULL,
  `productId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK2y2p1la2wmxarsaqgjou9evlb` (`fromUserId`,`forUserId`,`productId`),
  KEY `FKkxi5724uyv4i68o93wehvgxlw` (`forUserId`),
  KEY `FK5251yk3c6fvvp2dl94u1dwl7b` (`productId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Charge`
--

DROP TABLE IF EXISTS `Charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Charge` (
  `id` varchar(255) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhw4boooei5pfmh2y2beb6wcrv` (`orderId`),
  CONSTRAINT `FK_47xcs5107vtpxe1hvbjm4ufeq` FOREIGN KEY (`orderId`) REFERENCES `CustomerOrder` (`id`),
  CONSTRAINT `FKhw4boooei5pfmh2y2beb6wcrv` FOREIGN KEY (`orderId`) REFERENCES `CustomerOrder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `comment` varchar(255) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `activitytId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpchbwdo4krxpa6wlkbyt2wox8` (`activitytId`),
  CONSTRAINT `FK_efqbpcxw2bbqrq93yvvkswr7e` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FK_h8tur8t6erokb3cow6ogwd5bj` FOREIGN KEY (`activitytId`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKglj76fadehkw5k055h22hp05w` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKpchbwdo4krxpa6wlkbyt2wox8` FOREIGN KEY (`activitytId`) REFERENCES `Activity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CustomerOrder`
--

DROP TABLE IF EXISTS `CustomerOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CustomerOrder` (
  `date` datetime NOT NULL,
  `fulfilled` bit(1) NOT NULL,
  `shipTo` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL,
  `forUserId` bigint(20) NOT NULL,
  `fromUserId` bigint(20) NOT NULL,
  `shippingAddressId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gayin97mnv7f1vx5s2nbdsuav` (`forUserId`),
  KEY `FKtrecuwumn0fxg0vsaj974xl2p` (`fromUserId`),
  KEY `FK3aut8aviea873derrmijny6s1` (`shippingAddressId`),
  CONSTRAINT `FK3aut8aviea873derrmijny6s1` FOREIGN KEY (`shippingAddressId`) REFERENCES `UserAddress` (`id`),
  CONSTRAINT `FK_2q0bs35vec7xgxtvheqmoorkc` FOREIGN KEY (`shippingAddressId`) REFERENCES `UserAddress` (`id`),
  CONSTRAINT `FK_b8bh7uu269xbbigxjcwvp2097` FOREIGN KEY (`fromUserId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_gayin97mnv7f1vx5s2nbdsuav` FOREIGN KEY (`forUserId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_if0aj1xve7t3i788oe535pmvc` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKqvxvc4685nfnbfbxy69vy6cfu` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKtrecuwumn0fxg0vsaj974xl2p` FOREIGN KEY (`fromUserId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DATABASECHANGELOGLOCK`
--

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FacebookUser`
--

DROP TABLE IF EXISTS `FacebookUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FacebookUser` (
  `userID` bigint(20) NOT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `facebookUserID` varchar(255) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `UK_3isb2mse3f94qi21pg2x2rtc9` (`facebookUserID`),
  UNIQUE KEY `UK3isb2mse3f94qi21pg2x2rtc9` (`facebookUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FileMetaData`
--

DROP TABLE IF EXISTS `FileMetaData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileMetaData` (
  `fileID` bigint(20) NOT NULL AUTO_INCREMENT,
  `directory` varchar(255) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Friendship`
--

DROP TABLE IF EXISTS `Friendship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Friendship` (
  `confirmed` bit(1) NOT NULL,
  `id` bigint(20) NOT NULL,
  `followedId` bigint(20) DEFAULT NULL,
  `followerId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_7qat53gjw1o74uolrxwe654wk` (`followerId`,`followedId`),
  UNIQUE KEY `UK7qat53gjw1o74uolrxwe654wk` (`followerId`,`followedId`),
  KEY `FKhaodx6hspxqxj49k6frnb0xxq` (`followedId`),
  CONSTRAINT `FK5k13muabdc3suj9u52dnmcy54` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FK75cyejwovomlw1dxfiue548g0` FOREIGN KEY (`followerId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_acva3ab1b41spc4w0nfgj27d4` FOREIGN KEY (`followerId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_ikrhib7h4ng7aweoaoj45yy39` FOREIGN KEY (`followedId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_pf22aabx2pwwognuty204nv40` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FKhaodx6hspxqxj49k6frnb0xxq` FOREIGN KEY (`followedId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OrderProduct`
--

DROP TABLE IF EXISTS `OrderProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderProduct` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `soldPrice` float NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `productId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8hho7q4fb7jq0huci8pwcnl52` (`orderId`),
  KEY `FK2r2tw2i9pstmie0utlgo6sef5` (`productId`),
  CONSTRAINT `FK2r2tw2i9pstmie0utlgo6sef5` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`),
  CONSTRAINT `FK8hho7q4fb7jq0huci8pwcnl52` FOREIGN KEY (`orderId`) REFERENCES `CustomerOrder` (`id`),
  CONSTRAINT `FK_863bh92tqxxsovjhfva65owh9` FOREIGN KEY (`orderId`) REFERENCES `CustomerOrder` (`id`),
  CONSTRAINT `FK_jbpmqoadeaf7yf23ht1nj3js0` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PasswordResetCode`
--

DROP TABLE IF EXISTS `PasswordResetCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PasswordResetCode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `expiryDate` datetime NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgx5j90rtithnsfg7d9lru4wmd` (`userId`),
  CONSTRAINT `FK_reoaavlmx6s5o0jjbuadglxge` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKgx5j90rtithnsfg7d9lru4wmd` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PaymentCustomer`
--

DROP TABLE IF EXISTS `PaymentCustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaymentCustomer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customerId` varchar(255) NOT NULL,
  `isDefault` bit(1) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_re3sqxc1uhviu8lwwbncn7ur8` (`userId`),
  UNIQUE KEY `UKre3sqxc1uhviu8lwwbncn7ur8` (`userId`),
  CONSTRAINT `FK_re3sqxc1uhviu8lwwbncn7ur8` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKmu2kcm69k51krd4fnqxtiswer` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PhoneNumber`
--

DROP TABLE IF EXISTS `PhoneNumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PhoneNumber` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `confirmed` bit(1) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKol155m0t9w9larmxidcxrehx5` (`userId`),
  CONSTRAINT `FK_7cpa44ledwhkkv0csan3u1l56` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKol155m0t9w9larmxidcxrehx5` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PhoneNumberConfirmation`
--

DROP TABLE IF EXISTS `PhoneNumberConfirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PhoneNumberConfirmation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `confirmation` varchar(255) DEFAULT NULL,
  `numberOfTries` int(11) NOT NULL,
  `phoneNumberID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbu7k6c9m8whlrn00enej9mmnd` (`phoneNumberID`),
  CONSTRAINT `FK_oksalv0mtv6kn2gmqu345h6x7` FOREIGN KEY (`phoneNumberID`) REFERENCES `PhoneNumber` (`id`),
  CONSTRAINT `FKbu7k6c9m8whlrn00enej9mmnd` FOREIGN KEY (`phoneNumberID`) REFERENCES `PhoneNumber` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `productId` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `largeFrontImage` varchar(255) DEFAULT NULL,
  `largeImage` varchar(255) DEFAULT NULL,
  `longDescription` longtext,
  `manufacturer` varchar(255) DEFAULT NULL,
  `mediumImage` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `onSale` bit(1) NOT NULL,
  `onlineAvailability` bit(1) NOT NULL,
  `regularPrice` float DEFAULT NULL,
  `retailer` varchar(255) DEFAULT NULL,
  `salePrice` float DEFAULT NULL,
  `shortDescription` longtext,
  `sku` varchar(20) DEFAULT NULL,
  `thumbnailImage` varchar(255) DEFAULT NULL,
  `upc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProductImageURL`
--

DROP TABLE IF EXISTS `ProductImageURL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductImageURL` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `imageURL` varchar(255) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtolpau4wdxrp5mhe1t8nknpqo` (`productId`),
  CONSTRAINT `FK_rfis450fxjs0n8fotiynckg9a` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`),
  CONSTRAINT `FKtolpau4wdxrp5mhe1t8nknpqo` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `PushPlayerID`
--

DROP TABLE IF EXISTS `PushPlayerID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PushPlayerID` (
  `playerID` varchar(255) NOT NULL,
  `userID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`playerID`),
  KEY `FKugpud0o3c6rnje6gt9irp58e` (`userID`),
  CONSTRAINT `FK_hk31nf222wgmg647gk6y7n18e` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKugpud0o3c6rnje6gt9irp58e` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleType` varchar(255) NOT NULL,
  `userID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKovm2mpb5majcfeag8s8p19ghw` (`userID`),
  CONSTRAINT `FK_jmeen4wh30e2mfd7bp6ieowtn` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  CONSTRAINT `FKovm2mpb5majcfeag8s8p19ghw` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `userID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RegistrationDate` datetime DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `passwordChangeDate` datetime DEFAULT NULL,
  `profPicID` bigint(20) DEFAULT NULL,
  `passwordAutoGenerated` bit(1) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `UK_e6gkqunxajvyxl5uctpl2vl2p` (`email`),
  KEY `FK23lvlkggv6rb29dccj9at7xj0` (`profPicID`),
  CONSTRAINT `FK23lvlkggv6rb29dccj9at7xj0` FOREIGN KEY (`profPicID`) REFERENCES `FileMetaData` (`fileID`),
  CONSTRAINT `FK_3vdl8uksleog9hxl0yfa76ex9` FOREIGN KEY (`profPicID`) REFERENCES `FileMetaData` (`fileID`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserAddress`
--

DROP TABLE IF EXISTS `UserAddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserAddress` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `formattedAddress` varchar(255) DEFAULT NULL,
  `isDefault` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `userID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK43vltgt39ablkjqat03v4lml8` (`userID`),
  CONSTRAINT `FK43vltgt39ablkjqat03v4lml8` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_6hhty5yg4a3dhpi8h3q66mtgg` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WishListProduct`
--

DROP TABLE IF EXISTS `WishListProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WishListProduct` (
  `id` bigint(20) NOT NULL,
  `productId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_7migj5a4v614bek3i0tx51dwm` (`userId`,`productId`),
  UNIQUE KEY `UK7migj5a4v614bek3i0tx51dwm` (`userId`,`productId`),
  KEY `FKpuol5rgw7tiffeugp8wpylabh` (`productId`),
  CONSTRAINT `FK9f3yqnv12801ueit58utujxf0` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_4uhps4ginu436i6lpjplu4sug` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`),
  CONSTRAINT `FK_5quv1hff3tfm2e0135gr1svca` FOREIGN KEY (`userId`) REFERENCES `User` (`userID`),
  CONSTRAINT `FK_8lnwn3f7da9qrvsy30ni484yg` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`),
  CONSTRAINT `FKpuol5rgw7tiffeugp8wpylabh` FOREIGN KEY (`productId`) REFERENCES `Product` (`productId`),
  CONSTRAINT `FKqchu4bpp22kfee48wurghlvlw` FOREIGN KEY (`id`) REFERENCES `Activity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-11 15:08:02
