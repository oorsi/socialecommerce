package com.everestes.socialecommerce.api.model.user;

public class FollowUnfollowRequest {
	private Long userID;

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}
}
