package com.everestes.socialecommerce.api.controller.order;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.order.OrderSubmitModel;
import com.everestes.socialecommerce.api.model.order.OrderView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.order.NoPaymentMethodException;
import com.everestes.socialecommerce.service.order.NoUserAddressException;
import com.everestes.socialecommerce.service.order.OrderService;
import com.everestes.socialecommerce.service.payment.ChargePaymentMethodException;
import com.everestes.socialecommerce.service.payment.PaymentMethodException;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private Mapper mapper;

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public OrderView submitOrder(@AuthenticationPrincipal @Validated User activeUser,
			@RequestBody OrderSubmitModel order) throws ChargePaymentMethodException, NoUserAddressException,
			NoPaymentMethodException, PaymentMethodException {
		return mapper.map(orderService.submitOrder(activeUser.getUserID(), order.getForUser(), order.getShipTo(),
				order.getShippingAddress(), order.getPaymentMethod()), OrderView.class);
	}

	@RequestMapping("/all")
	public List<OrderView> getCustomerOrders(@AuthenticationPrincipal User activeUser) {

		return DozerHelper.map(mapper, orderService.getCustomerOrders(activeUser.getUserID()), OrderView.class);
	}

	@RequestMapping("/id/{orderId}")
	public OrderView getCustomerOrder(@AuthenticationPrincipal User activeUser, @PathVariable Long orderId) {
		return mapper.map(orderService.getCustomerOrder(activeUser.getUserID(), orderId), OrderView.class);
	}

}
