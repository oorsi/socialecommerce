package com.everestes.socialecommerce.api.model.user;

import org.hibernate.validator.constraints.NotEmpty;

public class ResetPasswordForm {

	@NotEmpty
	private String token;
	@NotEmpty
	private String password;
	@NotEmpty
	private String confirmPassword;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
