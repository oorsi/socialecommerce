package com.everestes.socialecommerce.api.controller.news;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.activity.ActivityService;

@Controller
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private ActivityService activityService;

	@Value("${ui.page.items.number}")
	private Integer limit;

	@RequestMapping("/json")
	public @ResponseBody List<Activity> newsJson(@AuthenticationPrincipal User activeUser,
			@RequestParam(required = false) Long fromDate) {
		Date date = null;
		if (fromDate != null)
			date = Date.from(Instant.ofEpochMilli(fromDate));

		return activityService.getFreindsActivity(activeUser, date, null);
	}

}
