package com.everestes.socialecommerce.api.model.user;

public class FacebookLoginResponse {

	private FacebookLoginStatus status;
	private JwtAuthenticationResponse response;

	private String firstName;
	private String lastName;

	public FacebookLoginStatus getStatus() {
		return status;
	}

	public void setStatus(FacebookLoginStatus status) {
		this.status = status;
	}

	public JwtAuthenticationResponse getResponse() {
		return response;
	}

	public void setResponse(JwtAuthenticationResponse response) {
		this.response = response;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
