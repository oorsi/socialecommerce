package com.everestes.socialecommerce.api.controller.phoneNumber;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.phone.PhoneNumberService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/phoneNumber")
public class PhoneNumberController {

	@Autowired
	private PhoneNumberService phoneNumberService;

	@Autowired
	private Mapper mapper;

	@RequestMapping("/search")
	public List<com.everestes.socialecommerce.api.model.user.PhoneNumber> checkPhoneNumbers(
			@AuthenticationPrincipal User activeUser, @RequestBody List<String> phoneNumbers) {
		return DozerHelper.map(mapper, phoneNumberService.searchPhoneNumber(activeUser.getUserID(), phoneNumbers),
				com.everestes.socialecommerce.api.model.user.PhoneNumber.class);
	}

	@RequestMapping("/unconfirmed")
	public com.everestes.socialecommerce.api.model.user.PhoneNumber getUnconfirmedPhoneNumber(
			@AuthenticationPrincipal User activeUser) {
		return mapper.map(phoneNumberService.getUnconfirmedPhoneNumber(activeUser.getUserID()),
				com.everestes.socialecommerce.api.model.user.PhoneNumber.class);
	}

	@RequestMapping("/hasPhoneNumber")
	public boolean hasPhoneNumber(@AuthenticationPrincipal User activeUser) {
		return phoneNumberService.hasPhoneNumber(activeUser.getUserID());
	}
}
