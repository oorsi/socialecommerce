package com.everestes.socialecommerce.api.model.cart;

public class CartProductRequest {
	private Long to;
	private Long productId;
	private Integer retailer;
	private String sku;

	public Long getTo() {
		return to;
	}

	public void setTo(Long to) {
		this.to = to;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getRetailer() {
		return retailer;
	}

	public void setRetailer(Integer retailer) {
		this.retailer = retailer;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
}
