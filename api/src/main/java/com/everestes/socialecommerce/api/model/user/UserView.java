package com.everestes.socialecommerce.api.model.user;

public class UserView {

	private long userID;
	private String firstName;
	private String lastName;
	private String profilePictureName;

	private long numberOfFollowers;
	private long numberOfFollowed;

	private boolean followed;

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfilePictureName() {
		return profilePictureName;
	}

	public void setProfilePictureName(String profilePictureName) {
		this.profilePictureName = profilePictureName;
	}

	public long getNumberOfFollowers() {
		return numberOfFollowers;
	}

	public void setNumberOfFollowers(long numberOfFollowers) {
		this.numberOfFollowers = numberOfFollowers;
	}

	public long getNumberOfFollowed() {
		return numberOfFollowed;
	}

	public void setNumberOfFollowed(long numberOfFollowed) {
		this.numberOfFollowed = numberOfFollowed;
	}

	public boolean isFollowed() {
		return followed;
	}

	public void setFollowed(boolean followed) {
		this.followed = followed;
	}

}
