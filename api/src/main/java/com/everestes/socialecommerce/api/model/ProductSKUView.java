package com.everestes.socialecommerce.api.model;

import com.everestes.socialecommerce.domain.type.Retailer;

public class ProductSKUView {

	private String sku;
	private Retailer retailer;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

}
