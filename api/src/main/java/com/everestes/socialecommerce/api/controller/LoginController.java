package com.everestes.socialecommerce.api.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.JwtTokenUtil;
import com.everestes.socialecommerce.api.model.EmptyJSONResponse;
import com.everestes.socialecommerce.api.model.FieldError;
import com.everestes.socialecommerce.api.model.user.JwtAuthenticationRequest;
import com.everestes.socialecommerce.api.model.user.JwtAuthenticationResponse;
import com.everestes.socialecommerce.api.model.user.RequestTokenRequest;
import com.everestes.socialecommerce.api.model.user.ResetPasswordForm;
import com.everestes.socialecommerce.api.model.user.UserViewWithEmail;
import com.everestes.socialecommerce.domain.user.FacebookUser;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.ForgotPasswordCodeNotFoundException;
import com.everestes.socialecommerce.service.user.PasswordConfirmationNotMatchException;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;

@RestController

public class LoginController {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("authMgr")
	private AuthenticationManager authenticationManager;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private Mapper mapper;

	// TODO: delete
	@Autowired
	private FriendshipService friendshipService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
			HttpServletRequest request, Locale locale) {

		// Perform the security
		Authentication authentication;
		try {
			authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));

			SecurityContextHolder.getContext().setAuthentication(authentication);

			// Reload password post-security so we can generate token
			final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getUsername());
			final String token = jwtTokenUtil.generateToken(userDetails, DeviceUtils.getCurrentDevice(request));

			// Return the token
			return ResponseEntity.ok(new JwtAuthenticationResponse(token));
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "password", "wrong.password", "Wrong password") });
		}
	}

	@RequestMapping(value = "/fbLogin", method = RequestMethod.POST)
	public ResponseEntity<?> facebookLogin(@RequestBody String accessToken, HttpServletRequest request) {
		FacebookUser fbUser = userService.getFacebookUser(accessToken);
		if (null != fbUser) {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(fbUser.getUser(),
					fbUser.getUser().getPassword(), fbUser.getUser().getAuthorities());

			Authentication auth = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(auth);

			// Reload password post-security so we can generate token
			final String jwtToken = jwtTokenUtil.generateToken(fbUser.getUser(), DeviceUtils.getCurrentDevice(request));

			return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken));
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@RequestMapping(value = "/fbUserInfo", method = RequestMethod.POST)
	public UserViewWithEmail getFacebookUserInfo(@RequestBody String accessToken) {
		return mapper.map(userService.getUserFacebookInfo(accessToken), UserViewWithEmail.class);
	}

	@RequestMapping(value = "/password/reset/token", method = RequestMethod.POST)
	public ResponseEntity<?> getPasswordResetToken(@RequestBody @Validated RequestTokenRequest requestTokenRequest,
			Errors errors, Locale locale) {
		if (errors.hasErrors()) {
			List<FieldError> fieldErrorMessages = new ArrayList<FieldError>();
			for (org.springframework.validation.FieldError error : errors.getFieldErrors()) {
				fieldErrorMessages.add(FieldError.create(messageSource, locale, error.getField(), error.getCode(),
						error.getDefaultMessage()));
			}

			return ResponseEntity.badRequest().body(fieldErrorMessages);
		}
		boolean response;
		try {
			response = userService.generatePasswordResetCode(requestTokenRequest.getEmail());
		} catch (NoSuchAlgorithmException e) {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "email", "email.notFound", "Unknown Error") });
		}
		if (response) {
			return ResponseEntity.ok(new EmptyJSONResponse());
		} else {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "email", "email.notFound", "Email not found") });
		}
	}

	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public ResponseEntity<?> resetPassword(@AuthenticationPrincipal User activeUser,
			@RequestBody @Validated ResetPasswordForm resetPasswordForm, Errors errors, HttpServletRequest request,
			Locale locale) {

		if (errors.hasErrors()) {
			List<FieldError> fieldErrorMessages = new ArrayList<FieldError>();
			for (org.springframework.validation.FieldError error : errors.getFieldErrors()) {
				fieldErrorMessages.add(FieldError.create(messageSource, locale, error.getField(), error.getCode(),
						error.getDefaultMessage()));
			}

			return ResponseEntity.badRequest().body(fieldErrorMessages);
		}

		User user = null;
		try {
			user = userService.resetPassword(resetPasswordForm.getToken(), resetPasswordForm.getPassword(),
					resetPasswordForm.getConfirmPassword());
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user,
					user.getPassword(), user.getAuthorities());

			Authentication auth = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(auth);

			// Reload password post-security so we can generate token
			final String jwtToken = jwtTokenUtil.generateToken(user, DeviceUtils.getCurrentDevice(request));

			return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken));
		} catch (ForgotPasswordCodeNotFoundException e) {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "token", "token.invalid", "Invalid reset code") });
		} catch (PasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(new FieldError[] { FieldError.create(messageSource, locale,
					"confirmPassword", "confirmPassword.notMatch", "Passwords don't match.") });
		}
	}

	@RequestMapping(value = "/push", method = RequestMethod.POST)
	public void sendPush() {
		friendshipService.sendPush();
	}

}
