package com.everestes.socialecommerce.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

	@RequestMapping("/")
	private String getHealth() {
		return "{\"status\": \"up\"}";
	}

}
