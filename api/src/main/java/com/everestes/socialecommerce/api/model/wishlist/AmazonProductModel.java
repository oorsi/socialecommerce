package com.everestes.socialecommerce.api.model.wishlist;

public class AmazonProductModel {
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
