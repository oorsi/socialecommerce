package com.everestes.socialecommerce.api.controller.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.everestes.socialecommerce.api.model.EmptyJSONResponse;
import com.everestes.socialecommerce.api.model.FieldError;
import com.everestes.socialecommerce.api.model.user.BirthdateModel;
import com.everestes.socialecommerce.api.model.user.PasswordChangeForm;
import com.everestes.socialecommerce.api.model.user.PhoneNumber;
import com.everestes.socialecommerce.api.model.user.UserView;
import com.everestes.socialecommerce.domain.file.FileMetaData;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.user.phoneNumber.PhoneNumberConfirmation;
import com.everestes.socialecommerce.file.upload.exception.FileUploadException;
import com.everestes.socialecommerce.service.FileManagementService;
import com.everestes.socialecommerce.service.user.PasswordConfirmationNotMatchException;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.user.WrongPasswordException;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;
import com.everestes.socialecommerce.service.user.phone.PhoneNumberService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private FriendshipService friendshipService;

	@Autowired
	private FileManagementService fileManagementService;

	@Autowired
	private Mapper mapper;

	@Autowired
	private UserService userService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private PhoneNumberService phoneNumberService;

	@RequestMapping("/profile/picture/upload")
	private FileMetaData upload(@AuthenticationPrincipal User activeUser,
			@RequestParam("file") MultipartFile multipartFile) throws IOException, FileUploadException {
		return fileManagementService.uploadProfilePicture(activeUser.getUserID(), multipartFile.getOriginalFilename(),
				multipartFile.getInputStream());

	}

	@RequestMapping("/info.json")
	private UserView userInfo(@AuthenticationPrincipal User activeUser) {

		UserView userInfo = mapper.map(activeUser, UserView.class);
		userInfo.setNumberOfFollowed(friendshipService.numberOfFollowing(activeUser.getUserID()));
		userInfo.setNumberOfFollowers(friendshipService.numberOfFollowers(activeUser.getUserID()));
		return userInfo;
	}

	@PostMapping("/password/change")
	private ResponseEntity<?> passwordChange(@AuthenticationPrincipal User activeUser,
			@RequestBody @Validated PasswordChangeForm passwordChangeForm, Errors errors, Locale locale) {
		if (errors.hasErrors()) {
			List<FieldError> fieldErrorMessages = new ArrayList<FieldError>();
			for (org.springframework.validation.FieldError error : errors.getFieldErrors()) {
				fieldErrorMessages.add(FieldError.create(messageSource, locale, error.getField(), error.getCode(),
						error.getDefaultMessage()));
			}

			return ResponseEntity.badRequest().body(fieldErrorMessages);
		}
		try {
			return ResponseEntity
					.ok(userService.changePassword(activeUser.getUserID(), passwordChangeForm.getOldPassword(),
							passwordChangeForm.getPassword(), passwordChangeForm.getConfirmPassword()));
		} catch (PasswordConfirmationNotMatchException e) {
			return ResponseEntity.badRequest().body(new FieldError[] { FieldError.create(messageSource, locale,
					"confirmPassword", "confirmPassword.notMatch", "Passwords don't match.") });
		} catch (WrongPasswordException e) {
			return ResponseEntity.badRequest().body(new FieldError[] {
					FieldError.create(messageSource, locale, "oldPassword", "oldPassword.wrong", "Wrong Password") });
		}
	}

	@PostMapping("/phoneNumber/save")
	private ResponseEntity<?> savePhoneNumber(@AuthenticationPrincipal User activeUser,
			@RequestBody @Validated PhoneNumber phoneNumber) {

		phoneNumberService.savePhoneNumber(activeUser, phoneNumber.getPhoneNumber());
		return ResponseEntity.ok(new EmptyJSONResponse());
	}

	@PostMapping("/phoneNumber/confirm")
	private ResponseEntity<?> confirmPhoneNumber(@AuthenticationPrincipal User activeUser,
			@RequestBody @Validated PhoneNumberConfirmation confirmation) {

		boolean result = phoneNumberService.confirmPhoneNumber(activeUser.getUserID(), confirmation.getConfirmation());
		if (result) {
			return ResponseEntity.ok(new EmptyJSONResponse());
		} else {
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/birthday/save")
	private ResponseEntity<?> saveBirthday(@AuthenticationPrincipal User activeUser,
			@RequestBody @Validated BirthdateModel birthdateModel) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(birthdateModel.getYear(), birthdateModel.getMonth(), birthdateModel.getDay());

		userService.saveBirthdate(activeUser, calendar.getTime());
		return ResponseEntity.ok(new EmptyJSONResponse());
	}

}
