package com.everestes.socialecommerce.api.controller.address;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.address.Address;
import com.everestes.socialecommerce.domain.address.UserAddress;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.address.AddressNotFoundException;
import com.everestes.socialecommerce.service.address.AddressService;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping("/all")
	public @ResponseBody List<UserAddress> getUserAddresses(@AuthenticationPrincipal User activeUser) {
		return addressService.getUserAddresses(activeUser.getUserID());
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody UserAddress save(@AuthenticationPrincipal User activeUser, @RequestBody Address address) {
		return addressService.save(address.getId(), address.getName(), address.getAddress1(), address.getAddress2(),
				address.getCity(), address.getState(), address.getZip(), address.getPhoneNumber(),
				address.getFormattedAddress(), address.isDefault(), activeUser.getUserID());
	}

	@GetMapping(value = "/{id}/get")
	public @ResponseBody UserAddress get(@AuthenticationPrincipal User activeUser, @PathVariable Long id) {
		return addressService.getUserAddress(activeUser.getUserID(), id);
	}

	@PostMapping(value = "/{id}/delete")
	public @ResponseBody void delete(@AuthenticationPrincipal User activeUser, @PathVariable Long id)
			throws AddressNotFoundException {
		addressService.deleteAddress(activeUser.getUserID(), id);
	}

	@PostMapping(value = "/{id}/makeDefault")
	public @ResponseBody UserAddress makeDefault(@AuthenticationPrincipal User activeUser, @PathVariable Long id)
			throws AddressNotFoundException {
		return addressService.makeDefault(activeUser.getUserID(), id);
	}

	@ExceptionHandler(AddressNotFoundException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> requestHandlingNoHandlerFound() {
		return new ResponseEntity<String>(
				String.format("{\"message\": \"%s\",\"error\": \"%s\"}", "Address Not Found", "ADDRESS_NOT_FOUND"),
				HttpStatus.BAD_REQUEST);
	}
}
