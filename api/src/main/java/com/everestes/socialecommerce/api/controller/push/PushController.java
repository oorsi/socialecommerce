package com.everestes.socialecommerce.api.controller.push;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.EmptyJSONResponse;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.push.PushService;

@RestController
@RequestMapping("/push")
public class PushController {

	@Autowired
	private PushService pushService;

	@PostMapping("/savePlayerID")
	private ResponseEntity<EmptyJSONResponse> savePlayerIDs(@AuthenticationPrincipal User user,
			@RequestBody String playerID) {
		pushService.savePlayerID(user.getUserID(), playerID);
		return ResponseEntity.ok(new EmptyJSONResponse());
	}

	@PostMapping("/deletePlayerID")
	private ResponseEntity<EmptyJSONResponse> deletePlayerIDs(@AuthenticationPrincipal User user,
			@RequestBody String playerID) {
		pushService.deletePlayerID(user.getUserID(), playerID);
		return ResponseEntity.ok(new EmptyJSONResponse());
	}
}
