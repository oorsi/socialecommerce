package com.everestes.socialecommerce.api.model;

public class ProductView {

	private Long productId;
	private int retailerId;
	private String sku;
	private String name;
	private Float regularPrice;
	private Float salePrice;
	private String image;

//	private List<ProductSKUView> productSKUs;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(int retailerId) {
		this.retailerId = retailerId;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(Float regularPrice) {
		this.regularPrice = regularPrice;
	}

	public Float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Float salePrice) {
		this.salePrice = salePrice;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

//	public List<ProductSKUView> getProductSKUs() {
//		return productSKUs;
//	}
//
//	public void setProductSKUs(List<ProductSKUView> productSKUs) {
//		this.productSKUs = productSKUs;
//	}

}
