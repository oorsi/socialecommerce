package com.everestes.socialecommerce.api.model.signin;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

public class SignUpForm {

	@Email(message = "email.invalid")
	@NotNull(message = "field.required")
	private String email;
	@NotNull(message = "field.required")
	private String password;
	@NotNull(message = "field.required")
	private String firstName;
	@NotNull(message = "field.required")
	private String lastName;
	// @NotNull(message = "{field.required}")
	private String confirmPassword;

	// Facebook access token
	private String fbat;

	private Date birthDate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFbat() {
		return fbat;
	}

	public void setFbat(String fbat) {
		this.fbat = fbat;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
}
