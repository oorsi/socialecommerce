package com.everestes.socialecommerce.api.controller.catalog;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.ProductView;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.service.product.ProductService;
import com.everestes.socialecommerce.service.product.TrendingProductService;
//import com.everestes.socialecommerce.service.product.TrendingProductService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private TrendingProductService trendingProductService;

	@Value("${ui.page.items.number}")
	private Integer limit;

	@Autowired
	private Mapper mapper;

	@RequestMapping(value = "/search", params = { "s" })
	public List<ProductView> search(@RequestParam String s, @RequestParam int page)
			throws UnsupportedEncodingException {
		return DozerHelper.map(mapper, productService.searchProducts(s, page, limit), ProductView.class);
	}

	@RequestMapping(value = "/search", params = { "upc" })
	public List<ProductView> searchByUPC(@RequestParam("upc") String upc) {
		return DozerHelper.map(mapper, productService.searchProductsByUPC(upc), ProductView.class);
	}

	@RequestMapping("/retailer/{retailer}/sku/{sku}")
	public Product get(@PathVariable int retailer, @PathVariable String sku) {
		return productService.getProductBySku(Retailer.get(retailer), sku, true);
	}

	@RequestMapping("/id/{id}")
	public Product get(@PathVariable Long id) {
		return productService.getProductById(id, true);
	}

	@RequestMapping("/list")
	public List<ProductView> getProducts(@RequestParam String[] sku) {
		return DozerHelper.map(mapper, productService.getProductBySku(Retailer.BEST_BUY, sku), ProductView.class);
	}

	@RequestMapping("/trending")
	public List<ProductView> getTrendingProducts() {
		List<Product> trendingProducts = trendingProductService.getTrendingProducts();
		if (null == trendingProducts || trendingProducts.size() == 0) {
			trendingProducts = productService.getTrendingProducts();
		}
		return DozerHelper.map(mapper, trendingProducts, ProductView.class);
	}

}
