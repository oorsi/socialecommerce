package com.everestes.socialecommerce.api.controller.activity;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.CommentForm;
import com.everestes.socialecommerce.api.model.CommentView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.activity.CommentService;
import com.everestes.socialecommerce.service.activity.LikeService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping(value = "/activity")
public class CommentLikeController {

	@Autowired
	private CommentService commentService;

	@Autowired
	private LikeService likeService;

	@Autowired
	private Mapper mapper;

	@RequestMapping(value = "/comment/add", method = RequestMethod.POST)
	private CommentView addComment(@AuthenticationPrincipal User user, @RequestBody CommentForm commentForm) {
		return mapper.map(
				commentService.saveComment(commentForm.getComment(), user.getUserID(), commentForm.getActivityId()),
				CommentView.class);
	}

	@RequestMapping("/{activityId}/comments.json")
	private List<CommentView> getCommentsForActivity(@PathVariable Long activityId) {
		return DozerHelper.map(mapper, commentService.getCommentsForActivity(activityId), CommentView.class);
	}

	@RequestMapping(value = "/comment/remove", method = RequestMethod.DELETE)
	private void removeComment(@AuthenticationPrincipal User user, @RequestParam Long commentId) {
		commentService.removeComment(commentId);
	}

	@RequestMapping(value = "/like", method = RequestMethod.POST)
	private ResponseEntity<?> like(@AuthenticationPrincipal User user, @RequestBody Long activityID) {
		likeService.like(user.getUserID(), activityID);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/unlike", method = RequestMethod.POST)
	private ResponseEntity<?> unlike(@AuthenticationPrincipal User user, @RequestBody Long activityID) {
		return ResponseEntity.ok(likeService.unlike(user.getUserID(), activityID));
	}

}
