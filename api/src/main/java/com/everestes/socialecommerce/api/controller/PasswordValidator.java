package com.everestes.socialecommerce.api.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.everestes.socialecommerce.api.model.signin.SignUpForm;

public class PasswordValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return SignUpForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SignUpForm form = (SignUpForm) target;

		if (!(form.getPassword().equals(form.getConfirmPassword()))) {
			errors.rejectValue("confirmPassword", "confirmPassword.notMatch");
		}

	}

}
