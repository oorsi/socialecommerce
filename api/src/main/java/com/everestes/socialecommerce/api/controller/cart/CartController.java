package com.everestes.socialecommerce.api.controller.cart;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.cart.CartProductRequest;
import com.everestes.socialecommerce.api.model.order.CartProductUpdateForm;
import com.everestes.socialecommerce.domain.cart.CartProduct;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.cart.CartProductNotFoundException;
import com.everestes.socialecommerce.service.cart.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/add", params = { "productId" }, method = RequestMethod.POST)
	private CartProduct addToCart(@AuthenticationPrincipal User activeUser, @RequestParam Long productId) {
		return cartService.addToCart(activeUser.getUserID(), activeUser.getUserID(), productId);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	private CartProduct addToCart(@AuthenticationPrincipal User activeUser,
			@RequestBody CartProductRequest cartProduct) {
		if (null == cartProduct.getTo()) {
			cartProduct.setTo(activeUser.getUserID());
		}

		if (null != cartProduct.getProductId()) {
			return cartService.addToCart(activeUser.getUserID(), cartProduct.getTo(), cartProduct.getProductId());
		} else {
			return cartService.addToCart(activeUser.getUserID(), cartProduct.getTo(),
					Retailer.get(cartProduct.getRetailer()), cartProduct.getSku());
		}
	}

	@RequestMapping(value = "/add", params = { "to", "productId" }, method = RequestMethod.POST)
	private CartProduct addToCart(@AuthenticationPrincipal User activeUser, @RequestParam Long to,
			@RequestParam Long productId) {
		return cartService.addToCart(activeUser.getUserID(), to, productId);
	}

	@RequestMapping(value = "/add", params = { "retailer", "sku" }, method = RequestMethod.POST)
	private CartProduct addToCart(@AuthenticationPrincipal User activeUser, @RequestParam Integer retailer,
			@RequestParam String sku) {
		return cartService.addToCart(activeUser.getUserID(), activeUser.getUserID(), Retailer.get(retailer), sku);
	}

	@RequestMapping(value = "/add", params = { "to", "retailer", "sku" }, method = RequestMethod.POST)
	private CartProduct addToCart(@AuthenticationPrincipal User activeUser, @RequestParam Long to,
			@RequestParam Retailer retailer, @RequestParam String sku) {
		return cartService.addToCart(activeUser.getUserID(), to, retailer, sku);
	}

	@RequestMapping("/products")
	private List<CartProduct> cart(@AuthenticationPrincipal User activeUser) {
		return cartService.getCart(activeUser.getUserID());
	}

	@RequestMapping("/size")
	private long cartSize(@AuthenticationPrincipal User activeUser) {
		return cartService.getCartSize(activeUser.getUserID());
	}

	@RequestMapping(value = "/product/update", method = RequestMethod.POST)
	private ResponseEntity<?> setQuantity(@AuthenticationPrincipal User activeUser,
			@RequestBody CartProductUpdateForm cartProductUpdateForm) {
		try {
			return ResponseEntity
					.ok(cartService.updateQuantity(activeUser.getUserID(), cartProductUpdateForm.getForId(),
							cartProductUpdateForm.getProductId(), cartProductUpdateForm.getQuantity()));
		} catch (CartProductNotFoundException e) {
			logger.error("Cart product not found for user ", e);
			return ResponseEntity.notFound().build();
		}

	}

	@PostMapping(value = "/remove")
	private void removeCartProduct(@AuthenticationPrincipal User activeUser,
			@RequestBody CartProductRequest cartProductRequest) {
		cartService.deleteProductFromCart(activeUser.getUserID(), cartProductRequest.getTo(),
				cartProductRequest.getProductId());
	}

}
