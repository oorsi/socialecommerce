package com.everestes.socialecommerce.api.model.user;

import java.io.Serializable;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtAuthenticationResponse implements Serializable {

	private static final long serialVersionUID = 1250166508152483573L;

	private final String token;
	private boolean facebookConnected;

	public JwtAuthenticationResponse(String token) {
		this.token = token;
	}

	public String getToken() {
		return this.token;
	}

	public boolean isFacebookConnected() {
		return facebookConnected;
	}

	public void setFacebookConnected(boolean facebookConnected) {
		this.facebookConnected = facebookConnected;
	}
}
