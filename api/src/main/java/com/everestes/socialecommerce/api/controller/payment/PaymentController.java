package com.everestes.socialecommerce.api.controller.payment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.everestes.socialecommerce.api.model.payment.StripeToken;
import com.everestes.socialecommerce.domain.payment.PaymentMethod;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.payment.PaymentMethodException;
import com.everestes.socialecommerce.service.payment.PaymentService;

@Controller
@RequestMapping("/payment")
public class PaymentController {
	@Value("${stripe.apiKey.publishable}")
	private String stripeKey;

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody PaymentMethod addPaymentMethod(@AuthenticationPrincipal User user,
			@RequestBody StripeToken token) throws PaymentMethodException {
		return paymentService.savePaymentInfo(user.getUserID(), token.getToken(), token.isSetAsDefault());
	}

	@RequestMapping(value = "/all")
	public @ResponseBody List<PaymentMethod> paymentMethods(@AuthenticationPrincipal User user)
			throws PaymentMethodException {
		return paymentService.getPaymentMethods(user.getUserID());
	}

	@RequestMapping(value = "/default")
	public @ResponseBody PaymentMethod defaultPaymentMethod(@AuthenticationPrincipal User user)
			throws PaymentMethodException {
		return paymentService.getDefaultPaymentMethod(user.getUserID());
	}

	//
	@PostMapping(value = "/{id}/default")
	public @ResponseBody void changeDefaultMethod(@AuthenticationPrincipal User user, @PathVariable String id)
			throws PaymentMethodException {
		paymentService.changeDefaultMethod(user.getUserID(), id);
	}

	@PostMapping(value = "/{id}/delete")
	public @ResponseBody void deletePaymentMethod(@AuthenticationPrincipal User user, @PathVariable String id)
			throws PaymentMethodException {
		paymentService.deletePaymentMethod(user.getUserID(), id);
	}

}
