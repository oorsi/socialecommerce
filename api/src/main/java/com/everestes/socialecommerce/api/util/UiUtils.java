//package com.everestes.socialecommerce.api.util;
//
//import java.util.Locale;
//
//import org.springframework.context.MessageSource;
//import org.springframework.context.NoSuchMessageException;
//
//import com.everestes.socialecommerce.api.model.FieldError;
//
//public class UiUtils {
//
//	/**
//	 * Converts a Spring FieldError into a {@link FieldErrorMessage} using the
//	 * given MessageSource.
//	 */
//	public static FieldError makeFieldErrorMessage(org.springframework.validation.FieldError error,
//			MessageSource messageSource) {
//		try {
//			return new FieldError(error.getField(), messageSource.getMessage(error, Locale.getDefault()));
//		} catch (NoSuchMessageException ex) {
//			// This means we didn't have a message and the error provided no
//			// default message.
//			return new FieldError(error.getField(), "unknown error");
//		}
//	}
//
//}
