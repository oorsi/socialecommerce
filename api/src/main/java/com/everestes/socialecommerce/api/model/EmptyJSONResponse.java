package com.everestes.socialecommerce.api.model;

public class EmptyJSONResponse {

	// Spring MVC throws IllegalArgumentException if the object is empty
	public String getValue() {
		return "value";
	}

}
