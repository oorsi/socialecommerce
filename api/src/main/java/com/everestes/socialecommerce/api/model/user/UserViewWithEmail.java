package com.everestes.socialecommerce.api.model.user;

public class UserViewWithEmail extends UserView {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
