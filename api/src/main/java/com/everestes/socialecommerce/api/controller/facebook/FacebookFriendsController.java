package com.everestes.socialecommerce.api.controller.facebook;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.user.UserView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/friends/fb")
public class FacebookFriendsController {

	@Autowired
	private FriendshipService friendshipService;

	@Autowired
	private Mapper mapper;

	@RequestMapping("/search")
	public List<UserView> searchFriends(@AuthenticationPrincipal User activeUser, String accessToken) {
		return DozerHelper.map(mapper, friendshipService.getFacebookFriends(activeUser.getUserID(), accessToken),
				UserView.class);
	}

}
