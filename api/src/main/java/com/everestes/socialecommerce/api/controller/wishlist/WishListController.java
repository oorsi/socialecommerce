package com.everestes.socialecommerce.api.controller.wishlist;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.WishlistRequest;
import com.everestes.socialecommerce.api.model.wishlist.AmazonProductModel;
import com.everestes.socialecommerce.api.model.wishlist.WishlistProductView;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.domain.wishlist.WishListProduct;
import com.everestes.socialecommerce.service.wishlist.WishListService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/wishlist")
public class WishListController {

	@Autowired
	private WishListService wishListService;

	@Autowired
	private Mapper mapper;

	@RequestMapping("/list.json")
	public List<WishlistProductView> json(@AuthenticationPrincipal User activeUser) {
		return DozerHelper.map(mapper, wishListService.list(activeUser.getUserID()), WishlistProductView.class);

	}

	@PostMapping(value = "/add")
	public WishListProduct add(@AuthenticationPrincipal User user, @RequestBody WishlistRequest wishlistRequest) {
		if (null != wishlistRequest.getProductId())
			return wishListService.addToWishList(user, wishlistRequest.getProductId());
		if (StringUtils.isNotBlank(wishlistRequest.getUrl())) {
			return wishListService.addToWishList(user, wishlistRequest.getUrl());
		} else
			return wishListService.addToWishList(user, Retailer.get(wishlistRequest.getRetailerId()),
					wishlistRequest.getSku());

	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, params = { "retailer", "sku" })
	public WishListProduct add(@AuthenticationPrincipal User user, @RequestParam int retailer,
			@RequestParam String sku) {
		return wishListService.addToWishList(user, Retailer.get(retailer), sku);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, params = { "productId" })
	public WishListProduct add(@AuthenticationPrincipal User user, @RequestParam Long productId) {
		return wishListService.addToWishList(user, productId);
	}

	@PostMapping(value = "/amazon/add")
	public WishListProduct addAmazonProduct(@AuthenticationPrincipal User user, @RequestBody AmazonProductModel amazonProductModel) {
		return wishListService.addToWishList(user, amazonProductModel.getUrl());
	}

	@PostMapping(value = "/delete")
	public void deleteById(@AuthenticationPrincipal User user, @RequestBody WishlistRequest wishlistRequest) {
		wishListService.removeProduct(user.getUserID(), wishlistRequest.getProductId());
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteById(@AuthenticationPrincipal User user, @RequestParam long productId) {

		wishListService.removeProduct(user.getUserID(), productId);

	}

	@RequestMapping(value = "/isInWishList", params = { "productId" }, method = RequestMethod.GET)
	public boolean isInWishList(@AuthenticationPrincipal User user, @RequestParam(name = "productId") Long productId) {
		return wishListService.isInWishList(user.getUserID(), productId);
	}

	@RequestMapping(value = "/isInWishList", params = { "retailer", "sku" }, method = RequestMethod.GET)
	public boolean isInWishList(@AuthenticationPrincipal User user, @RequestParam Integer retailer,
			@RequestParam Long sku) {
		return wishListService.isInWishList(user.getUserID(), Retailer.get(retailer), sku);
	}
}
