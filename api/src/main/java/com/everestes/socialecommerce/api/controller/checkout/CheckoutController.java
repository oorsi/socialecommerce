package com.everestes.socialecommerce.api.controller.checkout;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.domain.cart.CartProduct;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.cart.CartService;

@RestController
@RequestMapping("/checkout")
public class CheckoutController {

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/products/{forId}")
	private List<CartProduct> checkOut(@AuthenticationPrincipal User activeUser, @PathVariable Long forId) {
		return cartService.getCart(activeUser.getUserID(), forId);

	}

	@RequestMapping(value = "/products")
	private List<CartProduct> checkOut(@AuthenticationPrincipal User activeUser) {
		return cartService.getCart(activeUser.getUserID(), activeUser.getUserID());

	}

	@RequestMapping(value = "/remove")
	private void deleteFromCart(@AuthenticationPrincipal User activeUser, @RequestParam Long forId,
			@RequestParam Long productId) {
		cartService.deleteProductFromCart(activeUser.getUserID(), forId, productId);

	}

}
