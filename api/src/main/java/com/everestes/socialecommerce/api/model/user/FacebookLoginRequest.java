package com.everestes.socialecommerce.api.model.user;

public class FacebookLoginRequest {
	private String accessToken;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
