package com.everestes.socialecommerce.api.model.user;

public class PhoneNumberConfirmation {
	private String confirmationCode;

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

}
