package com.everestes.socialecommerce.api.model.user;

public class PhoneNumber {

	private String phoneNumber;
	private UserView user;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public UserView getUser() {
		return user;
	}

	public void setUser(UserView user) {
		this.user = user;
	}

}
