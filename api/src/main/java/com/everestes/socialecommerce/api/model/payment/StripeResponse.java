package com.everestes.socialecommerce.api.model.payment;

public class StripeResponse {

	private String id;
	private StripeCard card;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StripeCard getCard() {
		return card;
	}

	public void setCard(StripeCard card) {
		this.card = card;
	}

}
