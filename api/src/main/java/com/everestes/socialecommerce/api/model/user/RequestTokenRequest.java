package com.everestes.socialecommerce.api.model.user;

import org.hibernate.validator.constraints.NotEmpty;

public class RequestTokenRequest {

	@NotEmpty(message = "email.notEmpty")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
