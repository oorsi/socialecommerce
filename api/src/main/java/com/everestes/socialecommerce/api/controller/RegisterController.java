package com.everestes.socialecommerce.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.everestes.socialecommerce.api.JwtTokenUtil;
import com.everestes.socialecommerce.api.model.FieldError;
import com.everestes.socialecommerce.api.model.signin.SignUpForm;
import com.everestes.socialecommerce.api.model.user.JwtAuthenticationResponse;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.DuplicateEmailException;
import com.everestes.socialecommerce.service.user.UserService;

@Controller
public class RegisterController {

	Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("authMgr")
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> registerUser(@Validated @RequestBody SignUpForm signUpForm, BindingResult result,
			HttpServletRequest request, Locale locale) {
		if (result.hasErrors()) {
			List<FieldError> fieldErrorMessages = new ArrayList<FieldError>();
			for (org.springframework.validation.FieldError error : result.getFieldErrors()) {
				fieldErrorMessages.add(FieldError.create(messageSource, locale, error.getField(), error.getCode(),
						error.getDefaultMessage()));
			}

			return ResponseEntity.badRequest().body(fieldErrorMessages);
		}

		User user;
		try {
			user = userService.registerUser(signUpForm.getEmail(), signUpForm.getPassword(), signUpForm.getFirstName(),
					signUpForm.getLastName(), locale, signUpForm.getBirthDate(), signUpForm.getFbat(), false);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user,
					signUpForm.getPassword(), user.getAuthorities());

			Authentication auth = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(auth);

			// Reload password post-security so we can generate token
			final String jwtToken = jwtTokenUtil.generateToken(user, DeviceUtils.getCurrentDevice(request));

			return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken));
		} catch (DuplicateEmailException e) {
			return ResponseEntity.badRequest().body(new FieldError[] { FieldError.create(messageSource, locale, "email",
					"register.alreadyRegistered", "Email address already Registered") });
		}

	}

}
