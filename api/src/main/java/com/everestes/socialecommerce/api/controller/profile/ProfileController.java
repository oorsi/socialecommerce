package com.everestes.socialecommerce.api.controller.profile;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.user.UserView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;

@RestController
@RequestMapping("/p")
public class ProfileController {

	@Autowired
	private UserService userService;

	@Autowired
	private Mapper mapper;

	@Autowired
	private FriendshipService friendshipService;

	@RequestMapping("/{userId}/info.json")
	private UserView getuserProfile(@AuthenticationPrincipal User activeUser, @PathVariable Long userId) {
		UserView userInfo = mapper.map(userService.getUserById(userId), UserView.class);
		userInfo.setNumberOfFollowed(friendshipService.numberOfFollowing(userId));
		userInfo.setNumberOfFollowers(friendshipService.numberOfFollowers(userId));

		if (null != activeUser) {
			userInfo.setFollowed(friendshipService.isFollowing(activeUser.getUserID(), userId));
		}
		return userInfo;
	}

}
