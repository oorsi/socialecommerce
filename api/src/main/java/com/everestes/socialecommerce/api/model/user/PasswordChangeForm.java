package com.everestes.socialecommerce.api.model.user;

import org.hibernate.validator.constraints.NotEmpty;

public class PasswordChangeForm {

	@NotEmpty
	private String oldPassword;
	@NotEmpty
	private String password;
	@NotEmpty
	private String confirmPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
