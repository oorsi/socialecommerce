package com.everestes.socialecommerce.api.model.user;

public class FriendshipView {

	private boolean confirmed;

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

}
