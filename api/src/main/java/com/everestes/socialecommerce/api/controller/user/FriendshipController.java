package com.everestes.socialecommerce.api.controller.user;

import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.user.FollowUnfollowRequest;
import com.everestes.socialecommerce.api.model.user.FriendshipView;
import com.everestes.socialecommerce.api.model.user.UserView;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.UserService;
import com.everestes.socialecommerce.service.user.friends.FriendshipService;
import com.everestes.socialecommerce.util.DozerHelper;

@RestController
@RequestMapping("/friends")
public class FriendshipController {

	@Autowired
	private FriendshipService frienshipService;

	@Autowired
	private UserService userService;

	@Autowired
	private Mapper mapper;

	@RequestMapping
	public List<UserView> getFriends(@AuthenticationPrincipal User activeUser, Model model,
			@RequestParam(name = "userID", required = false) Long userID) {
		if (null == userID) {
			return DozerHelper.map(mapper, frienshipService.getFriends(activeUser.getUserID()), UserView.class);
		}
		return DozerHelper.map(mapper, frienshipService.getFriends(userID), UserView.class);
	}

	@RequestMapping("/followers")
	public List<UserView> getFollowers(@AuthenticationPrincipal User activeUser, Model model,
			@RequestParam("userID") Long userID) {
		return DozerHelper.map(mapper, frienshipService.getFollowers(userID), UserView.class);
	}

	@RequestMapping(value = "/search", params = "s")
	public List<UserView> searchFriends(@RequestParam("s") String s, @AuthenticationPrincipal User activeUser,
			Model model) {
		return DozerHelper.map(mapper, userService.searchUsers(s, activeUser.getUserID()), UserView.class);
	}

	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	public FriendshipView follow(@AuthenticationPrincipal User activeUser, @RequestBody FollowUnfollowRequest request) {
		return mapper.map(frienshipService.followFriend(activeUser.getUserID(), request.getUserID()),
				FriendshipView.class);

	}

	@RequestMapping(value = "/unfollow", method = RequestMethod.POST)
	public void unfollow(@AuthenticationPrincipal User activeUser, @RequestBody FollowUnfollowRequest request) {
		frienshipService.unfollow(activeUser.getUserID(), request.getUserID());
	}

}
