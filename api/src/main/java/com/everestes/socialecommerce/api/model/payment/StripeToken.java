package com.everestes.socialecommerce.api.model.payment;

public class StripeToken {

	private String token;
	private boolean setAsDefault;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isSetAsDefault() {
		return setAsDefault;
	}

	public void setSetAsDefault(boolean setAsDefault) {
		this.setAsDefault = setAsDefault;
	}

}
