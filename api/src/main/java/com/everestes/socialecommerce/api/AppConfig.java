package com.everestes.socialecommerce.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.everestes.socialecommerce.api")
public class AppConfig {

}
