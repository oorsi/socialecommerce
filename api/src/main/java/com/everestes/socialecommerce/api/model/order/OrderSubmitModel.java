package com.everestes.socialecommerce.api.model.order;

import java.util.List;

public class OrderSubmitModel {

	private Long shipTo;
	private Long shippingAddress;
	private String paymentMethod;
	private Long forUser;

	private List<ProductModel> products;

	public Long getShipTo() {
		return shipTo;
	}

	public void setShipTo(Long shipTo) {
		this.shipTo = shipTo;
	}

	public Long getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Long shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Long getForUser() {
		return forUser;
	}

	public void setForUser(Long forUser) {
		this.forUser = forUser;
	}

	public List<ProductModel> getProducts() {
		return products;
	}

	public void setProducts(List<ProductModel> products) {
		this.products = products;
	}

}
