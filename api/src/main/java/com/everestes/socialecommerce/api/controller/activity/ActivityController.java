package com.everestes.socialecommerce.api.controller.activity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.activity.ActivityService;

@RestController
@RequestMapping("/activity")
public class ActivityController {

	@Autowired
	private ActivityService activityService;

	@RequestMapping("/forUser/{userId}")
	private List<Activity> getActivityForUser(@PathVariable Long userId, @AuthenticationPrincipal User user) {
		return activityService.getActivityForUser(user, userId);
	}

	@RequestMapping("/{id}/detail.json")
	private Activity getActivity(@PathVariable Long id, @AuthenticationPrincipal User user) {
		return activityService.getActivity(id, user);
	}

}
