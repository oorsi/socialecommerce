package com.everestes.socialecommerce.api.controller.facebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everestes.socialecommerce.api.model.EmptyJSONResponse;
import com.everestes.socialecommerce.domain.user.User;
import com.everestes.socialecommerce.service.user.UserService;

@RestController
@RequestMapping("/fb")
public class FacebookController {

	@Autowired
	private UserService userService;

	@PostMapping("/connect")
	public ResponseEntity<?> connectFacebook(@AuthenticationPrincipal User activeUser,
			@RequestBody String accessToken) {
		userService.connectFacebook(accessToken, activeUser.getUserID());
		return ResponseEntity.ok(new EmptyJSONResponse());
	}

}
