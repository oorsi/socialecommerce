package com.everestes.socialecommerce.file.upload.impl.aws;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.everestes.socialecommerce.file.upload.FileUploadService;
import com.everestes.socialecommerce.file.upload.exception.FileUploadException;
import com.everestes.socialecommerce.file.upload.response.FileUploadResponse;

@Service
public class AWSFileUploadService implements FileUploadService {

	@Value("#{environment.AWS_BUCKET_NAME}")
	private String bucketName;

	@Value("#{environment.AWS_ACCESS_KEY}")
	private String accessKey;

	@Value("#{environment.AWS_SECRET_KEY}")
	private String secretKey;

	public List<FileUploadResponse> upload(String directory, String fileName, InputStream stream)
			throws FileUploadException {

		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.withRegion(Regions.US_EAST_1).build();
		try {
			if (!s3client.doesBucketExist(bucketName)) {
				s3client.createBucket(new CreateBucketRequest(bucketName));
			}
			List<FileUploadResponse> fileUploadResponses = new ArrayList<>();
			String uniqueFileName = getUniqueFileName(fileName);
			s3client.putObject(bucketName, directory + "/" + uniqueFileName, stream, null);
			fileUploadResponses.add(new FileUploadResponse(uniqueFileName, directory));
			return fileUploadResponses;
		} catch (AmazonServiceException ase) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Caught an AmazonServiceException, wh ich " + "means your request made it "
					+ "to Amazon S3, but was rejected with an error response" + " for some reason.");
			stringBuilder.append("Error Message:    " + ase.getMessage());
			stringBuilder.append("HTTP Status Code: " + ase.getStatusCode());
			stringBuilder.append("AWS Error Code:   " + ase.getErrorCode());
			stringBuilder.append("Error Type:       " + ase.getErrorType());
			stringBuilder.append("Request ID:       " + ase.getRequestId());
			throw new FileUploadException(stringBuilder.toString(), ase);
		} catch (AmazonClientException ace) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Caught an AmazonClientException, which " + "means the client encountered "
					+ "an internal error while trying to " + "communicate with S3, "
					+ "such as not being able to access the network.");
			stringBuilder.append("Error Message: " + ace.getMessage());
			throw new FileUploadException(stringBuilder.toString(), ace);
		}

	}

	private String getUniqueFileName(String fileName) {
		return new Date().getTime() + "-" + fileName;
	}

}
