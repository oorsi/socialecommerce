package com.everestes.socialecommerce.file.upload.response;

public class FileUploadResponse {

	private String fileName;
	private String location;

	public FileUploadResponse(String fileName) {
		this.fileName = fileName;
	}

	public FileUploadResponse(String fileName, String location) {
		this.fileName = fileName;
		this.location = location;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
