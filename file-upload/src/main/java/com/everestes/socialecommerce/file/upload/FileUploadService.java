package com.everestes.socialecommerce.file.upload;

import java.io.InputStream;
import java.util.List;

import com.everestes.socialecommerce.file.upload.exception.FileUploadException;
import com.everestes.socialecommerce.file.upload.response.FileUploadResponse;


public interface FileUploadService {

	public List<FileUploadResponse> upload(String directoy, String fileName, InputStream inputStream) throws FileUploadException;

}
