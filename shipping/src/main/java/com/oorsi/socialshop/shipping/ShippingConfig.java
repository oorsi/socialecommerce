package com.oorsi.socialshop.shipping;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.oorsi.socialshop.shipping")
public class ShippingConfig {

}
