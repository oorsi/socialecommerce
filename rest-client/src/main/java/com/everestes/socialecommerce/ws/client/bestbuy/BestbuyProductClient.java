package com.everestes.socialecommerce.ws.client.bestbuy;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.catalog.ProductImageURL;
import com.everestes.socialecommerce.domain.catalog.ProductSKU;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.util.DozerHelper;
import com.everestes.socialecommerce.ws.client.bestbuy.beta.domain.BestBuyResponse;
import com.everestes.socialecommerce.ws.client.bestbuy.domain.product.BestBuyProduct;
import com.everestes.socialecommerce.ws.client.bestbuy.domain.product.Products;

@Component
public class BestbuyProductClient {

	private Logger logger = LoggerFactory.getLogger(BestbuyProductClient.class);

	@Value("${api.url.bestbuy}")
	private String endpointURL;

	@Value("#{environment.API_KEY_BESTBUY}")
	private String apiKey;

	@Autowired
	@Qualifier("restClientMapper")
	private Mapper mapper;

	@Autowired
	@Qualifier("restTemplate")
	protected RestOperations restTemplate;

	@Value("${api.url.bestbuy.beta}")
	private String betaEndpointURL;

	public List<Product> getProducts(String searchString, Integer pageNumber, Integer pageSize) {
		BestBuyURL url = new BestBuyURL(endpointURL, apiKey, "products").sort("salesRankShortTerm.asc")
				.addParameter("pageSize", pageSize.toString()).addParameter("page", pageNumber.toString());
		if (StringUtils.isNotEmpty(searchString)) {
			url.search(searchString.split("[\\W]"));
		}

		try {
			logger.info(url.getURI());
			return DozerHelper.map(mapper, restTemplate.getForObject(url.getURI(), Products.class).getProducts(),
					Product.class);
		} catch (RestClientException | URIException e) {
			logger.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	public List<Product> getProductBySku(String... skus) {

		try {
			String url = new BestBuyURL(endpointURL, apiKey, "products").sku(skus).getURI();
			logger.debug(url);
			return DozerHelper.map(mapper, restTemplate.getForObject(url, Products.class).getProducts(), Product.class);
		} catch (RestClientException | URIException e) {
			return null;
		}

	}

	public List<Product> getProductsByUPC(String... upcs) {

		try {
			String url = new BestBuyURL(endpointURL, apiKey, "products").upc(upcs).getURI();
			logger.debug(url);
			return DozerHelper.map(mapper, restTemplate.getForObject(url, Products.class).getProducts(), Product.class);
		} catch (RestClientException | URIException e) {
			return null;
		}

	}

	public List<Product> getTrendingProducts() {

		try {
			String url = new BestBuyURL(betaEndpointURL, apiKey, "products/trendingViewed").getURI();
			logger.debug(url);

			List<com.everestes.socialecommerce.ws.client.bestbuy.beta.domain.BestBuyProduct> bestBuyProducts = restTemplate
					.getForObject(url, BestBuyResponse.class).getResults();

			List<Product> products = new ArrayList<>();
			for (com.everestes.socialecommerce.ws.client.bestbuy.beta.domain.BestBuyProduct bestBuyProduct : bestBuyProducts) {
				Product product = mapper.map(bestBuyProduct, Product.class);
				product.addProductSKU(new ProductSKU(bestBuyProduct.getSku(), Retailer.BEST_BUY, product));
				products.add(product);
			}
			return products;

		} catch (RestClientException | URIException e) {
			logger.error("", e);
			return new ArrayList<>();
		}

	}

	public Product getProductBySku(String sku) {
		BestBuyURL url = new BestBuyURL(endpointURL, apiKey, "products").sku(sku);
		try {
			BestBuyProduct bestBuyProduct = restTemplate.getForObject(url.getURI(), Products.class).getProducts()
					.get(0);

			Product product = mapper.map(bestBuyProduct, Product.class);
			for (String imgURL : bestBuyProduct.getImageURLs()) {
				product.getProductImageURLs().add(new ProductImageURL(product, imgURL));
			}

			return product;
		} catch (RestClientException | URIException e) {
			return null;
		}

	}

}
