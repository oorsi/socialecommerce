package com.everestes.socialecommerce.ws.client.bestbuy.beta.domain;

import java.util.Map;

import com.everestes.socialecommerce.domain.type.Retailer;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BestBuyProduct {

	private String sku;
	private String name;

	private String image;
	private ProductPrices prices;

	private String upc;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	@JsonProperty("names")
	public void setName(Map<String, String> name) {
		this.name = name.get("title");
	}

	public String getImage() {
		return image;
	}

	@JsonProperty("images")
	public void setImage(Map<String, String> image) {
		this.image = image.get("standard");
	}

	public Retailer getRetailer() {
		return Retailer.BEST_BUY;
	}

	public ProductPrices getPrices() {
		return prices;
	}

	public void setPrices(ProductPrices prices) {
		this.prices = prices;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

}
