package com.everestes.socialecommerce.ws.client.bestbuy;

public class URIException extends Exception {

	private static final long serialVersionUID = -1338619564215947682L;

	public URIException() {
	}

	public URIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public URIException(String message, Throwable cause) {
		super(message, cause);
	}

	public URIException(String message) {
		super(message);
	}

	public URIException(Throwable cause) {
		super(cause);
	}

}
