package com.everestes.socialecommerce.ws.client.amazon.rainforest.domain;

public class Product {

	private String title;
	private String asin;
	private String link;
	private String image;
	private boolean isPrime;
	private Buyboxwinner buybox_winner;
	private Image[] images;
	private String description;
	private Image main_image;

	private Price[] prices;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAsin() {
		return asin;
	}

	public void setAsin(String asin) {
		this.asin = asin;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isPrime() {
		return isPrime;
	}

	public void setPrime(boolean isPrime) {
		this.isPrime = isPrime;
	}

	public Buyboxwinner getBuybox_winner() {
		return buybox_winner;
	}

	public void setBuybox_winner(Buyboxwinner buybox_winner) {
		this.buybox_winner = buybox_winner;
	}

	public Image[] getImages() {
		return images;
	}

	public void setImages(Image[] images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Image getMain_image() {
		return main_image;
	}

	public void setMain_image(Image main_image) {
		this.main_image = main_image;
	}

	public Price[] getPrices() {
		return prices;
	}

	public void setPrices(Price[] prices) {
		this.prices = prices;
	}

}
