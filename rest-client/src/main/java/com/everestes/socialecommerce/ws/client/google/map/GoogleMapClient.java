package com.everestes.socialecommerce.ws.client.google.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import com.everestes.socialecommerce.ws.client.google.map.model.GoogleMapResponse;
import com.everestes.socialecommerce.ws.client.google.map.model.Result;

@Component
public class GoogleMapClient {

	@Autowired
	@Qualifier("restTemplate")
	protected RestOperations restTemplate;

	@Value("${api.url.google.map}")
	private String endpointURL;

	public String vaidateAddress() {
		GoogleMapResponse googleMapResponse = restTemplate.getForObject(endpointURL, GoogleMapResponse.class);
		Result result = (Result) googleMapResponse.getResults().get(0);
		return result.getFormatted_address();
	}

}
