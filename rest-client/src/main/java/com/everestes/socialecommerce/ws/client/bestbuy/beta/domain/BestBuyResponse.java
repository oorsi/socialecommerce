package com.everestes.socialecommerce.ws.client.bestbuy.beta.domain;

import java.util.List;

public class BestBuyResponse {

	private List<BestBuyProduct> results;

	public List<BestBuyProduct> getResults() {
		return results;
	}

	public void setResults(List<BestBuyProduct> results) {
		this.results = results;
	}

}
