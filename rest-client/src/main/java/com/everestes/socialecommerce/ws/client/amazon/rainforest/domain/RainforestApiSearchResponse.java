package com.everestes.socialecommerce.ws.client.amazon.rainforest.domain;

import java.util.List;

public class RainforestApiSearchResponse {

	private List<Product> search_results;

	public List<Product> getSearch_results() {
		return search_results;
	}

	public void setSearch_results(List<Product> search_results) {
		this.search_results = search_results;
	}

}
