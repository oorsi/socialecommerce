package com.everestes.socialecommerce.ws.client.bestbuy.beta.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class BestBuyProductDescription {

	private String shortDescription;

	@JsonGetter("short")
	public String getShortDescription() {
		return shortDescription;
	}

	@JsonSetter("short")
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

}
