package com.everestes.socialecommerce.ws.client.bestbuy.domain.product;

import java.util.List;

import com.everestes.socialecommerce.ws.client.bestbuy.domain.BestBuyResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Products extends BestBuyResponse {

	private List<BestBuyProduct> products;

	public List<BestBuyProduct> getProducts() {
		return products;
	}

	public void setProducts(List<BestBuyProduct> products) {
		this.products = products;
	}

}
