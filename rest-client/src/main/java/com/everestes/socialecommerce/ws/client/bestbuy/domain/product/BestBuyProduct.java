package com.everestes.socialecommerce.ws.client.bestbuy.domain.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.everestes.socialecommerce.domain.type.Retailer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BestBuyProduct {

	private Long sku;
	private String name;
	private boolean active;
	private Float regularPrice;
	private Float salePrice;
	private boolean onSale;
	private boolean onlineAvailability;
	private String shortDescription;
	private String longDescription;
	private String manufacturer;

	private String image;
	private String largeFrontImage;
	private String accessoriesImage;
	private String alternateViewsImage;
	private String angleImage;
	private String backViewImage;
	private String energyGuideImage;
	private String leftViewImage;
	private String rightViewImage;
	private String remoteControlImage;
	private String topViewImage;
	// private String largeImage;

	private String upc;

	private BestBuyProduct[] frequentlyPurchasedWith;
	private BestBuyProduct[] relatedProducts;

	public Long getSku() {
		return sku;
	}

	public void setSku(Long sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Float getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(Float regularPrice) {
		this.regularPrice = regularPrice;
	}

	public Float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Float salePrice) {
		this.salePrice = salePrice;
	}

	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}

	public boolean isOnlineAvailability() {
		return onlineAvailability;
	}

	public void setOnlineAvailability(boolean onlineAvailability) {
		this.onlineAvailability = onlineAvailability;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLargeFrontImage() {
		return largeFrontImage;
	}

	public void setLargeFrontImage(String largeFrontImage) {
		this.largeFrontImage = largeFrontImage;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Retailer getRetailer() {
		return Retailer.BEST_BUY;
	}

	public BestBuyProduct[] getFrequentlyPurchasedWith() {
		return frequentlyPurchasedWith;
	}

	public void setFrequentlyPurchasedWith(BestBuyProduct[] frequentlyPurchasedWith) {
		this.frequentlyPurchasedWith = frequentlyPurchasedWith;
	}

	public BestBuyProduct[] getRelatedProducts() {
		return relatedProducts;
	}

	public void setRelatedProducts(BestBuyProduct[] relatedProducts) {
		this.relatedProducts = relatedProducts;
	}

	public String getAccessoriesImage() {
		return accessoriesImage;
	}

	public void setAccessoriesImage(String accessoriesImage) {
		this.accessoriesImage = accessoriesImage;
	}

	public String getAlternateViewsImage() {
		return alternateViewsImage;
	}

	public void setAlternateViewsImage(String alternateViewsImage) {
		this.alternateViewsImage = alternateViewsImage;
	}

	public String getAngleImage() {
		return angleImage;
	}

	public void setAngleImage(String angleImage) {
		this.angleImage = angleImage;
	}

	public String getBackViewImage() {
		return backViewImage;
	}

	public void setBackViewImage(String backViewImage) {
		this.backViewImage = backViewImage;
	}

	public String getEnergyGuideImage() {
		return energyGuideImage;
	}

	public void setEnergyGuideImage(String energyGuideImage) {
		this.energyGuideImage = energyGuideImage;
	}

	public String getLeftViewImage() {
		return leftViewImage;
	}

	public void setLeftViewImage(String leftViewImage) {
		this.leftViewImage = leftViewImage;
	}

	public String getRightViewImage() {
		return rightViewImage;
	}

	public void setRightViewImage(String rightViewImage) {
		this.rightViewImage = rightViewImage;
	}

	public String getRemoteControlImage() {
		return remoteControlImage;
	}

	public void setRemoteControlImage(String remoteControlImage) {
		this.remoteControlImage = remoteControlImage;
	}

	public String getTopViewImage() {
		return topViewImage;
	}

	public void setTopViewImage(String topViewImage) {
		this.topViewImage = topViewImage;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public List<String> getImageURLs() {
		List<String> imageURLs = new ArrayList<>();

		if (StringUtils.isNotEmpty(largeFrontImage)) {
			imageURLs.add(largeFrontImage);
		}
		if (StringUtils.isNotEmpty(alternateViewsImage)) {
			imageURLs.add(alternateViewsImage);
		}
		if (StringUtils.isNotEmpty(accessoriesImage)) {
			imageURLs.add(accessoriesImage);
		}
		if (StringUtils.isNotEmpty(angleImage)) {
			imageURLs.add(angleImage);
		}
		if (StringUtils.isNotEmpty(backViewImage)) {
			imageURLs.add(backViewImage);
		}
		if (StringUtils.isNotEmpty(rightViewImage)) {
			imageURLs.add(rightViewImage);
		}
		if (StringUtils.isNotEmpty(topViewImage)) {
			imageURLs.add(topViewImage);
		}
		if (StringUtils.isNotEmpty(energyGuideImage)) {
			imageURLs.add(energyGuideImage);
		}
		if (StringUtils.isNotEmpty(leftViewImage)) {
			imageURLs.add(leftViewImage);
		}

		if (StringUtils.isNotEmpty(accessoriesImage)) {
			imageURLs.add(accessoriesImage);
		}
		if (StringUtils.isNotEmpty(remoteControlImage)) {
			imageURLs.add(remoteControlImage);
		}
		return imageURLs;

	}

	@Override
	public String toString() {
		return "Product [sku=" + sku + ", name=" + name + "]";
	}

}
