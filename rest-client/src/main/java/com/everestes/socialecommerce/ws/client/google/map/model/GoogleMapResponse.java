package com.everestes.socialecommerce.ws.client.google.map.model;

import java.util.List;

public class GoogleMapResponse {

	private String status;
	private List<?> results;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<?> getResults() {
		return results;
	}

	public void setResults(List<?> results) {
		this.results = results;
	}

}
