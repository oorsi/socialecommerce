package com.everestes.socialecommerce.ws.client.amazon.rainforest.domain;

public class Price {
	private Float value;
	private String currency;

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
