
package com.everestes.socialecommerce.ws.client.bestbuy;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

public class BestBuyURL {

	private final String url;

	private final String queryType;

	private List<NameValuePair> parameters;

	private StringBuilder queryString;

	public BestBuyURL(String url, String apiKey, final String queryType) {
		this.url = url;
		this.queryType = queryType;

		queryString = new StringBuilder();

		parameters = new ArrayList<>();
		parameters.add(new BasicNameValuePair("apiKey", apiKey));
		parameters.add(new BasicNameValuePair("format", "json"));
	}

	public BestBuyURL addParameter(String key, String value) {
		parameters.add(new BasicNameValuePair(key, value));
		return this;
	}

	public BestBuyURL sku(String... skus) {

		for (int j = 0; j < skus.length; j++) {
			if (j != 0) {
				or();
			}
			sku(Long.parseLong(skus[j]));
		}
		return this;
	}

	public BestBuyURL sku(Long... skus) {
		for (int j = 0; j < skus.length; j++) {
			if (j != 0) {
				or();
			}
			sku(skus[j]);
		}
		return this;
	}

	public BestBuyURL or() {
		queryString.append("%7C");
		return this;
	}

	public BestBuyURL and() {
		queryString.append("%26");
		return this;
	}

	public BestBuyURL sku(Long sku) {
		queryString.append("sku=");
		queryString.append(sku);
		return this;
	}

	public BestBuyURL upc(String upc) {
		queryString.append("upc=");
		queryString.append(upc);
		return this;
	}

	public BestBuyURL upc(String... upcs) {
		for (int j = 0; j < upcs.length; j++) {
			if (j != 0) {
				or();
			}
			upc(upcs[j]);
		}
		return this;
	}

	public BestBuyURL search(String[] searchString) {
		for (int j = 0; j < searchString.length; j++) {
			if (j != 0) {
				and();
			}
			queryString.append("search=");
			queryString.append(searchString[j]);
		}

		return this;
	}

	public BestBuyURL sort(String searchString) {
		parameters.add(new BasicNameValuePair("sort", searchString));
		return this;
	}

	public String getURI() throws URIException {
		StringBuilder builder = new StringBuilder(url);
		if (!url.endsWith("/")) {
			builder.append('/');
		}
		builder.append(queryType);

		if (queryString.length() != 0) {
			builder.append("((");
			builder.append(queryString.toString());
			builder.append("))");

		}

		try {
			URIBuilder uriBuilder = new URIBuilder(builder.toString());
			uriBuilder.addParameters(parameters);
			return URLDecoder.decode(uriBuilder.build().toString(), "UTF-8");
		} catch (URISyntaxException e) {
			throw new URIException(e);
		} catch (UnsupportedEncodingException e) {
			throw new URIException(e);
		}

	}

}
