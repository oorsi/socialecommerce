package com.everestes.socialecommerce.ws.client.amazon.rainforest.domain;

public class RainforestApiResponse {

	private Product product;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
