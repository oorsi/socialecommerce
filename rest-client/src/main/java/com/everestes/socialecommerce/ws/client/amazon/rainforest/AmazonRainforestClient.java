package com.everestes.socialecommerce.ws.client.amazon.rainforest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.catalog.ProductSKU;
import com.everestes.socialecommerce.domain.type.Retailer;
import com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.RainforestApiResponse;
import com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.RainforestApiSearchResponse;

@Component
public class AmazonRainforestClient {

	@Value("#{environment.API_KEY_RAINFOREST}")
	private String apiKey;

	@Value("${api.url.amazon.rainforest}")
	private String baseURL;

	@Autowired
	@Qualifier("restTemplate")
	protected RestOperations restTemplate;

	@Autowired
	@Qualifier("restClientMapper")
	private Mapper mapper;

	public List<Product> search(String searchString) throws UnsupportedEncodingException {
		UriComponentsBuilder uriComponents = UriComponentsBuilder.fromUriString(baseURL).queryParam("api_key", apiKey)
				.queryParam("type", "search").queryParam("amazon_domain", "amazon.com")
				.queryParam("search_term", searchString.replace(' ', '+'));

		List<com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.Product> amazonProducts = restTemplate
				.getForObject(uriComponents.build().toUri(), RainforestApiSearchResponse.class).getSearch_results();

		List<Product> products = new ArrayList<>();
		for (com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.Product amazonProduct : amazonProducts) {
			Product product = mapper.map(amazonProduct, Product.class);
			if (amazonProduct.getPrices() != null && amazonProduct.getPrices().length > 0) {
				product.setRegularPrice(amazonProduct.getPrices()[0].getValue());
				product.setSalePrice(amazonProduct.getPrices()[0].getValue());
			}

			product.setRetailer(Retailer.AMAZON);
			products.add(product);
		}
		return products;
	}

	public Product getProductByAsin(String asin) {
		UriComponentsBuilder uriComponents = UriComponentsBuilder.fromUriString(baseURL).queryParam("api_key", apiKey)
				.queryParam("type", "product").queryParam("amazon_domain", "amazon.com").queryParam("asin", asin);

		com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.Product amazonProduct = restTemplate
				.getForObject(uriComponents.build().toUri(), RainforestApiResponse.class).getProduct();

		return getProductFromAmazonProduct(amazonProduct);
	}

	public Product getProductByURL(String url) {
		UriComponentsBuilder uriComponents = UriComponentsBuilder.fromUriString(baseURL).queryParam("api_key", apiKey)
				.queryParam("type", "product").queryParam("url", url);

		com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.Product amazonProduct = restTemplate
				.getForObject(uriComponents.build().toUri(), RainforestApiResponse.class).getProduct();

		return getProductFromAmazonProduct(amazonProduct);
	}

	public Product getProductFromAmazonProduct(
			com.everestes.socialecommerce.ws.client.amazon.rainforest.domain.Product amazonProduct) {
		Product product = mapper.map(amazonProduct, Product.class);
		product.setRetailer(Retailer.AMAZON);
		product.setProductSKUs(getProductSKUFromASIN(product, amazonProduct.getAsin()));
		product.setImage(amazonProduct.getMain_image().getLink());

		return product;
	}

	public List<ProductSKU> getProductSKUFromASIN(Product product, String asin) {
		List<ProductSKU> productSKUs = new ArrayList<>();
		productSKUs.add(new ProductSKU(asin, Retailer.AMAZON, product));
		return productSKUs;
	}

}
