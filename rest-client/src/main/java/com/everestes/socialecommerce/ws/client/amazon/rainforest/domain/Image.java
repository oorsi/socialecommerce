package com.everestes.socialecommerce.ws.client.amazon.rainforest.domain;

public class Image {

	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
