package com.everestes.socialecommerce.ws.client.bestbuy.beta.domain;

public class ProductPrices {

	private Float regular;
	private Float current;

	public Float getRegular() {
		return regular;
	}

	public void setRegular(Float regular) {
		this.regular = regular;
	}

	public Float getCurrent() {
		return current;
	}

	public void setCurrent(Float current) {
		this.current = current;
	}

}
