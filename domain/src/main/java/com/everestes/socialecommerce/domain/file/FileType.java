package com.everestes.socialecommerce.domain.file;

public enum FileType {
	THUMBNAIL, FULL
}
