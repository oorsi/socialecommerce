package com.everestes.socialecommerce.domain.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Retailer {
	AMAZON(2, "Amazon"), BEST_BUY(1, "Best Buy"), UNKNOWN(0, "Unknown");

	private static final Map<Integer, Retailer> lookup = new HashMap<Integer, Retailer>();

	static {
		for (Retailer r : EnumSet.allOf(Retailer.class))
			lookup.put(r.getId(), r);
	}

	private Retailer(int id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}

	private int id;
	private String displayName;

	public int getId() {
		return id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public static Retailer get(int id) {
		return lookup.get(id);
	}

}
