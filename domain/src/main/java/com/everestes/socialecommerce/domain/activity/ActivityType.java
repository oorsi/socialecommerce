package com.everestes.socialecommerce.domain.activity;

public enum ActivityType {
	WLP, COMMENT, LIKE, FOLLOW, CUSTOMER_ORDER
}
