package com.everestes.socialecommerce.domain.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.everestes.socialecommerce.domain.type.Retailer;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
// @Table(uniqueConstraints = @UniqueConstraint(columnNames = { "retailer",
// "sku" }))
public class Product {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long productId;

	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Retailer retailer;

	// ASIN for amazon
	private String sku;

	private String name;
	private boolean active;
	private Float regularPrice;
	private Float salePrice;
	private boolean onSale;
	private boolean onlineAvailability;

	@Lob
	private String shortDescription;

	@Lob
	private String longDescription;
	private String image;
	private String largeFrontImage;
	private String mediumImage;
	private String thumbnailImage;
	private String largeImage;
	private String manufacturer;

	private String upc;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<ProductImageURL> productImageURLs;

	@Transient
	private Product[] frequentlyPurchasedWith;
	@Transient
	private Product[] relatedProducts;

//	@JsonIgnore
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ProductSKU> productSKUs;

	private Date addedDate;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Float getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(Float regularPrice) {
		this.regularPrice = regularPrice;
	}

	public Float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Float salePrice) {
		this.salePrice = salePrice;
	}

	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
	}

	public boolean isOnlineAvailability() {
		return onlineAvailability;
	}

	public void setOnlineAvailability(boolean onlineAvailability) {
		this.onlineAvailability = onlineAvailability;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLargeFrontImage() {
		return largeFrontImage;
	}

	public void setLargeFrontImage(String largeFrontImage) {
		this.largeFrontImage = largeFrontImage;
	}

	public String getMediumImage() {
		return mediumImage;
	}

	public void setMediumImage(String mediumImage) {
		this.mediumImage = mediumImage;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(String thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public String getLargeImage() {
		return largeImage;
	}

	public void setLargeImage(String largeImage) {
		this.largeImage = largeImage;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	public Product[] getFrequentlyPurchasedWith() {
		return frequentlyPurchasedWith;
	}

	public void setFrequentlyPurchasedWith(Product[] frequentlyPurchasedWith) {
		this.frequentlyPurchasedWith = frequentlyPurchasedWith;
	}

	public Product[] getRelatedProducts() {
		return relatedProducts;
	}

	public void setRelatedProducts(Product[] relatedProducts) {
		this.relatedProducts = relatedProducts;
	}

	public int getRetailerId() {
		return getRetailer().getId();
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

//	@Override
//	public String toString() {
//		return "Product [sku=" + sku + ", name=" + name + "]";
//	}

	public List<ProductImageURL> getProductImageURLs() {
		if (null == productImageURLs) {
			productImageURLs = new ArrayList<>();
		}
		return productImageURLs;
	}

	public void setProductImageURLs(List<ProductImageURL> productImageURLs) {
		this.productImageURLs = productImageURLs;
	}

	@JsonIgnore
	public List<ProductSKU> getProductSKUs() {
		return productSKUs;
	}

	public void setProductSKUs(List<ProductSKU> productSKUs) {
		this.productSKUs = productSKUs;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public void addProductSKU(ProductSKU productSKU) {
		if (null == productSKUs) {
			productSKUs = new ArrayList<>();
		}
		productSKUs.add(productSKU);
	}

}
