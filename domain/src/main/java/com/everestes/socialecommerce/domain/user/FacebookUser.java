package com.everestes.socialecommerce.domain.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "facebookUserID" }),
		@UniqueConstraint(columnNames = { "userID" }) })
public class FacebookUser {

	@Id
	private Long userID;

	@OneToOne
	@JoinColumn(name = "userID", insertable = false, updatable = false)
	private User user;

	@Column(nullable = false)
	private String facebookUserID;

	private String accessToken;

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFacebookUserID() {
		return facebookUserID;
	}

	public void setFacebookUserID(String facebookUserID) {
		this.facebookUserID = facebookUserID;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
