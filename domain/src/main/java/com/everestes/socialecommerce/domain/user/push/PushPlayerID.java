package com.everestes.socialecommerce.domain.user.push;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everestes.socialecommerce.domain.user.User;

@Entity
public class PushPlayerID {

	@Id
	private String playerID;

	@ManyToOne
	@JoinColumn(name = "userID")
	private User user;

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
