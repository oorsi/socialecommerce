package com.everestes.socialecommerce.domain.user.relationship;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "followerId", "followedId" }))
@DiscriminatorValue("FOLLOW")
public class Friendship extends Activity {

	@ManyToOne
	@JoinColumn(name = "followerId")
	@JsonIgnore
	private User follower;

	@ManyToOne
	@JoinColumn(name = "followedId")
	private User followed;

	private boolean confirmed;

	public User getFollower() {
		return follower;
	}

	public void setFollower(User follower) {
		super.setUser(follower);
		this.follower = follower;
	}

	public User getFollowed() {
		return followed;
	}

	public void setFollowed(User followed) {
		super.setForUser(followed);
		this.followed = followed;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

}
