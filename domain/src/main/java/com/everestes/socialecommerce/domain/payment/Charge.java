package com.everestes.socialecommerce.domain.payment;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.everestes.socialecommerce.domain.order.CustomerOrder;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Charge {

	@Id
	private String id;

	@OneToOne
	@JoinColumn(name = "orderId", nullable = false)
	@JsonIgnore
	private CustomerOrder customerOrder;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CustomerOrder getCustomerOrder() {
		return customerOrder;
	}

	public void setCustomerOrder(CustomerOrder customerOrder) {
		this.customerOrder = customerOrder;
	}

}
