package com.everestes.socialecommerce.domain.payment;

public class PaymentMethod {

	private String id;
	private String brand;
	private Long expMonth;
	private Long expYear;
	private String last4;
	private String customer;

	private Boolean defaultForCurrency;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Long getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(Long expMonth) {
		this.expMonth = expMonth;
	}

	public Long getExpYear() {
		return expYear;
	}

	public void setExpYear(Long expYear) {
		this.expYear = expYear;
	}

	public String getLast4() {
		return last4;
	}

	public void setLast4(String last4) {
		this.last4 = last4;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Boolean getDefaultForCurrency() {
		return defaultForCurrency;
	}

	public void setDefaultForCurrency(Boolean defaultForCurrency) {
		this.defaultForCurrency = defaultForCurrency;
	}

}
