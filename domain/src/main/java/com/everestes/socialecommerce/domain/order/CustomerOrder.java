package com.everestes.socialecommerce.domain.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.address.UserAddress;
import com.everestes.socialecommerce.domain.payment.Charge;
import com.everestes.socialecommerce.domain.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DiscriminatorValue("CUSTOMER_ORDER")
public class CustomerOrder extends Activity {

	@OneToMany(mappedBy = "customerOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<OrderProduct> orderProducts;

	@ManyToOne
	@JoinColumn(name = "fromUserId", nullable = false)
	@JsonIgnore
	private User fromUser;

	@ManyToOne
	@JoinColumn(name = "forUserId", nullable = false)
	private User forUser;

	@NotNull
	private Long shipTo;

	@OneToOne(mappedBy = "customerOrder")
	private Charge charge;

	@ManyToOne
	@JoinColumn(name = "shippingAddressId", nullable = true)
	private UserAddress shippingAddress;

	@NotNull
	private Date date;

	private boolean fulfilled;

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		super.setUser(fromUser);
		this.fromUser = fromUser;
	}

	public User getForUser() {
		return forUser;
	}

	public void setForUser(User forUser) {
		super.setForUser(forUser);
		this.forUser = forUser;
	}

	public List<OrderProduct> getOrderProducts() {
		if (null == orderProducts) {
			orderProducts = new ArrayList<>();
		}
		return orderProducts;
	}

	public void addOrderProduct(OrderProduct orderProduct) {
		if (!getOrderProducts().contains(orderProduct)) {
			getOrderProducts().add(orderProduct);
		}
	}

	public void setOrderProducts(List<OrderProduct> orderProducts) {
		this.orderProducts = orderProducts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getShipTo() {
		return shipTo;
	}

	public void setShipTo(Long shipTo) {
		this.shipTo = shipTo;
	}

	public UserAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(UserAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public float getTotalPrice() {
		float sum = 0;
		for (OrderProduct orderProduct : getOrderProducts()) {
			sum += orderProduct.getTotalPrice();
		}
		return sum;
	}

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

	public boolean isFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(boolean fulfilled) {
		this.fulfilled = fulfilled;
	}
}
