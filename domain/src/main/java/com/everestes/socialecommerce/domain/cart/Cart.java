package com.everestes.socialecommerce.domain.cart;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.everestes.socialecommerce.domain.user.User;

//@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "fromUserId", "forUserId" }))
public class Cart {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fromUserId", nullable = false)
	private User fromUser;

	@ManyToOne
	@JoinColumn(name = "forUserId", nullable = false)
	private User forUser;

	@OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
	private List<CartProduct> cartProducts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getForUser() {
		return forUser;
	}

	public void setForUser(User forUser) {
		this.forUser = forUser;
	}

	public List<CartProduct> getCartProducts() {
		if (null == cartProducts) {
			cartProducts = new ArrayList<>();
		}
		return cartProducts;
	}

	public void setCartProducts(List<CartProduct> cartProducts) {
		this.cartProducts = cartProducts;
	}

}
