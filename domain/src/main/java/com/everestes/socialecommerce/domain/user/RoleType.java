package com.everestes.socialecommerce.domain.user;

public enum RoleType {
	SUPER_ADMIN, ADMIN, CUSTOMER
}
