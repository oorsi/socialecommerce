package com.everestes.socialecommerce.domain.cart;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class CartProduct {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fromUserId", nullable = false)
	@JsonIgnore
	private User fromUser;

	@ManyToOne
	@JoinColumn(name = "forUserId", nullable = false)
	private User forUser;

	@ManyToOne
	@JoinColumn(name = "productId", nullable = false)
	private Product product;

	private Date date;

	private int quantity;

	@JsonIgnore
	private boolean purchased;

	@JsonIgnore
	private boolean removed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getForUser() {
		return forUser;
	}

	public void setForUser(User forUser) {
		this.forUser = forUser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getTotalPrice() {
		return quantity * product.getSalePrice();
	}

	public boolean isPurchased() {
		return purchased;
	}

	public void setPurchased(boolean purchased) {
		this.purchased = purchased;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}
}
