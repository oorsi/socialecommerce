package com.everestes.socialecommerce.domain.user.relationship;

public enum Relationship {
	FRIEND("Friend"), WIFE("Wife"), HUSBAND("Husband"), GIRL_FRIEND("Girl Friend"), BOY_FRIEND("Boy Friend"), FIANCEE(
			"Fiancee"), CO_WORKER("Co-worker"), CLASSMATE("Classmate");

	private String relationship;

	Relationship(String relationship) {
		this.relationship = relationship;
	}

	public String getRelationship() {
		return relationship;
	}
}
