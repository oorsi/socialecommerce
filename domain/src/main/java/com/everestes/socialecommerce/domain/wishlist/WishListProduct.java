package com.everestes.socialecommerce.domain.wishlist;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.everestes.socialecommerce.domain.activity.Activity;
import com.everestes.socialecommerce.domain.catalog.Product;
import com.everestes.socialecommerce.domain.user.User;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "userId", "productId" }))
@DiscriminatorValue("WLP")
public class WishListProduct extends Activity {

	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;

	@ManyToOne
	@JoinColumn(nullable = false, name = "userId")
	private User u;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public User getU() {
		return u;
	}

	public void setU(User user) {
		super.setUser(user);
		this.u = user;
	}

}
