package com.everestes.socialecommerce.domain.catalog;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everestes.socialecommerce.domain.type.Retailer;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ProductSKU {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;

	private String sku;

	@JsonIgnore
	@Enumerated(EnumType.STRING)
	private Retailer retailer;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;

	public ProductSKU() {
	}

	public ProductSKU(String sku, Retailer retailer, Product product) {
		this.sku = sku;
		this.retailer = retailer;
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Retailer getRetailer() {
		return retailer;
	}

	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	public int getRetailerId() {
		return this.retailer.getId();
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
