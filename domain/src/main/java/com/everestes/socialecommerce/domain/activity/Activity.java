package com.everestes.socialecommerce.domain.activity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.everestes.socialecommerce.domain.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "activityType")
public abstract class Activity {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(nullable = false, name = "userId")
	private User user;

	@ManyToOne
	@JoinColumn(nullable = true, name = "forUserId")
	private User forUser;

	@Column(nullable = false)
	private Date created;

	@Column(nullable = false)
	private Date updated;

	@OneToMany(mappedBy = "activity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<Comment> comments;

	@OneToMany(mappedBy = "activity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	@LazyCollection(LazyCollectionOption.EXTRA)
	private List<ActivityLike> likes;

	@Transient
	private boolean liked;

	@Column(updatable = false, insertable = false)
	@Enumerated(EnumType.STRING)
	private ActivityType activityType;

	public Activity() {
		created = updated = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCreated() {
		return created;
	}

	public User getForUser() {
		return forUser;
	}

	public void setForUser(User forUser) {
		this.forUser = forUser;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<ActivityLike> getLikes() {
		return likes;
	}

	public void setLikes(List<ActivityLike> likes) {
		this.likes = likes;
	}

	public int getNumberOfLikes() {
		if (null != likes) {
			return likes.size();
		}
		return 0;
	}

	public int getNumberOfComments() {
		if (null != comments) {
			return comments.size();
		}
		return 0;
	}

	public void onCreate() {
		updated = created = new Date();
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

}
