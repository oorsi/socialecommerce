package com.everestes.socialecommerce.domain.file;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;

@Entity
public class FileMetaData {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long fileID;
	private String directory;
	private String fileName;

	public Long getFileID() {
		return fileID;
	}

	public void setFileID(Long fileID) {
		this.fileID = fileID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	@Override
	public String toString() {
		return fileName;
	}

}
