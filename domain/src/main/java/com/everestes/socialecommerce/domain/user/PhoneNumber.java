package com.everestes.socialecommerce.domain.user;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;

import com.everestes.socialecommerce.domain.user.phoneNumber.PhoneNumberConfirmation;

@Entity
public class PhoneNumber {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.AUTO)
	private Long id;

	@OneToOne
	@JoinColumn(name = "userId")
	private User user;

	private String phoneNumber;

	private boolean confirmed;

	@OneToOne(mappedBy = "phoneNumber", cascade = CascadeType.ALL)
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private PhoneNumberConfirmation phoneNumberConfirmation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public PhoneNumberConfirmation getPhoneNumberConfirmation() {
		return phoneNumberConfirmation;
	}

	public void setPhoneNumberConfirmation(PhoneNumberConfirmation phoneNumberConfirmation) {
		this.phoneNumberConfirmation = phoneNumberConfirmation;
	}

}
