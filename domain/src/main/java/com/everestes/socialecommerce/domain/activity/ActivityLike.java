package com.everestes.socialecommerce.domain.activity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.everestes.socialecommerce.domain.user.User;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "activityId", "likedById" }) })
@DiscriminatorValue("LIKE")
public class ActivityLike extends Activity {

	@ManyToOne
	@JoinColumn(nullable = false, name = "activityId")
	@JsonBackReference
	private Activity activity;

	@ManyToOne
	@JoinColumn(name = "likedById", nullable = false)
	private User likedBy;

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public User getLikedBy() {
		return likedBy;
	}

	public void setLikedBy(User likedBy) {
		super.setUser(likedBy);
		this.likedBy = likedBy;
	}

}
