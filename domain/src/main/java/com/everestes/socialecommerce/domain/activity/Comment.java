package com.everestes.socialecommerce.domain.activity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@DiscriminatorValue("COMMENT")
public class Comment extends Activity {

	private String comment;

	@ManyToOne
	@JoinColumn(nullable = false, name = "activitytId")
	@JsonBackReference
	private Activity activity;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		super.setForUser(activity.getUser());
		this.activity = activity;
	}

}
