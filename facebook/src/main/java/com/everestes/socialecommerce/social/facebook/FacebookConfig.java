package com.everestes.socialecommerce.social.facebook;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.everestes.socialecommerce.data.DataConfig;

@Configuration
@PropertySource("classpath:facebook.properties")
@ComponentScan("com.everestes.socialecommerce.social.facebook")
@Import(DataConfig.class)
public class FacebookConfig {

	// @Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		configurer.setIgnoreUnresolvablePlaceholders(true);
		return configurer;
	}

}
