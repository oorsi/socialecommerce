package com.everestes.socialecommerce.social.facebook.friends;

import java.util.List;

import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;

import com.everestes.socialecommerce.domain.user.User;

@Component
public class FacebookService {

	public User getUser(String accessToken) {
		org.springframework.social.facebook.api.User fbUser = new FacebookTemplate(accessToken).userOperations()
				.getUserProfile();
		User user = new User();
		user.setFirstName(fbUser.getFirstName());
		user.setLastName(fbUser.getLastName());
		user.setEmail(fbUser.getEmail());

		return user;
	}

	public String getUserID(String accessToken) {
		org.springframework.social.facebook.api.User fbUser = new FacebookTemplate(accessToken).userOperations()
				.getUserProfile();
		return fbUser.getId();
	}

	public List<String> getFriends(String accessToken) {
		PagedList<String> facebookUsers = new FacebookTemplate(accessToken).friendOperations().getFriendIds();
		return facebookUsers;

	}

}
